-- This script was motivated by the switch from ms translator to ms azure translator.
-- A new table will be created for the ms azure translator connector.  The old table
-- will continue to exist but will not be deleted.  There will be no code that uses
-- it.

USE `gateway`;

START TRANSACTION;

-- Remove optional FK pointing to the old translator connector.
UPDATE `twitter_topic` SET `ms_translator_connector_id` = null;

-- Add the new table
CREATE TABLE `ms_azure_translator_connector` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `version` bigint(20) NOT NULL,
  `remote_id` varchar(255) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `chars_per_month` bigint(20) NOT NULL,
  `subscription_key` varchar(255) NOT NULL,
  `application_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKm1fot36m8k5r1vjs43qn8rqpo` (`application_id`),
  CONSTRAINT `FKm1fot36m8k5r1vjs43qn8rqpo` FOREIGN KEY (`application_id`) REFERENCES `application` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- drop the FK pointing to the old translator connector
ALTER TABLE `twitter_topic` DROP FOREIGN KEY `FKqt4edxnelabje78jh1j8twxqx`;

-- add a FK pointing to the new translator connector
ALTER TABLE `twitter_topic` ADD CONSTRAINT `FK_topic_azure_translator_id` FOREIGN KEY (`ms_translator_connector_id`) REFERENCES `ms_azure_translator_connector` (`id`);

ALTER TABLE `gateway`.`klaver_connector`
CHANGE COLUMN `remote_id` `remote_id` VARCHAR(36) NOT NULL,
ADD INDEX `klaver_connector_remote_id_index` (`remote_id`);

ALTER TABLE `gateway`.`ms_azure_translator_connector`
CHANGE COLUMN `remote_id` `remote_id` VARCHAR(36) NOT NULL,
ADD INDEX `ms_azure_translator_connector_remote_id_index` (`remote_id`);

ALTER TABLE `gateway`.`twitter_connector`
CHANGE COLUMN `remote_id` `remote_id` VARCHAR(36) NOT NULL,
ADD INDEX `twitter_connector_remote_id_index` (`remote_id`);

ALTER TABLE `gateway`.`twitter_keyword`
CHANGE COLUMN `remote_id` `remote_id` VARCHAR(36) NOT NULL,
ADD INDEX `twitter_keyword_remote_id_index` (`remote_id`);

ALTER TABLE `gateway`.`twitter_topic`
CHANGE COLUMN `remote_id` `remote_id` VARCHAR(36) NOT NULL,
ADD INDEX `twitter_topic_remote_id_index` (`remote_id`);

COMMIT;

