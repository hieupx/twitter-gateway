USE `gateway`;

ALTER TABLE `klaver_connector` ADD UNIQUE KEY `UK_i55gv5i14nictl3goomy3lwii` (`token`);

ALTER TABLE `ms_azure_translator_connector` MODIFY COLUMN `subscription_key` varchar(255) NOT NULL;

ALTER TABLE `ms_azure_translator_connector` ADD UNIQUE KEY `UK_n00urwdbj549pdffy0l29edn9` (`subscription_key`);

ALTER TABLE `twitter_connector` ADD UNIQUE KEY `UKkxqye6pehcnjotuad6cy94b8x` (`consumer_key`,`consumer_secret`);