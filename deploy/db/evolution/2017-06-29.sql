use `gateway`;

ALTER TABLE `klaver_connector` ADD CONSTRAINT `UK_klaver_connector_remote_id_app_id` UNIQUE (`application_id`, `remote_id`);
ALTER TABLE `ms_azure_translator_connector` ADD CONSTRAINT `UK_translator_topic_remote_id_app_id` UNIQUE (`application_id`, `remote_id`);
ALTER TABLE `twitter_connector` ADD CONSTRAINT `UK_twitter_connector_remote_id_app_id` UNIQUE (`application_id`, `remote_id`);
ALTER TABLE `twitter_topic` ADD CONSTRAINT `UK_twitter_topic_remote_id_app_id` UNIQUE (`application_id`, `remote_id`);
ALTER TABLE `twitter_keyword` ADD CONSTRAINT `UK_twitter_keyword_remote_id_app_id` UNIQUE (`application_id`, `remote_id`);