ALTER TABLE `gateway`.`twitter_keyword`
    CHANGE COLUMN `query` `query` TEXT NOT NULL ,
    CHANGE COLUMN `search_query` `search_query` TEXT NOT NULL;
