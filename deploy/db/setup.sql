CREATE DATABASE IF NOT EXISTS gateway CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- Below statement only works on MySQL 5.7 and above
-- CREATE USER IF NOT EXISTS 'gateway_user' IDENTIFIED BY 'NtqH@n01';

-- Below statement will automatically create the user.
GRANT ALL PRIVILEGES ON gateway.* TO 'gateway_user'@'%' IDENTIFIED BY 'NtqH@n01';
FLUSH PRIVILEGES;
