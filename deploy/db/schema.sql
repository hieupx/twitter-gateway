-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: gateway
-- ------------------------------------------------------
-- Server version	5.7.15-log

USE `gateway`;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `application`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `version` bigint(20) NOT NULL,
  `app_id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `app_secret` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_9q5v0sivpmi0ruax0gweere69` (`app_id`),
  UNIQUE KEY `UK_6e90jvkn0xacoatqyje680hyn` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `klaver_connector`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `klaver_connector` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `version` bigint(20) NOT NULL,
  `remote_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `application_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_i55gv5i14nictl3goomy3lwii` (`token`),
  KEY `FK6shxua8bdxi5cap1rqflub2nr` (`application_id`),
  CONSTRAINT `UK_klaver_connector_remote_id_app_id` UNIQUE (`application_id`, `remote_id`),
  CONSTRAINT `FK6shxua8bdxi5cap1rqflub2nr` FOREIGN KEY (`application_id`) REFERENCES `application` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ms_azure_translator_connector`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ms_azure_translator_connector` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `version` bigint(20) NOT NULL,
  `remote_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chars_per_month` bigint(20) NOT NULL,
  `subscription_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `application_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_n00urwdbj549pdffy0l29edn9` (`subscription_key`),
  KEY `FKm1fot36m8k5r1vjs43qn8rqpo` (`application_id`),
  CONSTRAINT `UK_translator_topic_remote_id_app_id` UNIQUE (`application_id`, `remote_id`),
  CONSTRAINT `FKm1fot36m8k5r1vjs43qn8rqpo` FOREIGN KEY (`application_id`) REFERENCES `application` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `role`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `version` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitter_connector`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitter_connector` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `version` bigint(20) NOT NULL,
  `remote_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_token_secret` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `consumer_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumer_secret` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `interval_length` int(11) NOT NULL,
  `requests_per_interval` int(11) NOT NULL,
  `application_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKll7u6donevaeic1ehhcgle2rq` (`application_id`),
  UNIQUE KEY `UKkxqye6pehcnjotuad6cy94b8x` (`consumer_key`,`consumer_secret`),
  CONSTRAINT `UK_twitter_connector_remote_id_app_id` UNIQUE (`application_id`, `remote_id`),
  CONSTRAINT `FKll7u6donevaeic1ehhcgle2rq` FOREIGN KEY (`application_id`) REFERENCES `application` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitter_keyword`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitter_keyword` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `version` bigint(20) NOT NULL,
  `remote_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doing_recent_tweets_search` bit(1) NOT NULL,
  `historical_searches_count` bigint(20) NOT NULL,
  `lang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_historical_tweets_count` int(11) NOT NULL,
  `last_recent_tweets_count` int(11) NOT NULL,
  `last_serviced` datetime DEFAULT NULL,
  `max_tweet_id` bigint(20) NOT NULL,
  `query` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `recent_searches_count` bigint(20) NOT NULL,
  `search_query` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `serviced_count` bigint(20) NOT NULL,
  `since_tweet_id` bigint(20) NOT NULL,
  `total_tweets_count` bigint(20) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `application_id` bigint(20) NOT NULL,
  `topic_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK9awcbs63sowffvwd9jlbr7967` (`application_id`),
  KEY `FK246jun8cupdao4bt0kks1gqeo` (`topic_id`),
  CONSTRAINT `UK_twitter_keyword_remote_id_app_id` UNIQUE (`application_id`, `remote_id`),
  CONSTRAINT `FK246jun8cupdao4bt0kks1gqeo` FOREIGN KEY (`topic_id`) REFERENCES `twitter_topic` (`id`),
  CONSTRAINT `FK9awcbs63sowffvwd9jlbr7967` FOREIGN KEY (`application_id`) REFERENCES `application` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitter_keyword_scheduling_info`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitter_keyword_scheduling_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `version` bigint(20) NOT NULL,
  `last_serviced_keyword_id` bigint(20) NOT NULL,
  `topic_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKlr4636lmktdfqrq4nwr3cocab` (`topic_id`),
  CONSTRAINT `FKlr4636lmktdfqrq4nwr3cocab` FOREIGN KEY (`topic_id`) REFERENCES `twitter_topic` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitter_topic`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitter_topic` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `version` bigint(20) NOT NULL,
  `remote_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `find_negative_emoticons` bit(1) DEFAULT NULL,
  `find_original_posts` bit(1) DEFAULT NULL,
  `find_positive_emoticons` bit(1) DEFAULT NULL,
  `find_questions` bit(1) DEFAULT NULL,
  `keywords_count` int(11) NOT NULL,
  `last_serviced` datetime DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lon` double DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `radius_unit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `run_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `serviced_count` bigint(20) NOT NULL,
  `since` datetime DEFAULT NULL,
  `time_zone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `until` datetime DEFAULT NULL,
  `application_id` bigint(20) NOT NULL,
  `klaver_connector_id` bigint(20) DEFAULT NULL,
  `ms_translator_connector_id` bigint(20) DEFAULT NULL,
  `twitter_connector_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_twitter_topic_run_status` (`run_status`),
  KEY `FK2gqlw1vrsexnbscnns2v1gj7x` (`application_id`),
  KEY `FKkixp30vks8espvruet4bb2u56` (`klaver_connector_id`),
  KEY `FKqt4edxnelabje78jh1j8twxqx` (`ms_translator_connector_id`),
  KEY `FK93pr0dst6k5scj39td2v2ct1l` (`twitter_connector_id`),
  CONSTRAINT `UK_twitter_topic_remote_id_app_id` UNIQUE (`application_id`, `remote_id`),
  CONSTRAINT `FK2gqlw1vrsexnbscnns2v1gj7x` FOREIGN KEY (`application_id`) REFERENCES `application` (`id`),
  CONSTRAINT `FK93pr0dst6k5scj39td2v2ct1l` FOREIGN KEY (`twitter_connector_id`) REFERENCES `twitter_connector` (`id`),
  CONSTRAINT `FKkixp30vks8espvruet4bb2u56` FOREIGN KEY (`klaver_connector_id`) REFERENCES `klaver_connector` (`id`),
  CONSTRAINT `FK_topic_azure_translator_id` FOREIGN KEY (`ms_translator_connector_id`) REFERENCES `ms_azure_translator_connector` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitter_topic_scheduling_info`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitter_topic_scheduling_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `version` bigint(20) NOT NULL,
  `last_serviced_topic_id` bigint(20) NOT NULL,
  `twitter_connector_id` bigint(20) DEFAULT NULL,
  `last_available_requests` int(11) NOT NULL,
  `last_window_reset_time` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKhq5qa0biy9g1dexs0km0okyk0` (`twitter_connector_id`),
  CONSTRAINT `FKhq5qa0biy9g1dexs0km0okyk0` FOREIGN KEY (`twitter_connector_id`) REFERENCES `twitter_connector` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitter_topic_status`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitter_topic_status` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `version` bigint(20) NOT NULL,
  `code` int(11) DEFAULT NULL,
  `details` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyword_id` bigint(20) NOT NULL,
  `service_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_content` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `topic_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK74pp3ydnm7pdi1fp4xdao3li5` (`keyword_id`),
  KEY `FKrkubmo75es1lmnpo6s4xi8b6m` (`topic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `version` bigint(20) NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_sb8bbouer5wak8vyiiy4pf2bx` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_role`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  KEY `FKa68196081fvovjhkek5m97n3y` (`role_id`),
  KEY `FK859n2jvi8ivhui0rl0esws6o` (`user_id`),
  CONSTRAINT `FK859n2jvi8ivhui0rl0esws6o` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKa68196081fvovjhkek5m97n3y` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'gateway'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-07 22:28:44

ALTER TABLE `gateway`.`klaver_connector`
CHANGE COLUMN `remote_id` `remote_id` VARCHAR(36) NOT NULL,
ADD INDEX `klaver_connector_remote_id_index` (`remote_id`);

ALTER TABLE `gateway`.`ms_azure_translator_connector`
CHANGE COLUMN `remote_id` `remote_id` VARCHAR(36) NOT NULL,
ADD INDEX `ms_azure_translator_connector_remote_id_index` (`remote_id`);

ALTER TABLE `gateway`.`twitter_connector`
CHANGE COLUMN `remote_id` `remote_id` VARCHAR(36) NOT NULL,
ADD INDEX `twitter_connector_remote_id_index` (`remote_id`);

ALTER TABLE `gateway`.`twitter_keyword`
CHANGE COLUMN `remote_id` `remote_id` VARCHAR(36) NOT NULL,
ADD INDEX `twitter_keyword_remote_id_index` (`remote_id`);

ALTER TABLE `gateway`.`twitter_topic`
CHANGE COLUMN `remote_id` `remote_id` VARCHAR(36) NOT NULL,
ADD INDEX `twitter_topic_remote_id_index` (`remote_id`);