#!/bin/sh
SERVICE_NAME=pubsense-gateway
WORKING_DIR=/home/jbu/workspace/pubsense-gateway/
PATH_TO_JAR=/home/jbu/workspace/pubsense-gateway/target/pubsense-gateway.jar
PID_PATH_NAME=/tmp/pubsense-gateway-pid
case $1 in
    start)
        echo "Starting $SERVICE_NAME ..."
        if [ ! -f $PID_PATH_NAME ]; then
		cd $WORKING_DIR
		nohup java -jar $PATH_TO_JAR 2>> /dev/null >> /dev/null &
		echo $! > $PID_PATH_NAME
            	echo "$SERVICE_NAME started ..."
        else
		echo "$SERVICE_NAME is already running ..."
        fi
    ;;
    stop)
        if [ -f $PID_PATH_NAME ]; then
            PID=$(cat $PID_PATH_NAME);
            echo "$SERVICE_NAME stoping ..."
            kill $PID;
            echo "$SERVICE_NAME stopped ..."
            rm $PID_PATH_NAME
        else
            echo "$SERVICE_NAME is not running ..."
        fi
    ;;
    restart)
        if [ -f $PID_PATH_NAME ]; then
            PID=$(cat $PID_PATH_NAME);
            echo "$SERVICE_NAME stopping ...";
            kill $PID;
            echo "$SERVICE_NAME stopped ...";
            rm $PID_PATH_NAME
            echo "$SERVICE_NAME starting ..."
            nohup java -jar $PATH_TO_JAR 2>> /dev/null >> /dev/null &
                        echo $! > $PID_PATH_NAME
            echo "$SERVICE_NAME started ..."
        else
            echo "$SERVICE_NAME is not running ..."
        fi
    ;;
esac 
