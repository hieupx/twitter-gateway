#!/bin/bash

#configurable properties
GATEWAY_ROOT=/home/jbu/workspace/pubsense-gateway
USERAPP_ROOT=/home/jbu/workspace/ms-pubsense-userapp
UI_ROOT=/home/jbu/workspace/app-pubsense-box

#README: This script creates pubsense.zip in the current
#working directory with the following directory structure
#when unzipped. Modify the configuration variables
#above before running.

#
#pubsense
	#app-pubsense-box.zip
	#gateway
		#gateway-service.sh
		#setup.sql
		#schema.sql
		#evolution
		#pubsense.gateway.jar
		#config
			#application.properties.example
	#pubsense-userapp
		#schema.sql
		#evolution
		#create_users.sql
		#ms-pubsense-userapp.war

pushd $USERAPP_ROOT
grails clean && grails prod war
popd

pushd $GATEWAY_ROOT
mvn clean install -DskipTests=true -Pprod
popd

mkdir ./tmp

pushd ./tmp

mkdir ./pubsense
mkdir ./pubsense/gateway
mkdir ./pubsense/pubsense-userapp
mkdir ./pubsense/app-pubsense-box

cp $GATEWAY_ROOT/deploy/db/schema.sql ./pubsense/gateway
cp $GATEWAY_ROOT/deploy/db/setup.sql ./pubsense/gateway
cp -R $GATEWAY_ROOT/deploy/db/evolution ./pubsense/gateway
cp $GATEWAY_ROOT/deploy/scripts/gateway_service.sh ./pubsense/gateway
cp -R $GATEWAY_ROOT/deploy/config ./pubsense/gateway
cp $GATEWAY_ROOT/target/pubsense-gateway.jar ./pubsense/gateway

cp $USERAPP_ROOT/db/create_users.sql ./pubsense/pubsense-userapp
cp $USERAPP_ROOT/db/schema.sql ./pubsense/pubsense-userapp
cp -R $USERAPP_ROOT/db/evolution ./pubsense/pubsense-userapp
cp $USERAPP_ROOT/target/ms-pubsense-userapp.war ./pubsense/pubsense-userapp

#dont add anything we dont want the customer to have
cp $UI_ROOT/app-pubsense-box.html ./pubsense/app-pubsense-box
cp -R $UI_ROOT/elements ./pubsense/app-pubsense-box
cp -R $UI_ROOT/scripts ./pubsense/app-pubsense-box

zip -r ./pubsense.zip ./pubsense

popd

cp ./tmp/pubsense.zip ./

rm -rf ./tmp

exit
