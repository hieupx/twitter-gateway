package com.senseinfosys.pubsense.gateway;

import com.senseinfosys.pubsense.gateway.domain.twitter.TwitterConnectorService;
import com.senseinfosys.pubsense.gateway.domain.user.UserInitializer;
import com.senseinfosys.pubsense.gateway.infrastructure.RestClient;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class PubsenseGatewayApplication {

    public static void main(String... args) {
        try {
            SpringApplication.run(PubsenseGatewayApplication.class, args);
        } catch (Exception e) {
            System.out.println("\nGateway encountered exception during startup: " + e.getMessage());
            System.out.println("Cannot start Gateway, see more in file: log/spring/spring.log");
            throw e;
        }
        System.out.println("  .   ____          _            __ _ _\n" +
                " /\\\\ / ___'_ __ _ _(_)_ __  __ _ \\ \\ \\ \\\n" +
                "( ( )\\___ | '_ | '_| | '_ \\/ _` | \\ \\ \\ \\\n" +
                " \\\\/  ___)| |_)| | | | | || (_| |  ) ) ) )\n" +
                "  '  |____| .__|_| |_|_| |_\\__, | / / / /\n" +
                " =========|_|==============|___/=/_/_/_/\n" +
                " :: Gateway Booted :: ");

    }

    @Bean
    CommandLineRunner init(UserInitializer userInitializer, TwitterConnectorService twitterConnectorService) {
        return args -> {
            userInitializer.populateUsersAndRoles();
            RestClient.init();

            // start scheduling of existing connectors
            twitterConnectorService.scheduleAll();
        };
    }

}
