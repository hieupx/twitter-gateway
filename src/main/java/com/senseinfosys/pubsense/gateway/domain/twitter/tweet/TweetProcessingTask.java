package com.senseinfosys.pubsense.gateway.domain.twitter.tweet;

import com.senseinfosys.pubsense.gateway.domain.core.DomainException;
import com.senseinfosys.pubsense.gateway.domain.klaver.KlaverException;
import com.senseinfosys.pubsense.gateway.domain.klaver.KlaverService;
import com.senseinfosys.pubsense.gateway.domain.translator.TranslatorService;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.Keyword;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.Topic;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.TopicStatusService;
import com.senseinfosys.pubsense.gateway.infrastructure.monitor.MonitorService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by toan on 9/9/16.
 */
final class TweetProcessingTask implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(TweetProcessingTask.class);

    private final TranslatorService translatorService;

    private final TopicStatusService topicStatusService;

    private final Collection<Tweet> tweets;

    private final Topic topic;

    private final Keyword keyword;

    private final KlaverService klaverService;

    private final Tweet.SearchType searchType;

    private final MonitorService monitorService;

    TweetProcessingTask(Collection<Tweet> tweets,
                        Tweet.SearchType searchType,
                        Keyword keyword,
                        Topic topic,
                        TranslatorService translatorService,
                        TopicStatusService topicStatusService,
                        KlaverService klaverService,
                        MonitorService monitorService) {
        this.tweets = tweets;
        this.searchType = searchType;
        this.keyword = keyword;
        this.topic = topic;
        this.translatorService = translatorService;
        this.topicStatusService = topicStatusService;
        this.klaverService = klaverService;
        this.monitorService = monitorService;
    }

    @Override
    public void run() {

        monitorService.monitor();

        tweets.forEach(tweet -> tweet.convert(searchType, keyword, topic.getTwitterConnector()));

        if (topic.getMsTranslatorConnector() != null) {
            Collection<Tweet> nonEnglishTweets = tweets
                    .stream()
                    .filter(tweet -> !"en".equals(tweet.getLang()))
                    .collect(Collectors.toList());

            translateToEnglish(nonEnglishTweets);
        }

        try {
            klaverService.postTweets(tweets, topic, keyword);
        } catch (KlaverException e) {
            topicStatusService.saveErrorStatus(e, topic, keyword);
            LOG.error("Error processing tweets of keyword:" + keyword.toString() + ", topic: " + topic.toString() + " due to " + e.getMessage(), e);
        }

    }

    private void translateToEnglish(Collection<Tweet> nonEnglishTweets) {
        Map<String, List<Tweet>> tweetByLang = nonEnglishTweets
                .stream()
                .filter(tweet -> StringUtils.isNoneBlank(tweet.getTextRaw()))
                .collect(Collectors.groupingBy(Tweet::getLang));

        tweetByLang.forEach((lang, tweets) -> {
            List<String> texts = tweets
                    .stream()
                    .map(Tweet::getTextRaw)
                    .collect(Collectors.toList());
            try {
                List<String> enTexts = translatorService.translateToEnglish(texts, topic.getMsTranslatorConnector());
                for (int i = 0; i < tweets.size(); i++) {
                    Tweet tweet = tweets.get(i);
                    tweet.textEng(enTexts.get(i).toLowerCase());
                }
            } catch (DomainException e) {
                topicStatusService.saveWarningStatus(e, topic, keyword);
                LOG.error(e.getMessage(), e);
            }
        });
    }

}
