package com.senseinfosys.pubsense.gateway.domain.twitter.tweet;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.senseinfosys.pubsense.gateway.domain.twitter.TwitterConnector;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.Keyword;
import twitter4j.GeoLocation;
import twitter4j.HashtagEntity;
import twitter4j.Status;
import twitter4j.User;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Tweet {

    @JsonIgnore
    private Status status;

    @JsonProperty("id")
    private long id;

    @JsonProperty("topic_id")
    private String topicId;

    @JsonProperty("topic")
    private String topic;

    @JsonProperty("data_connector_id")
    private String dataConnectorId;

    @JsonProperty("data_source")
    private String dataSource;

    @JsonProperty("query_id")
    private String keywordId;

    @JsonProperty("query")
    private String searchQuery;

    // screen_name
    @JsonProperty("user_name")
    private String userName;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    @JsonProperty("date")
    private Date date;

    @JsonProperty("geo_coord")
    private Coordinates geoCoordinates;

    @JsonProperty("geo_loc")
    private String userLocation;

    @JsonProperty("lang")
    private String lang;

    @JsonProperty("declared_query_lang")
    private String declaredQueryLang;

    @JsonProperty("favourites")
    private int favourites;

    @JsonProperty("text_raw")
    private String textRaw;

    @JsonProperty("text_eng")
    private String textEng;

    @JsonProperty("hash_tags")
    private List<String> hashTags;

    @JsonProperty("permanent_link")
    private String permanentLink;

    @JsonProperty("share_count")
    private int shareCount;

    @JsonIgnore
    private SearchType searchType;

    public long getId() {
        return id;
    }

    public String getTopicId() {
        return topicId;
    }

    public String getTopic() {
        return topic;
    }

    public String getDataConnectorId() {
        return dataConnectorId;
    }

    public String getDataSource() {
        return dataSource;
    }

    public String getKeywordId() {
        return keywordId;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public String getUserName() {
        return userName;
    }

    public Date getDate() {
        return date;
    }

    public Coordinates getGeoCoordinates() {
        return geoCoordinates;
    }

    public String getLang() {
        return lang;
    }

    public int getFavourites() {
        return favourites;
    }

    public String getTextRaw() {
        return textRaw;
    }

    public String getTextEng() {
        return textEng;
    }

    public List<String> getHashTags() {
        return hashTags;
    }

    public String getPermanentLink() {
        return permanentLink;
    }

    public SearchType getSearchType() {
        return searchType;
    }

    public int getShareCount() {
        return shareCount;
    }

    public String getDeclaredQueryLang() {
        return declaredQueryLang;
    }

    public Tweet() {
    }

    public Tweet(Status status) {
        this.status = status;
    }

    public Tweet id(long id) {
        this.id = id;
        return this;
    }

    public Tweet shareCount(int shareCount) {
        this.shareCount = shareCount;
        return this;
    }

    public Tweet keywordId(String keywordId) {
        this.keywordId = keywordId;
        return this;
    }

    public Tweet searchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
        return this;
    }

    public Tweet userName(User user) {
        if (user != null) {
            this.userName = user.getScreenName();
        }
        return this;
    }

    public Tweet date(Date date) {
        this.date = date;
        return this;
    }

    public Tweet lang(String lang) {
        this.lang = lang;
        return this;
    }

    private Tweet declaredQueryLang(String lang) {
        this.declaredQueryLang = lang;
        return this;
    }

    public Tweet favourites(int favourites) {
        this.favourites = favourites;
        return this;
    }

    public Tweet textRaw(String textRaw) {
        this.textRaw = textRaw;
        return this;
    }

    public Tweet textEng(String textEng) {
        this.textEng = textEng;
        return this;
    }

    public Tweet hashTag(HashtagEntity[] hashTags) {
        this.hashTags = Arrays.stream(hashTags).map(HashtagEntity::getText).collect(Collectors.toList());
        return this;
    }

    public Tweet topic(String topic) {
        this.topic = topic;
        return this;
    }

    public Tweet topicId(String topicId) {
        this.topicId = topicId;
        return this;
    }

    public Tweet dataConnectorId(String dataConnectorId) {
        this.dataConnectorId = dataConnectorId;
        return this;
    }

    public Tweet dataSource(String dataSource) {
        this.dataSource = dataSource;
        return this;
    }

    public Tweet geoCoordinates(GeoLocation geoLocation) {
        if (geoLocation != null) {
            this.geoCoordinates = new Coordinates(geoLocation.getLongitude(), geoLocation.getLatitude());
        }
        return this;
    }

    public Tweet userLocation(User user) {
        if (user != null) {
            this.userLocation = user.getLocation();
        }
        return this;
    }

    public Tweet buildPermanentLink() {
        this.permanentLink = "https://twitter.com/" + this.userName + "/status/" + this.id;
        return this;
    }

    public String getUserLocation() {
        return userLocation;
    }

    private static class Coordinates {

        private double[] coordinates;

        private final String type = "Point";

        public Coordinates() {
        }

        public Coordinates(double... coordinates) {
            this.coordinates = coordinates;
        }

        public double[] getCoordinates() {
            return coordinates;
        }

        public String getType() {
            return type;
        }

    }

    @Override
    public String toString() {
        return status != null ? status.getText() : textRaw;
    }

    public void convert(SearchType searchType, Keyword keyword, TwitterConnector connector) {
        this
                .id(status.getId())
                .keywordId(keyword.getRemoteId())
                .searchQuery(keyword.getSearchQuery())
                .declaredQueryLang(keyword.getLang())
                .topicId(keyword.getTopic().getRemoteId())
                .topic(keyword.getTopic().getDescription())
                .dataConnectorId(connector.getRemoteId())
                .dataSource(connector.name())
                .date(status.getCreatedAt())
                .favourites(status.getFavoriteCount())
                .lang(status.getLang())
                .userName(status.getUser())
                .textRaw(status.getText())
                .shareCount(status.getRetweetCount())
                .geoCoordinates(status.getGeoLocation())
                .userLocation(status.getUser())
                .hashTag(status.getHashtagEntities())
                .searchType(searchType)
                .buildPermanentLink();
    }

    private Tweet searchType(SearchType searchType) {
        this.searchType = searchType;
        return this;
    }

    public enum SearchType {
        RECENT,
        HISTORICAL
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tweet tweet = (Tweet) o;

        return id == tweet.id;

    }

    @Override
    public int hashCode() {
        return Long.hashCode(id);
    }
}
