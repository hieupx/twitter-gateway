package com.senseinfosys.pubsense.gateway.domain.twitter;

import com.senseinfosys.pubsense.gateway.domain.core.DomainException;
import twitter4j.TwitterException;

/**
 * Created by toan on 9/27/16.
 */
public class TweetException extends DomainException {

    protected TwitterException twitterException;

    public TweetException(String message, int code, TwitterException cause) {
        super(DomainType.TWEET, code, message, cause);
        this.twitterException = cause;
    }

    public TwitterException getTwitterException() {
        return twitterException;
    }
}
