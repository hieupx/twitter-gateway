package com.senseinfosys.pubsense.gateway.domain.twitter.topic;

import com.senseinfosys.pubsense.gateway.domain.app.AppRelatedDomainRepository;
import com.senseinfosys.pubsense.gateway.domain.twitter.TwitterConnector;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

interface TopicRepository extends AppRelatedDomainRepository<Topic> {

    @Query(value = "select * from twitter_topic t " +
            "where t.run_status != ?1 " +
            "and ((t.since <= ?2 and t.until >= ?2) or (t.since <= ?2 and (t.until is null))) " +
            "and t.twitter_connector_id = ?3 " +
            "and keywords_count > 0 " +
            "and t.id > ?4 " +
            "order by t.id asc " +
            "limit ?5",
            nativeQuery = true)
    List<Topic> findSomeToService(String excludedStatus, Date currentDate, long connectorId, long sinceId, long take);


    @Query(value = "select count(*) from twitter_topic t " +
            "where t.run_status != ?1 " +
            "and ((t.since <= ?2 and t.until >= ?2) or (t.since <= ?2 and (t.until is null))) " +
            "and t.twitter_connector_id = ?3 ",
            nativeQuery = true)
    Long countTopicsNeedServicing(String excludedStatus, Date currentDate, long connectorId);

    List<Topic> findByTwitterConnector(TwitterConnector connector);

}
