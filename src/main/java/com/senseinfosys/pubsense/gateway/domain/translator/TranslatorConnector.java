package com.senseinfosys.pubsense.gateway.domain.translator;

import com.senseinfosys.pubsense.gateway.domain.connector.Connector;

import javax.persistence.MappedSuperclass;

/**
 * Created by toan on 10/3/16.
 */

@MappedSuperclass
public abstract class TranslatorConnector extends Connector {
}
