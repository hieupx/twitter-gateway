package com.senseinfosys.pubsense.gateway.domain.twitter.scheduling;

import com.senseinfosys.pubsense.gateway.domain.core.DomainRepository;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.Topic;

import java.util.Collection;
import java.util.List;

/**
 * Created by toan on 9/8/16.
 */
public interface KeywordSchedulingInformationRepository extends DomainRepository<KeywordSchedulingInformation> {

    List<KeywordSchedulingInformation> findByTopicIn(Collection<Topic> topics);

    KeywordSchedulingInformation findByTopic(Topic topic);
}
