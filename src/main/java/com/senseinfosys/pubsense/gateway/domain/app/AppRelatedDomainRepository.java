package com.senseinfosys.pubsense.gateway.domain.app;

import com.senseinfosys.pubsense.gateway.domain.core.DomainRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Collection;
import java.util.List;

@NoRepositoryBean
public interface AppRelatedDomainRepository<E extends AppRelatedDomain> extends DomainRepository<E> {
    E findByRemoteIdAndApplication(String remoteId, Application application);

    List<E> findByRemoteIdIn(Collection<String> remoteIds);

    E findByRemoteId(String remoteId);
}
