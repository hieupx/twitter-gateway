package com.senseinfosys.pubsense.gateway.domain.translator.ms;

import com.senseinfosys.pubsense.gateway.domain.translator.TranslateException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.concurrent.*;

@Component
public class MsTranslatorBackoffManager {

    // This class aids the scheduler in determining if it should
    // continue to make translation requests.  As of now, there is
    // no monitored quota, but if we always receive errors from the
    // server, we shouldn't constantly keep trying.  For instance, if
    // user runs out of quota, likely the user will find out about the
    // problem and will require adding more funds to the account which is
    // not quick, so we should backoff for some period of time.

    // subscription key -> timestamp to stop backing off
    private ConcurrentMap<String, Date> backoffEndDateBySubscriptionKey = new ConcurrentHashMap<>();

    public final void backoffUntil(String subscriptionKey, Date until) {
        if (StringUtils.isBlank(subscriptionKey)) {
            throw new TranslateException(TranslateException.INVALID_SUBSCRIPTION_KEY);
        }
        backoffEndDateBySubscriptionKey.putIfAbsent(subscriptionKey, until);
    }

    public final boolean isBackoffNeeded(String subscriptionKey) {
        if (StringUtils.isBlank(subscriptionKey)) {
            throw new TranslateException(TranslateException.INVALID_SUBSCRIPTION_KEY);
        }

        // Backoff checking does not need to be perfectly synchronized w/ other threads
        // If we incorrectly backoff due to race condition, it's not a big deal.
        Date end = backoffEndDateBySubscriptionKey.get(subscriptionKey);
        if (end == null) {
            return false;
        }
        boolean backoffNeeded = (new Date().before(end));
        if (!backoffNeeded) {
            // backoff period expired, so remove the backoff restriction
            backoffEndDateBySubscriptionKey.remove(subscriptionKey);
        }
        return backoffNeeded;
    }

    public MsTranslatorBackoffManager() {
    }
}
