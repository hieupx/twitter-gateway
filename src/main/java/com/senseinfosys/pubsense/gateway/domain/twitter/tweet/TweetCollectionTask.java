package com.senseinfosys.pubsense.gateway.domain.twitter.tweet;

import com.senseinfosys.pubsense.gateway.domain.core.DomainException;
import com.senseinfosys.pubsense.gateway.domain.klaver.KlaverService;
import com.senseinfosys.pubsense.gateway.domain.translator.TranslatorService;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.*;
import com.senseinfosys.pubsense.gateway.infrastructure.annotation.ThreadSafeShared;
import com.senseinfosys.pubsense.gateway.infrastructure.date.DateUtils;
import com.senseinfosys.pubsense.gateway.infrastructure.id.IdFactory;
import com.senseinfosys.pubsense.gateway.infrastructure.monitor.MonitorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import static com.senseinfosys.pubsense.gateway.domain.twitter.scheduling.TwitterScheduler.TWITTER_SCHEDULER;

/**
 * Created by toan on 9/8/16.
 */
public final class TweetCollectionTask implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(TweetCollectionTask.class);

    private int order;

    private final String id = IdFactory.getUUID();

    private final KlaverService klaverService;

    @ThreadSafeShared("thread-safely shared resource")
    private final SearchTweetsWindowResource resource;

    private final int maxAllocatedRequests;

    private Topic topic;

    private Keyword keyword;

    private final TweetService tweetService;

    private final ExecutorService tweetProcessingExecutor;

    private final KeywordService keywordService;

    private final BlockingQueue<TweetCollectionTask> windowOverdueTasks;

    private final TranslatorService translatorService;

    private final TopicStatusService topicStatusService;

    private final int maxTweetsPerRequest;

    private CountDownLatch waitLatch;

    private CountDownLatch holdLatch;

    private MonitorService monitorService;

    public TweetCollectionTask(SearchTweetsWindowResource resource,
                               Topic topic,
                               Keyword keyword,
                               int maxAllocatedRequests,
                               int maxTweetsPerRequest,
                               TweetService tweetService,
                               KeywordService keywordService,
                               TranslatorService translatorService,
                               TopicStatusService topicStatusService,
                               KlaverService klaverService,
                               MonitorService monitorService, ExecutorService tweetProcessingExecutor,
                               BlockingQueue<TweetCollectionTask> windowOverdueTasks) {
        this.resource = resource;
        this.topic = topic;
        this.keyword = keyword;
        this.maxAllocatedRequests = maxAllocatedRequests;
        this.tweetService = tweetService;
        this.topicStatusService = topicStatusService;
        this.maxTweetsPerRequest = maxTweetsPerRequest;
        this.tweetProcessingExecutor = tweetProcessingExecutor;
        this.keywordService = keywordService;
        this.translatorService = translatorService;
        this.klaverService = klaverService;
        this.monitorService = monitorService;
        this.windowOverdueTasks = windowOverdueTasks;
    }

    @Override
    public void run() {
        try {
            long start = System.currentTimeMillis();
            waitForThePrecedingTasksCompletion();

            logStart();

            Tweet.SearchType searchType;
            if (keyword.shouldPerformRecentTweetsSearch()) {
                searchType = Tweet.SearchType.RECENT;
            } else {
                searchType = Tweet.SearchType.HISTORICAL;
            }

            List<Tweet> collectedTweets = tweetService.find(
                    keyword,
                    maxTweetsPerRequest,
                    resource.takeRequests(maxAllocatedRequests),
                    topic.getTwitterConnector(),
                    topic.getFindOriginalPosts(),
                    searchType
            );

            keyword.setCounts(collectedTweets.size());
            keyword.serviced();
            topic.serviced();
            topic.setUpdatedBy(TWITTER_SCHEDULER);
            keyword.updatedBy(TWITTER_SCHEDULER);

            keywordService.updateServiceFields(topic, keyword);
            topicStatusService.saveSuccessStatus(topic, keyword);

            tweetProcessingExecutor.execute(new TweetProcessingTask(
                    collectedTweets,
                    searchType,
                    keyword,
                    topic,
                    translatorService,
                    topicStatusService,
                    klaverService,
                    monitorService)
            );


            logEnd(start, searchType, collectedTweets);

        } catch (SearchTweetsWindowResourceException e) {
            putOffToNextWindow();
            logWarning(e);
        } catch (DomainException e) {
            logError(e);
        } catch (Exception e) {
            LOG.error("Error collecting tweets for keyword:" + keyword.toString() + ", topic: " + topic.toString() + " due to " + e.getMessage(), e);
        } finally {
            notifyTheNextTaskToStart();
        }
    }

    private void logStart() {
        LOG.info("ORDER: {}, TASK-ID:{} Start collecting tweets for keyword: '{}' of topic: '{}' at: '{}' ", order, id, keyword.getSearchQuery(),
                topic.getDescription(), DateUtils.isoDateTimeFormat(System.currentTimeMillis()), System.currentTimeMillis());
    }

    private void logEnd(long startTime, Tweet.SearchType searchType, List<Tweet> collectedTweets) {
        LOG.info("ORDER: {}, TASK-ID:{} - Finish collecting tweets for keyword: '{}' of topic: '{}' at: '{}', elapsed-time: {}ms, tweet count: {}",
                order, id, keyword.getSearchQuery(), topic.getDescription(),
                DateUtils.isoDateTimeFormat(System.currentTimeMillis()),
                System.currentTimeMillis() - startTime,
                collectedTweets.size(), searchType);
    }

    private void logError(DomainException e) {
        TopicStatus error = topicStatusService.saveErrorStatus(e, topic, keyword);
        LOG.error(e.getMessage() + " " + error.getStatusContent(), e);
    }

    private void notifyTheNextTaskToStart() {
        if (holdLatch != null) {
            holdLatch.countDown();
        }
    }

    private void waitForThePrecedingTasksCompletion() {
        if (waitLatch != null) {
            // wait for the preceding task to finish, timeout is 60s
            LOG.info("[AWAIT] Keyword: " + topic.getId() + ", ORDER: " + order);
            try {
                waitLatch.await(60 * order, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
            }
        }
    }

    private void putOffToNextWindow() {
        windowOverdueTasks.offer(this);
    }

    private void logWarning(SearchTweetsWindowResourceException e) {
        TopicStatus warningStatus = topicStatusService.saveWarningStatus(resource, topic, keyword);
        LOG.warn(e.getMessage() + " " + warningStatus.getStatusContent(), e);
    }

    public TweetCollectionTask setWaitLatch(CountDownLatch waitLatch) {
        this.waitLatch = waitLatch;
        return this;
    }

    public Topic getTopic() {
        return topic;
    }

    public Keyword getKeyword() {
        return keyword;
    }

    public void setHoldLatch(CountDownLatch holdLatch) {
        this.holdLatch = holdLatch;
    }

    public CountDownLatch getHoldLatch() {
        return holdLatch;
    }

    public void order(int order) {
        this.order = order;
    }
}
