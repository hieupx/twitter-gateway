package com.senseinfosys.pubsense.gateway.domain.twitter.scheduling;

import com.senseinfosys.pubsense.gateway.domain.core.Domain;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.Topic;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "twitter_keyword_scheduling_info")
public class KeywordSchedulingInformation extends Domain {

    @OneToOne
    private Topic topic;

    private long lastServicedKeywordId;

    public KeywordSchedulingInformation() {
    }

    public KeywordSchedulingInformation(Topic topic, long lastServicedKeywordId) {
        this.topic = topic;
        this.lastServicedKeywordId = lastServicedKeywordId;
    }

    public long getLastServicedKeywordId() {
        return lastServicedKeywordId;
    }

    public void setLastServicedKeywordId(long lastServicedKeywordId) {
        this.lastServicedKeywordId = lastServicedKeywordId;
    }

    public Topic getTopic() {

        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }
}
