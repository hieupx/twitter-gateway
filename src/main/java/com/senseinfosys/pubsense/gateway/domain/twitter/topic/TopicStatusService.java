package com.senseinfosys.pubsense.gateway.domain.twitter.topic;

import com.senseinfosys.pubsense.gateway.domain.core.DomainException;
import com.senseinfosys.pubsense.gateway.domain.twitter.tweet.SearchTweetsWindowResource;
import javaslang.Tuple;
import javaslang.Tuple2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.senseinfosys.pubsense.gateway.domain.twitter.scheduling.TwitterScheduler.TWITTER_SCHEDULER;

/**
 * Created by toan on 9/27/16.
 */

@Service
public class TopicStatusService {

    @Autowired
    private TopicStatusRepository topicStatusRepository;

    @Autowired
    private TopicService topicService;

    public List<Tuple2<Topic, TopicStatus>> findLatest(Collection<String> topicIds) {
        if (CollectionUtils.isEmpty(topicIds)) {
            return new ArrayList<>(0);
        }
        List<Topic> topics = topicService.findByRemoteIds(topicIds);
        return topics
                .stream()
                .map(topic -> latestStatus(topic))
                .collect(Collectors.toList());
    }

    private Tuple2<Topic, TopicStatus> latestStatus(Topic topic) {
        TopicStatus status = topicStatusRepository.findFirstByTopicIdOrderByCreatedAtDesc(topic.getId());
        if (status == null) {
            status = new TopicStatus(topic).asNotStarted();
        }
        return Tuple.of(topic, status);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public TopicStatus saveErrorStatus(DomainException e, Topic topic, Keyword keyword) {
        TopicStatus status = new TopicStatus(topic, keyword).asError(e, topic);
        status.createdBy(TWITTER_SCHEDULER);
        topicStatusRepository.save(status);
        return status;
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void saveSuccessStatus(Topic topic, Keyword keyword) {
        TopicStatus status = new TopicStatus(topic, keyword).asSuccess();
        status.createdBy(TWITTER_SCHEDULER);
        topicStatusRepository.save(status);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public TopicStatus saveWarningStatus(SearchTweetsWindowResource resource, Topic topic, Keyword keyword) {
        TopicStatus status = new TopicStatus(topic, keyword).asWarning(resource);
        status.createdBy(TWITTER_SCHEDULER);
        return topicStatusRepository.save(status);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public TopicStatus saveWarningStatus(DomainException e, Topic topic, Keyword keyword) {
        TopicStatus status = new TopicStatus(topic, keyword).asWarning(e, topic);
        status.createdBy(TWITTER_SCHEDULER);
        return topicStatusRepository.save(status);
    }

    public void deleteByTopic(Topic topic) {
        List<TopicStatus> statuses = topicStatusRepository.findByTopicId(topic.getId());
        topicStatusRepository.delete(statuses);
    }

    public void deleteByKeywords(Set<Keyword> keywords) {
        if (!CollectionUtils.isEmpty(keywords)) {
            List<TopicStatus> statuses = topicStatusRepository.findByKeywordIdIn(keywords.stream().map(Keyword::getId).collect(Collectors.toList()));
            topicStatusRepository.delete(statuses);
        }
    }
}
