package com.senseinfosys.pubsense.gateway.domain.twitter.topic;

import com.senseinfosys.pubsense.gateway.domain.app.AppRelatedDomainRepository;

import java.util.List;

interface KeywordRepository extends AppRelatedDomainRepository<Keyword> {

    List<Keyword> findByTopic(Topic topic);

    List<Keyword> findByTopicIn(List<Topic> topics);

}
