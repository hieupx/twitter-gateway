package com.senseinfosys.pubsense.gateway.domain.twitter.scheduling;

import com.senseinfosys.pubsense.gateway.domain.twitter.TwitterConnector;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.Topic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by toan on 10/19/16.
 */

@Service
public class SchedulingInformationService {

    @Autowired
    KeywordSchedulingInformationRepository keywordSchedulingInformationRepository;

    @Autowired
    TopicSchedulingInformationRepository topicSchedulingInformationRepository;

    public void deleteTopicSchedulingInformation(TwitterConnector twitterConnector) {
        TopicSchedulingInformation topicSchedulingInformation = topicSchedulingInformationRepository.findByTwitterConnector(twitterConnector);
        if (topicSchedulingInformation != null) {
            topicSchedulingInformationRepository.delete(topicSchedulingInformation);
        }
    }

    public void deleteKeywordSchedulingInformation(Topic topic) {
        KeywordSchedulingInformation keywordSchedulingInformation = keywordSchedulingInformationRepository.findByTopic(topic);
        if (keywordSchedulingInformation != null) {
            keywordSchedulingInformationRepository.delete(keywordSchedulingInformation);
        }
    }
}
