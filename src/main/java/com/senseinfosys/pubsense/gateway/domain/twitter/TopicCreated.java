package com.senseinfosys.pubsense.gateway.domain.twitter;

import com.senseinfosys.pubsense.gateway.domain.twitter.topic.Topic;

/**
 * Created by toan on 11/11/16.
 */
public class TopicCreated extends TopicEvent {

    public TopicCreated(Topic topic) {
        super(topic);
    }
}
