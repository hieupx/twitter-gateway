package com.senseinfosys.pubsense.gateway.domain.translator.ms;

import com.senseinfosys.pubsense.gateway.domain.app.Application;
import com.senseinfosys.pubsense.gateway.domain.connector.Connector;
import com.senseinfosys.pubsense.gateway.domain.connector.ConnectorSupport;
import com.senseinfosys.pubsense.gateway.domain.core.DomainService;
import com.senseinfosys.pubsense.gateway.domain.translator.ms.MsTranslatorClientCache;
import com.senseinfosys.pubsense.gateway.domain.translator.ms.MsTranslatorConnector;
import com.senseinfosys.pubsense.gateway.domain.translator.ms.MsTranslatorConnectorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by toan on 10/5/16.
 */

@Service
public class MsTranslatorConnectorService extends DomainService implements ConnectorSupport {

    @Autowired
    private MsTranslatorConnectorRepository msTranslatorConnectorRepository;

    @Autowired
    private MsTranslatorClientCache msTranslatorClientCache;


    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void delete(String remoteId, Application app) {
        MsTranslatorConnector msConnector = msTranslatorConnectorRepository.findByRemoteIdAndApplication(remoteId, app);
        ensureConnectorExists(msConnector);
        msTranslatorConnectorRepository.delete(msConnector);
        msTranslatorClientCache.remove(msConnector);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Connector create(MsTranslatorConnector connector) {
        MsTranslatorConnector existingConnector = msTranslatorConnectorRepository
                .findByRemoteIdAndApplication(connector.getRemoteId(), connector.getApplication());
        ensureConnectorNotExisting(existingConnector);
        connector.createdBy(getCurrentAppId());
        MsTranslatorConnector msTranslatorConnector = msTranslatorConnectorRepository.save(connector);
        return msTranslatorConnector;
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Connector update(MsTranslatorConnector msTranslatorConnector) {
        MsTranslatorConnector existingConnector = msTranslatorConnectorRepository
                .findByRemoteIdAndApplication(msTranslatorConnector.getRemoteId(), msTranslatorConnector.getApplication());
        ensureConnectorExists(existingConnector);
        existingConnector.update(msTranslatorConnector, getCurrentAppId());
        MsTranslatorConnector updatedMsTranslatorConnector = msTranslatorConnectorRepository.save(existingConnector);
        msTranslatorClientCache.remove(updatedMsTranslatorConnector);
        return updatedMsTranslatorConnector;
    }

    public MsTranslatorConnector find(String msTranslatorId, Application app) {
        if (msTranslatorId == null) {
            return null;
        }
        return msTranslatorConnectorRepository.findByRemoteIdAndApplication(msTranslatorId, app);
    }
}
