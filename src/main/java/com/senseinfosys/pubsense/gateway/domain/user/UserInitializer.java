package com.senseinfosys.pubsense.gateway.domain.user;

import com.senseinfosys.pubsense.gateway.domain.user.Role.RoleName;
import com.senseinfosys.pubsense.gateway.infrastructure.id.IdFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Collection;

@Service
public class UserInitializer {

    private UserService userService;

    private UserRepository userRepository;

    private RoleRepository roleRepository;

    @Autowired
    public UserInitializer(UserRepository userRepository, RoleRepository roleRepository, UserService userService) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.userService = userService;
    }

    @Transactional
    public void populateUsersAndRoles() {
        if (userRepository.findByUsername("gateway_admin") == null && userRepository.findByUsername("gateway_user") == null) {

            Role adminRole = new Role(RoleName.ROLE_ADMIN);
            Role userRole = new Role(RoleName.ROLE_USER);
            adminRole.createdBy(IdFactory.getUUID());
            userRole.createdBy(adminRole.getCreatedBy());
            Collection<Role> roles = Arrays.asList(adminRole, userRole);

            roleRepository.save(roles);

            User admin = new User("gateway_admin", "ch894ae3d9JVcTESjY6BiKKeSJeEiM1N", Arrays.asList(adminRole));
            User user = new User("gateway_user", "gg4W9W8j29pny5JiJk3IlXJ97XpFC088", Arrays.asList(adminRole));
            admin.createdBy(adminRole.getCreatedBy());
            user.createdBy(adminRole.getCreatedBy());
            userService.save(admin);
            userService.save(user);
        }
    }

}
