package com.senseinfosys.pubsense.gateway.domain.twitter;

import com.senseinfosys.pubsense.gateway.domain.connector.ConnectorException;
import javaslang.Tuple;
import javaslang.Tuple2;
import org.springframework.stereotype.Component;
import twitter4j.HttpResponseCode;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Component
public final class TwitterClientCache {

    private final ConcurrentMap<String, Tuple2<TwitterConnector, Twitter>> twitterByConsumerKey = new ConcurrentHashMap<>();

    public void remove(TwitterConnector connector) {
        twitterByConsumerKey.remove(connector.getConsumerKey());
    }

    public Twitter get(TwitterConnector connector) {
        Tuple2<TwitterConnector, Twitter> tuple = twitterByConsumerKey.get(connector.getConsumerKey());
        if (tuple == null) {
            Configuration conf = new ConfigurationBuilder().setApplicationOnlyAuthEnabled(true).build();
            Twitter client = new TwitterFactory(conf).getInstance();
            client.setOAuthConsumer(connector.getConsumerKey(), connector.getConsumerSecret());
            try {
                client.getOAuth2Token();
            } catch (TwitterException e) {
                if (e.getStatusCode() == HttpResponseCode.FORBIDDEN || e.getStatusCode() == HttpResponseCode.UNAUTHORIZED) {
                    throw new ConnectorException(ConnectorException.INVALID_CREDENTIALS, "Invalid twitter connector", e);
                } else {
                    throw new ConnectorException(ConnectorException.UNKNOWN_ERROR, e.getMessage(), e);
                }
            }
            tuple = Tuple.of(connector, client);
            Tuple2<TwitterConnector, Twitter> previousTuple = twitterByConsumerKey.putIfAbsent(connector.getConsumerKey(), tuple);
            if (previousTuple != null) {
                // another thread snuck in and did the same work, make sure the same client is used
                tuple = previousTuple;
            }
        }
        return tuple._2;
    }

}
