package com.senseinfosys.pubsense.gateway.domain.twitter.tweet;

import com.senseinfosys.pubsense.gateway.domain.twitter.TweetException;
import twitter4j.TwitterException;

public class SearchTweetsException extends TweetException {

    public SearchTweetsException(String message, TwitterException cause) {
        super(message, cause.getErrorCode(), cause);
    }
}
