package com.senseinfosys.pubsense.gateway.domain.app;

import com.senseinfosys.pubsense.gateway.domain.core.Domain;
import com.senseinfosys.pubsense.gateway.infrastructure.id.IdFactory;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Application extends Domain {

    public static final int SECRET_LENGTH = 32;

    @Column(unique = true, length = 50, nullable = false)
    private String clientId;

    @Column(unique = true, length = 50, nullable = false)
    private String appId;

    @Column(length = 50, nullable = false)
    private String appSecret;

    public Application() {
    }

    public Application(String appSecret, String clientId) {
        this.appId = IdFactory.getUUID();
        this.appSecret = appSecret;
        this.clientId = clientId;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public static int getSecretLength() {
        return SECRET_LENGTH;
    }

    public String getClientId() {
        return clientId;
    }

    public String getAppId() {
        return appId;
    }

    @Override
    public Application createdBy(String createdBy) {
        super.createdBy(createdBy);
        return this;
    }

}
