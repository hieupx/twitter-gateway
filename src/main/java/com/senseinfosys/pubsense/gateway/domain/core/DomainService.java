package com.senseinfosys.pubsense.gateway.domain.core;

import com.senseinfosys.pubsense.gateway.domain.twitter.topic.Topic;
import org.springframework.beans.factory.annotation.Autowired;

import com.senseinfosys.pubsense.gateway.infrastructure.security.AuthenticationService;

import java.util.List;

public abstract class DomainService {

    @Autowired
    protected AuthenticationService authenticationService;

    public String getCurrentAppId() {
        return authenticationService.getCurrentAppId();
    }

    public String getCurrentUserId() {
        return authenticationService.getCurrentUserId();
    }

}
