package com.senseinfosys.pubsense.gateway.domain.user;

import com.senseinfosys.pubsense.gateway.domain.core.DomainRepository;

interface UserRepository extends DomainRepository<User> {

    public User findByUsername(String username);

    public User findByUsernameAndPassword(String username, String password);

}
