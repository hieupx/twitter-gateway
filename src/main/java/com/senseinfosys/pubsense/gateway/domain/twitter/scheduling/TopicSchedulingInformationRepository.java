package com.senseinfosys.pubsense.gateway.domain.twitter.scheduling;

import com.senseinfosys.pubsense.gateway.domain.core.DomainRepository;
import com.senseinfosys.pubsense.gateway.domain.twitter.TwitterConnector;

/**
 * Created by toan on 9/7/16.
 */
public interface TopicSchedulingInformationRepository extends DomainRepository<TopicSchedulingInformation> {

    TopicSchedulingInformation findByTwitterConnector(TwitterConnector connector);

}
