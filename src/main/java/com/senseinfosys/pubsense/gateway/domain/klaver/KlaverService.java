package com.senseinfosys.pubsense.gateway.domain.klaver;

import com.github.krukow.clj_lang.PersistentHashMap;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.senseinfosys.pubsense.gateway.domain.app.Application;
import com.senseinfosys.pubsense.gateway.domain.connector.ConnectorSupport;
import com.senseinfosys.pubsense.gateway.domain.core.DomainService;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.Keyword;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.Topic;
import com.senseinfosys.pubsense.gateway.domain.twitter.tweet.Tweet;
import com.senseinfosys.pubsense.gateway.infrastructure.json.JsonUtils;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.stream.Collectors;

import static com.senseinfosys.pubsense.gateway.domain.klaver.KlaverException.NO_KLAVER_CONNECTOR;
import static com.senseinfosys.pubsense.gateway.domain.klaver.KlaverException.POST_TWEETS_ERROR;

/**
 * Created by toan on 10/4/16.
 */
@Service
public class KlaverService extends DomainService implements ConnectorSupport {

    private static final Logger LOG = LoggerFactory.getLogger(KlaverService.class);

    private KlaverConnectorRepository klaverConnectorRepository;

    private String tweetsUrl;

    @Autowired
    public KlaverService(KlaverConnectorRepository klaverConnectorRepository, Environment env) {
        this.klaverConnectorRepository = klaverConnectorRepository;
        this.tweetsUrl = env.getProperty("klaver.tweets.url");
    }

    @Transactional
    public KlaverConnector create(KlaverConnector klaverConnector) {
        klaverConnector.createdBy(getCurrentAppId());
        return klaverConnectorRepository.save(klaverConnector);
    }

    @Transactional
    public void delete(String remoteId, Application app) {
        KlaverConnector connector = klaverConnectorRepository.findByRemoteIdAndApplication(remoteId, app);
        if (connector != null) {
            klaverConnectorRepository.delete(connector);
        }
    }

    public KlaverConnector find(String klaverConnectorId, Application app) {
        return klaverConnectorRepository.findByRemoteIdAndApplication(klaverConnectorId, app);
    }

    public void postTweets(Collection<Tweet> tweets, Topic topic, Keyword keyword) {
        if (topic.getKlaverConnector() == null) {
            throw new KlaverException(NO_KLAVER_CONNECTOR, topic + " doesn't have Klaver connector.")
                    .customMessage("No Klaver connector.");
        }
        HttpResponse<String> res = null;
        try {
            res = Unirest
                    .post(tweetsUrl)
                    .header("apiKey", topic.getKlaverConnector().getToken())
                    .header("Accept", "application/json")
                    .header("Content-Type", "application/json")
                    .body(PersistentHashMap.create("data", tweets))
                    .asString();

            logInfo(tweets, topic, keyword, res);
            logDebug(tweets, topic, keyword, res);
            logError(tweets, topic, keyword, res);

        } catch (UnirestException e) {
            logError(tweets, topic, keyword, res);
            throw new KlaverException(POST_TWEETS_ERROR, e.getMessage(), e).customMessage("Error posting tweets to Klaver.");
        }
    }

    private void logError(Collection<Tweet> tweets, Topic topic, Keyword keyword, HttpResponse<String> res) {
        String fullJson = JsonUtils.serialize(tweets);
        if (res == null) {
            LOG.error("Error posting {} tweets produced by keyword: {} and topic: {}. \nFull tweet json: {}",
                    tweets.size(),
                    keyword.toString(),
                    topic.toString(),
                    fullJson);
        } else if (res.getStatus() >= HttpStatus.SC_BAD_REQUEST) {
            LOG.error("Error posting {} tweets produced by keyword: {} and topic: {}. \n\n\n\n\nFull tweet json: {}. \n\n\n\n\nKlaver's response: status: {}, body: {}",
                    tweets.size(),
                    keyword.toString(),
                    topic.toString(),
                    fullJson,
                    res.getStatus(),
                    res.getBody());
        }
    }

    private void logDebug(Collection<Tweet> tweets, Topic topic, Keyword keyword, HttpResponse<String> res) {
        if (LOG.isDebugEnabled()) {
            String fullJson = JsonUtils.serialize(tweets);
            LOG.debug("Posting {} tweets produced by keyword: {} and topic: {}. \nFull tweet json: {}. \nKlaver's response: status: {}, body: {}",
                    tweets.size(),
                    keyword.toString(),
                    topic.toString(),
                    fullJson,
                    res.getStatus(),
                    res.getBody());
        }
    }

    private void logInfo(Collection<Tweet> tweets, Topic topic, Keyword keyword, HttpResponse<String> res) {
        String sampleJson = JsonUtils.serialize(tweets.stream().limit(5).collect(Collectors.toList()));
        LOG.info("Posting {} tweets produced by keyword: {}, topic: {}. \nSample tweet json: {}. \nKlaver's response: status: {}, body: {}",
                tweets.size(),
                keyword.toString(),
                topic.toString(),
                sampleJson,
                res.getStatus(),
                res.getBody());
    }
}
