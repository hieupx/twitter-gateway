package com.senseinfosys.pubsense.gateway.domain.connector;

import com.senseinfosys.pubsense.gateway.domain.core.Domain;
import com.senseinfosys.pubsense.gateway.domain.core.DomainException;
import org.springframework.http.HttpStatus;

/**
 * Created by toan on 10/6/16.
 */
public interface ConnectorSupport {

    default void ensureConnectorExists(Domain existingConnector) {
        if (existingConnector == null) {
            throw new ConnectorException(DomainException.NOT_FOUND, "Connector not found.")
                    .status(HttpStatus.NOT_FOUND);
        }
    }

    default void ensureConnectorNotExisting(Domain existingConnector) {
        if (existingConnector != null) {
            throw new ConnectorException(DomainException.ALREADY_EXISTS, "Connector existed.")
                    .status(HttpStatus.BAD_REQUEST);
        }
    }

}
