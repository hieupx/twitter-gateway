package com.senseinfosys.pubsense.gateway.domain.twitter.tweet;

import com.senseinfosys.pubsense.gateway.domain.twitter.TweetException;
import twitter4j.TwitterException;

import java.util.Date;

/**
 * Created by toan on 9/6/16.
 */
public class SearchTweetsWindowResourceException extends TweetException {


    public final Date ocurredAt = new Date();

    public SearchTweetsWindowResourceException(String message, int code, TwitterException cause) {
        super(message, code, cause);
    }

    public SearchTweetsWindowResourceException(String message, int code) {
        this(message, code, null);
    }

    public SearchTweetsWindowResourceException(String message) {
        this(message, 0);
    }
}
