package com.senseinfosys.pubsense.gateway.domain.translator.ms;

import com.memetix.mst.web.Credentials;
import com.memetix.mst.web.ShareableClientContext;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by toan on 9/22/16.
 */

@Component
public class MsTranslatorClientCache {

    private final ConcurrentMap<String, ShareableClientContext> clientContextBySubscriptionKey = new ConcurrentHashMap<>();

    public void remove(MsTranslatorConnector connector) {
        clientContextBySubscriptionKey.remove(connector.getSubscriptionKey());
    }

    public ShareableClientContext get(MsTranslatorConnector connector) {
        ShareableClientContext context = clientContextBySubscriptionKey.get(connector.getSubscriptionKey());
        if (context == null) {
            context = new ShareableClientContext(new Credentials(connector.getSubscriptionKey()));
            ShareableClientContext previousContext = clientContextBySubscriptionKey.putIfAbsent(connector.getSubscriptionKey(), context);
            if (previousContext != null) {
                // another thread did the same work
                context = previousContext;
            }
        }
        return context;
    }

}
