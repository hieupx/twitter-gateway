package com.senseinfosys.pubsense.gateway.domain.connector;

import com.senseinfosys.pubsense.gateway.domain.app.Application;
import com.senseinfosys.pubsense.gateway.domain.app.AppRelatedDomain;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class Connector extends AppRelatedDomain {

    @Enumerated(value = EnumType.STRING)
    protected ConnectorType type;

    public ConnectorType getType() {
        return type;
    }

    @Override
    public Connector application(Application application) {
        return (Connector) super.application(application);
    }

    public enum ConnectorType {

        KLAVER("klaver"),

        TWITTER("twitter"),

        MS_TRANSLATOR("ms_translator");

        private final String value;

        private ConnectorType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public String toString() {
            return value;
        }

        public static ConnectorType fromString(String name) {
            try {
                return valueOf(name);
            } catch (Exception e) {
                return null;
            }
        }
    }

    @Override
    public Connector remoteId(String remoteId) {
        return (Connector) super.remoteId(remoteId);
    }

    @Override
    public AppRelatedDomain and() {
        return this;
    }

    @Override
    public AppRelatedDomain then() {
        return this;
    }

    public Connector type(ConnectorType type) {
        this.type = type;
        return this;
    }
}
