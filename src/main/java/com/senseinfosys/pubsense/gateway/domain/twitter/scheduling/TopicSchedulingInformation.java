package com.senseinfosys.pubsense.gateway.domain.twitter.scheduling;

import com.senseinfosys.pubsense.gateway.domain.core.Domain;
import com.senseinfosys.pubsense.gateway.domain.twitter.TwitterConnector;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Created by toan on 9/6/16.
 */

@Entity
@Table(name = "twitter_topic_scheduling_info")
public class TopicSchedulingInformation extends Domain {

    private long lastServicedTopicId;

    private int lastAvailableRequests;

    private long lastWindowResetTime;

    @OneToOne
    private TwitterConnector twitterConnector;

    public TopicSchedulingInformation() {
    }

    public TopicSchedulingInformation(long lastTopicIndex, TwitterConnector twitterConnector) {
        this.lastServicedTopicId = lastTopicIndex;
        this.twitterConnector = twitterConnector;
    }

    public void setLastServicedTopicId(long lastServicedTopicId) {
        this.lastServicedTopicId = lastServicedTopicId;
    }

    public long getLastServicedTopicId() {
        return lastServicedTopicId;
    }

    public int getLastAvailableRequests() {
        return lastAvailableRequests;
    }

    public void setLastAvailableRequests(int lastAvailableRequests) {
        this.lastAvailableRequests = lastAvailableRequests;
    }

    public long getLastWindowResetTime() {
        return lastWindowResetTime;
    }

    public void setLastWindowResetTime(long lastWindowResetTime) {
        this.lastWindowResetTime = lastWindowResetTime;
    }

    public TwitterConnector getTwitterConnector() {
        return twitterConnector;
    }

    public void setTwitterConnector(TwitterConnector twitterConnector) {
        this.twitterConnector = twitterConnector;
    }
}
