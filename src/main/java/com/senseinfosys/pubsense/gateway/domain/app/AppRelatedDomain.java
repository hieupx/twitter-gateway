package com.senseinfosys.pubsense.gateway.domain.app;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.senseinfosys.pubsense.gateway.domain.core.Domain;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AppRelatedDomain extends Domain {

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false)
    @JsonIgnore
    protected Application application;

    @NotBlank
    protected String remoteId;

    public String getRemoteId() {
        return remoteId;
    }

    public AppRelatedDomain remoteId(String remoteId) {
        this.remoteId = remoteId;
        return this;
    }

    public Application getApplication() {
        return application;
    }

    public AppRelatedDomain application(Application application) {
        this.application = application;
        return this;
    }

}
