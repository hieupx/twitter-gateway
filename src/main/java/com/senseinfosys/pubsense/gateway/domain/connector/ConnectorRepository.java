package com.senseinfosys.pubsense.gateway.domain.connector;

import com.senseinfosys.pubsense.gateway.domain.app.AppRelatedDomainRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface ConnectorRepository<E extends Connector> extends AppRelatedDomainRepository<E> {

}
