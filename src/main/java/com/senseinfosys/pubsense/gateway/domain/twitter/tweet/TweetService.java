package com.senseinfosys.pubsense.gateway.domain.twitter.tweet;

import com.senseinfosys.pubsense.gateway.domain.twitter.TwitterConnector;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.Keyword;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by toan on 9/9/16.
 */
@Service
public class TweetService {

    private TweetRepository tweetRepository;

    @Autowired
    public TweetService(TweetRepository tweetRepository) {
        this.tweetRepository = tweetRepository;
    }


    public List<Tweet> find(Keyword keyword, int pageSize, int collectPages, TwitterConnector connector, Boolean findOriginalTweets, Tweet.SearchType searchType) {
        return tweetRepository.find(keyword, pageSize, collectPages, connector, findOriginalTweets, searchType);
    }
}
