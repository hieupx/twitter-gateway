package com.senseinfosys.pubsense.gateway.domain.connector;

import com.senseinfosys.pubsense.gateway.domain.app.AppService;
import com.senseinfosys.pubsense.gateway.domain.app.Application;
import com.senseinfosys.pubsense.gateway.domain.connector.Connector.ConnectorType;
import com.senseinfosys.pubsense.gateway.domain.core.Domain;
import com.senseinfosys.pubsense.gateway.domain.core.DomainService;
import com.senseinfosys.pubsense.gateway.domain.klaver.KlaverConnector;
import com.senseinfosys.pubsense.gateway.domain.klaver.KlaverService;
import com.senseinfosys.pubsense.gateway.domain.translator.ms.MsTranslatorConnectorService;
import com.senseinfosys.pubsense.gateway.domain.translator.ms.MsTranslatorConnector;
import com.senseinfosys.pubsense.gateway.domain.twitter.TwitterConnector;
import com.senseinfosys.pubsense.gateway.domain.twitter.TwitterConnectorService;
import com.senseinfosys.pubsense.gateway.web.connector.RequestConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

@Service
public class ConnectorService extends DomainService {

    @Autowired
    private AppService appService;

    @Autowired
    private Validator validator;

    @Autowired
    private TwitterConnectorService twitterConnectorService;

    @Autowired
    private MsTranslatorConnectorService msTranslatorConnectorService;

    @Autowired
    private KlaverService klaverService;

    public Connector create(RequestConnector requestConnector) {
        return createConnector(requestConnector);
    }

    public Connector update(RequestConnector requestConnector) {
        return updateConnector(requestConnector);
    }

    public void delete(String remoteId, String connectorType, String appId) {
        deleteConnector(remoteId, connectorType, appId);
    }

    private void deleteConnector(String remoteId, String connectorType, String appId) {
        ConnectorType type = ConnectorType.fromString(connectorType.toUpperCase());
        ensureConnectorTypeExists(type);
        Application app = appService.findByAppId(appId);
        switch (type) {
            case TWITTER:
                twitterConnectorService.delete(remoteId, app);
                return;
            case MS_TRANSLATOR:
                msTranslatorConnectorService.delete(remoteId, app);
                return;
            case KLAVER:
                klaverService.delete(remoteId, app);
                return;
        }
    }

    private Connector createConnector(RequestConnector requestConnector) {
        Connector connector = toConnector(requestConnector);
        validate(connector);
        switch (connector.getType()) {
            case TWITTER:
                return twitterConnectorService.create((TwitterConnector) connector);
            case MS_TRANSLATOR:
                return msTranslatorConnectorService.create((MsTranslatorConnector) connector);
            case KLAVER:
                return klaverService.create((KlaverConnector) connector);
            default:
                throw new ConnectorException(ConnectorException.INVALID_CONNECTOR, "Connector type not supported.")
                        .status(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    private Connector updateConnector(RequestConnector requestConnector) {
        Connector connector = toConnector(requestConnector);
        // we don't need validate(connector), @see TwitterConnector.update() and MsTranslatorConnector.update();
        switch (connector.getType()) {
            case TWITTER:
                return twitterConnectorService.update((TwitterConnector) connector);
            case MS_TRANSLATOR:
                return msTranslatorConnectorService.update((MsTranslatorConnector) connector);
            default:
                throw new ConnectorException(ConnectorException.INVALID_CONNECTOR, "Connector type not supported.")
                        .status(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    private void ensureConnectorTypeExists(ConnectorType type) {
        if (type == null) {
            throw new ConnectorException(ConnectorException.INVALID_CONNECTOR, "Connector type not supported.")
                    .status(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    private ConnectorType getConnectorType(RequestConnector requestConnector) {
        ConnectorType type = ConnectorType.fromString(requestConnector.getType());
        if (type == null) {
            throw new ConnectorException(ConnectorException.INVALID_CONNECTOR, "Connector type not supported.");
        }
        return type;
    }

    private void validate(Domain connector) {
        Set<ConstraintViolation<Domain>> constraintViolations = validator.validate(connector);
        if (!constraintViolations.isEmpty()) {
            throw new ConnectorException(ConnectorException.INVALID_CONNECTOR, "Invalid connector.")
                    .setConstraintViolations(constraintViolations);
        }
    }

    private Connector toConnector(RequestConnector requestConnector) {
        Application app = appService.findByAppId(requestConnector.getAppId());
        ConnectorType type = getConnectorType(requestConnector);
        return requestConnector.connector().application(app).type(type);
    }
}
