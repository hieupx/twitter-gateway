package com.senseinfosys.pubsense.gateway.domain.twitter.topic;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.senseinfosys.pubsense.gateway.domain.app.AppRelatedDomain;
import com.senseinfosys.pubsense.gateway.domain.app.Application;
import com.senseinfosys.pubsense.gateway.domain.klaver.KlaverConnector;
import com.senseinfosys.pubsense.gateway.domain.translator.ms.MsTranslatorConnector;
import com.senseinfosys.pubsense.gateway.domain.twitter.TwitterConnector;
import twitter4j.GeoLocation;
import twitter4j.Query;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "twitter_topic",
       indexes = {@Index(name = "idx_twitter_topic_run_status", columnList = "runStatus")},
       uniqueConstraints = {@UniqueConstraint(columnNames = {"application_id", "remoteId"})})
public class Topic extends AppRelatedDomain {

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "topic", targetEntity = Keyword.class, orphanRemoval = true)
    private Set<Keyword> keywords;

    private int keywordsCount;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = TwitterConnector.class)
    @JoinColumn(nullable = false)
    @JsonIgnore
    private TwitterConnector twitterConnector;

    // Important to keep the FK name the same via code or else someone auto-generating the schema/ddl
    // will generate a different FK name and possibly screw up our schema.sql and evolution scripts.
    @ManyToOne(fetch = FetchType.EAGER, targetEntity = MsTranslatorConnector.class)
    @JoinColumn(nullable = true, foreignKey = @ForeignKey(name="FK_topic_azure_translator_id"))
    @JsonIgnore
    // try a generic connector
    private MsTranslatorConnector msTranslatorConnector;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = KlaverConnector.class)
    private KlaverConnector klaverConnector;

    private String description;

    @Enumerated(value = EnumType.STRING)
    private RunStatus runStatus = RunStatus.STOPPED;

    private Date since;

    private Date until;

    private Double lat;

    private Double lon;

    private Double radius;

    private String timeZone;

    @Enumerated(value = EnumType.STRING)
    private DistanceUnit radiusUnit;

    private Boolean findNegativeEmoticons;

    private Boolean findPositiveEmoticons;

    private Boolean findQuestions;

    private Boolean findOriginalPosts;

    private volatile long servicedCount;

    private Date lastServiced;

    public Topic() {
    }

    public Set<Keyword> getKeywords() {
        if (keywords == null) {
            return Collections.emptySet();
        }
        return keywords;
    }

    public int getKeywordsCount() {
        return keywordsCount;
    }

    public String getDescription() {
        return description;
    }

    public static RunStatus getDerivedRunStatus(Date topicSince, Date topicUntil, RunStatus runStatus) {
        Date now = new Date();

        // hack to prevent many null checks
        Date since = topicSince == null ? new Date(Long.MIN_VALUE) : topicSince;
        Date until = topicUntil == null ? new Date(Long.MAX_VALUE) : topicUntil;

        if (runStatus == Topic.RunStatus.PAUSED) {
            return Topic.RunStatus.PAUSED;
        } else if (topicSince == null ||
                now.before(since) || // not started yet
                !now.before(until)) { // after data collection ended
            return Topic.RunStatus.STOPPED;
        } else if ((!now.before(since) && now.before(until)) || // in the range of data collection
                topicUntil == null) { // undefined stop time, collect data forever, using topicUntil instead of until to check to see if until date was unset
            return Topic.RunStatus.STARTED;
        } else {
            throw new TopicException(TopicException.UNKNOWN_ERROR, "Unknown RunStatus state.");
        }
    }

    public RunStatus getRunStatus() {
        return getDerivedRunStatus(since, until, runStatus);
    }

    public Date getSince() {
        return since;
    }

    public Date getUntil() {
        return until;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLon() {
        return lon;
    }

    public Double getRadius() {
        return radius;
    }

    public DistanceUnit getRadiusUnit() {
        return radiusUnit;
    }

    public Boolean getFindNegativeEmoticons() {
        return findNegativeEmoticons;
    }

    public Boolean getFindPositiveEmoticons() {
        return findPositiveEmoticons;
    }

    public Boolean getFindQuestions() {
        return findQuestions;
    }

    public Boolean getFindOriginalPosts() {
        return findOriginalPosts;
    }

    public TwitterConnector getTwitterConnector() {
        return twitterConnector;
    }

    public MsTranslatorConnector getMsTranslatorConnector() {
        return msTranslatorConnector;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public long getServicedCount() {
        return servicedCount;
    }

    public Date getLastServiced() {
        return lastServiced;
    }

    public Topic application(Application application) {
        return (Topic) super.application(application);
    }

    public Topic timeZone(String timeZone) {
        this.timeZone = timeZone;
        return this;
    }

    public Topic msTranslatorConnector(MsTranslatorConnector connector) {
        this.msTranslatorConnector = connector;
        return this;
    }

    public Topic setKeywordsCount(int keywordsCount) {
        this.keywordsCount = keywordsCount;
        return this;
    }

    public Topic twitterConnector(TwitterConnector connector) {
        this.twitterConnector = connector;
        return this;
    }

    @Override
    public Topic remoteId(String remoteId) {
        return (Topic) super.remoteId(remoteId);
    }

    public Topic keywords(Set<Keyword> keywords) {
        this.keywords = keywords;
        this.updateKeywordsCount();
        return this;
    }

    public Topic description(String description) {
        this.description = description;
        return this;
    }

    public Topic id(long id) {
        this.id = id;
        return this;
    }

    public Topic runStatus(String value) {
        if (value != null) {
            try {
                this.runStatus = RunStatus.valueOf(value);
            } catch (Exception e) {
                throw new TopicException(TopicException.RUN_STATUS_NOT_SUPPORTED, "Run status not supported.");
            }
        }
        return this;
    }

    public Topic since(Date since) {
        this.since = since;
        return this;
    }

    public Topic until(Date until) {
        this.until = until;
        return this;
    }

    public Topic lat(Double lat) {
        this.lat = lat;
        return this;
    }

    public Topic lon(Double lon) {
        this.lon = lon;
        return this;
    }

    public Topic radius(Double radius) {
        this.radius = radius;
        return this;
    }

    public KlaverConnector getKlaverConnector() {
        return klaverConnector;
    }

    @Override
    public Topic createdBy(String createdBy) {
        super.createdBy(createdBy);
        for (Keyword keyword : getKeywords()) {
            keyword.createdBy(createdBy);
        }
        return this;
    }

    @Override
    public Topic updatedBy(String updatedBy) {
        super.updatedBy(updatedBy);
        for (Keyword keyword : getKeywords()) {
            keyword.updatedBy(updatedBy);
        }
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        super.updatedBy(updatedBy);
    }

    public Topic radiusUnit(String radiusUnit) {
        if (radiusUnit == null) {
            return this;
        }
        try {
            this.radiusUnit = DistanceUnit.valueOf(radiusUnit);
            return this;
        } catch (Exception e) {
            throw new TopicException(TopicException.DISTANCE_UNIT_NOT_SUPPORTED, "Distance unit not supported.");
        }
    }

    public Topic findNegativeEmoticons(Boolean findNegativeEmotion) {
        this.findNegativeEmoticons = findNegativeEmotion;
        return this;
    }

    public Topic findPositiveEmoticons(Boolean findPositiveEmotion) {
        this.findPositiveEmoticons = findPositiveEmotion;
        return this;
    }

    public Topic findQuestions(Boolean findQuestions) {
        this.findQuestions = findQuestions;
        return this;
    }

    public Topic findOriginalPosts(Boolean findOriginalPosts) {
        this.findOriginalPosts = findOriginalPosts;
        return this;
    }

    public void updateKeywordsCount() {
        this.keywordsCount = keywords != null ? keywords.size() : 0;
    }

    // only valid in a window - start
    @Transient
    private List<Keyword> sortedKeywords;

    @Transient
    private int servicedKeywords;

    @Transient
    private boolean turnRelinquished;
    // only valid in a window - end

    public Keyword findKeywordToService(final long lastServicedKeywordId) {
        if (servicedKeywords == getKeywords().size() && !turnRelinquished) {
            // all keywords have been serviced in this window
            // to implement a fair service, the topic should relinquish and give the turn/request to other topics,
            // so they could have the chance to be serviced,
            // but do it only once in a window
            turnRelinquished = true;
            // no searchQuery visited
            return null;
        }
        // cache for later access
        if (sortedKeywords == null) {
            sortedKeywords = new ArrayList<>(getKeywords());
            sortedKeywords.sort((l, r) -> (l.getId() == r.getId() ? 0 : (l.getId() > r.getId()) ? 1 : -1));
        }
        servicedKeywords++;
        Optional<Keyword> first = sortedKeywords.stream().filter(k -> k.getId() > lastServicedKeywordId).findFirst();
        if (first.isPresent()) {
            return first.get();
        }
        // not found, just return the first
        return sortedKeywords.get(0);
    }

    public void serviced() {
        this.servicedCount++;
        this.lastServiced = new Date();
    }

    public Topic klaverConnector(KlaverConnector klaverConnector) {
        this.klaverConnector = klaverConnector;
        return this;
    }

    public boolean hasGeoCode() {
        return lat != null && lon != null && radius != null && radiusUnit != null;
    }

    public GeoLocation geoLoc() {
        return new GeoLocation(lat, lon);
    }

    public Query.Unit radiusUnit() {
        if (radiusUnit == DistanceUnit.MILES) {
            return Query.Unit.mi;
        }
        if (radiusUnit == DistanceUnit.KILOMETERS) {
            return Query.Unit.km;
        }
        return null;
    }

    public void updateServiceFields(Topic topic) {
        this.servicedCount = topic.servicedCount;
        this.lastServiced = topic.lastServiced;
        this.updatedAt = topic.updatedAt;
        this.updatedBy = topic.updatedBy;
    }

    public enum RunStatus {
        STARTED, PAUSED, STOPPED, NOT_STARTED
    }

    public enum DistanceUnit {
        KILOMETERS, MILES
    }

    public boolean searchOperatorsModified(Topic newTopic) {
        if (!Objects.equals(this.findNegativeEmoticons, newTopic.findNegativeEmoticons)) {
            return true;
        }
        if (!Objects.equals(this.findOriginalPosts, newTopic.findOriginalPosts)) {
            return true;
        }
        if (!Objects.equals(this.findPositiveEmoticons, newTopic.findPositiveEmoticons)) {
            return true;
        }
        if (!Objects.equals(this.findQuestions, newTopic.findQuestions)) {
            return true;
        }
        if (!Objects.equals(this.lat, newTopic.lat) && newTopic.hasGeoCode()) {
            return true;
        }
        if (!Objects.equals(this.lon, newTopic.lon) && newTopic.hasGeoCode()) {
            return true;
        }
        if (!Objects.equals(this.radiusUnit, newTopic.radiusUnit) && newTopic.hasGeoCode()) {
            return true;
        }
        if (!Objects.equals(this.radius, newTopic.radius) && newTopic.hasGeoCode()) {
            return true;
        }
        return false;
    }

    public void update(Topic topic, String updatedBy) {

        this.findNegativeEmoticons = topic.findNegativeEmoticons;
        this.findOriginalPosts = topic.findOriginalPosts;
        this.findPositiveEmoticons = topic.findPositiveEmoticons;
        this.findQuestions = topic.findQuestions;
        this.lat = topic.lat;
        this.lon = topic.lon;
        this.radiusUnit = topic.radiusUnit;
        this.radius = topic.radius;

        this.description = topic.description;
        this.msTranslatorConnector = topic.msTranslatorConnector;
        this.runStatus = topic.runStatus;
        this.since = topic.since;
        this.timeZone = topic.timeZone;
        this.twitterConnector = topic.twitterConnector;
        this.until = topic.until;
        this.klaverConnector = topic.klaverConnector;
        updatedBy(updatedBy);
        version++;
    }

    public void removeKeywords(Collection<Keyword> keywordsToDelete) {
        this.keywords.removeAll(keywordsToDelete);
    }

    public void addKeywords(Collection<Keyword> keywordsToCreate) {
        this.keywords.addAll(keywordsToCreate);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Topic topic = (Topic) o;

        return id == topic.id;

    }

    @Override
    public int hashCode() {
        return Long.hashCode(id);
    }

    @Override
    public String toString() {
        return "Topic{" +
                "id=" + id +
                ", uuid=" + remoteId +
                '}';
    }
}
