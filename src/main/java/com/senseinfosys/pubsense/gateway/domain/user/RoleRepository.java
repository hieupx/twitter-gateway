package com.senseinfosys.pubsense.gateway.domain.user;

import com.senseinfosys.pubsense.gateway.domain.core.DomainRepository;

interface RoleRepository extends DomainRepository<Role> {
}
