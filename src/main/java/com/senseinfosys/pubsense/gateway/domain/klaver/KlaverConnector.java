package com.senseinfosys.pubsense.gateway.domain.klaver;

import com.senseinfosys.pubsense.gateway.domain.connector.Connector;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Created by toan on 10/4/16.
 */
@Entity
@Table(
   uniqueConstraints = {@UniqueConstraint(columnNames = {"application_id", "remoteId"})}
)
public class KlaverConnector extends Connector {

    public static final String URL = "http://192.168.1.153/klaverrhcc/tweets";

    @Column(unique=true)
    private String token;

    public KlaverConnector() {

    }

    public KlaverConnector(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
