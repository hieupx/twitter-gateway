package com.senseinfosys.pubsense.gateway.domain.twitter.tweet;

import com.senseinfosys.pubsense.gateway.domain.twitter.TwitterConnector;
import com.senseinfosys.pubsense.gateway.infrastructure.annotation.EffectivelyImmutable;
import net.jcip.annotations.GuardedBy;
import net.jcip.annotations.ThreadSafe;
import org.joda.time.DateTimeConstants;
import twitter4j.RateLimitStatus;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by toan on 9/6/16.
 */

@ThreadSafe
public final class SearchTweetsWindowResource {

    private final Lock resourceLock = new ReentrantLock();

    @GuardedBy("resourceLock")
    private int remainingRequests;

    @EffectivelyImmutable("should be used like immutable")
    private final TwitterConnector connector;

    private final long resetTimeInMillis;

    private final long startTimeInMillis;

    private final int maxTakingRequests;

    private final int minTakingRequests;

    private final int maxRequestsPerInterval;

    private volatile boolean valid = true;

    private static final long MIN_WINDOW_LENGTH_IN_MS = DateTimeConstants.MILLIS_PER_MINUTE * 3L;

    public SearchTweetsWindowResource(TwitterConnector connector, RateLimitStatus rateLimitStatus, int maxRequestsPerInterval) {
        this.connector = connector;
        this.startTimeInMillis = System.currentTimeMillis();
        this.resetTimeInMillis = rateLimitStatus.getResetTimeInSeconds() * 1000L;
        this.maxRequestsPerInterval = maxRequestsPerInterval;
        this.remainingRequests = maxRequestsPerInterval;
        this.maxTakingRequests = 1;
        this.minTakingRequests = 1;
    }

    public boolean ensureValidResource() {
        return ensureValidResource(this.resetTimeInMillis, this.remainingRequests);
    }

    public static boolean ensureValidResource(long resetTimeInMillis, int requests) {
        if (requests == 0) {
            return false;
        }

        // make sure a window will last long enough
        if (resetTimeInMillis < System.currentTimeMillis() + MIN_WINDOW_LENGTH_IN_MS) {
            return false;
        }

        return true;
    }

    // Nobody outside this package can call this method
    int takeRequests(final int takingRequests) {
        if (!valid) {
            // prevent checking and mutating remainingRequests
            throw new SearchTweetsWindowResourceException("Window resource is invalid.");
        }
        validateTakingRequests(takingRequests);
        ensureValidWindow();
        int taken = takingRequests;
        resourceLock.lock();
        try {
            if (remainingRequests > 0) {
                if (remainingRequests >= takingRequests) {
                    remainingRequests -= takingRequests;
                } else {
                    taken = remainingRequests;
                    remainingRequests = 0;
                }
                return taken;
            } else {
                // prevent mutating remainingRequests
                this.valid = false;
                throw new SearchTweetsWindowResourceException("Trying to take: " + takingRequests + " requests, but none left.");
            }
        } finally {
            resourceLock.unlock();
        }
    }

    private void ensureValidWindow() {
        long leftMillis = resetTimeInMillis - System.currentTimeMillis();
        // Window's left time must be greater than 60s, to make sure the call succeed
        if (leftMillis < DateTimeConstants.MILLIS_PER_SECOND * 30L) {
            this.valid = false;
            throw new SearchTweetsWindowResourceException("Window is going to pass soon.");
        }
    }

    private void validateTakingRequests(int takingRequests) {
        if (takingRequests > maxTakingRequests) {
            throw new SearchTweetsWindowResourceException("takingRequests > maxTakingRequests: " + takingRequests + " > " + maxTakingRequests + ".");
        }

        if (takingRequests < minTakingRequests) {
            throw new SearchTweetsWindowResourceException("takingRequests < minTakingRequests: " + takingRequests + " < " + minTakingRequests + ".");
        }
    }


    public long getResetTimeInMillis() {
        return resetTimeInMillis;
    }

    public int getRemainingRequests() {
        resourceLock.lock();
        try {
            return remainingRequests;
        } finally {
            resourceLock.unlock();
        }
    }

    public int getMaxRequestsPerInterval() {
        return maxRequestsPerInterval;
    }

    public long getStartTimeInMillis() {
        return startTimeInMillis;
    }

    public TwitterConnector getConnector() {
        return connector;
    }
}