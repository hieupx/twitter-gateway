package com.senseinfosys.pubsense.gateway.domain.app;

import com.senseinfosys.pubsense.gateway.domain.core.DomainException;

public class AppException extends DomainException {

    private static final long serialVersionUID = -4340400200619806772L;

    public static final int NOT_FOUND = 1;

    public static final int APP_ALREADY_REGISTERED = 2;

    public AppException(int errorCode, String message) {
        super(DomainType.APP, errorCode, message);
    }

    public AppException(int errorCode, Throwable throwable) {
        super(DomainType.APP, errorCode, throwable);
    }

}
