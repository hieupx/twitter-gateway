package com.senseinfosys.pubsense.gateway.domain.twitter.topic;

import com.google.common.collect.Sets;
import com.google.common.eventbus.EventBus;
import com.senseinfosys.pubsense.gateway.domain.app.AppService;
import com.senseinfosys.pubsense.gateway.domain.app.Application;
import com.senseinfosys.pubsense.gateway.domain.connector.ConnectorException;
import com.senseinfosys.pubsense.gateway.domain.connector.ConnectorService;
import com.senseinfosys.pubsense.gateway.domain.core.DomainService;
import com.senseinfosys.pubsense.gateway.domain.klaver.KlaverConnector;
import com.senseinfosys.pubsense.gateway.domain.klaver.KlaverService;
import com.senseinfosys.pubsense.gateway.domain.translator.ms.MsTranslatorConnector;
import com.senseinfosys.pubsense.gateway.domain.translator.ms.MsTranslatorConnectorService;
import com.senseinfosys.pubsense.gateway.domain.twitter.TopicCreated;
import com.senseinfosys.pubsense.gateway.domain.twitter.TopicUpdated;
import com.senseinfosys.pubsense.gateway.domain.twitter.TwitterConnector;
import com.senseinfosys.pubsense.gateway.domain.twitter.TwitterConnectorService;
import com.senseinfosys.pubsense.gateway.domain.twitter.scheduling.SchedulingInformationService;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.Topic.RunStatus;
import com.senseinfosys.pubsense.gateway.web.topic.RequestTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class TopicService extends DomainService {

    @Autowired
    TopicRepository topicRepository;

    @Autowired
    AppService appService;

    @Autowired
    ConnectorService connectorService;

    @Autowired
    KeywordRepository keywordRepository;

    @Autowired
    private TwitterConnectorService twitterConnectorService;

    @Autowired
    private MsTranslatorConnectorService msTranslatorConnectorService;

    @Autowired
    private KlaverService klaverService;

    @Autowired
    private TopicStatusService topicStatusService;

    @Autowired
    private SchedulingInformationService schedulingInformationService;

    @Autowired
    private EventBus eventBus;

    public long countTopicsNeedServicing(Date currentMilis, long connectorId) {
        return topicRepository.countTopicsNeedServicing(RunStatus.PAUSED.name(), currentMilis, connectorId);
    }

    public List<Topic> findToService(Date currentDate, long connectorId, long sinceId, long take) {
        return topicRepository.findSomeToService(RunStatus.PAUSED.name(), currentDate, connectorId, sinceId, take);
    }

    @Transactional
    public Topic create(RequestTopic requestTopic) {
        Application app = appService.findByAppId(requestTopic.getAppId());
        ensureTopicNotExisting(requestTopic, app);
        TwitterConnector twitterConnector = twitterConnectorService.find(requestTopic.getTwitterConnectorId(),
                app);
        ensureConnectorExisted(twitterConnector);
        MsTranslatorConnector msTranslatorConnector = msTranslatorConnectorService.find(requestTopic.getMsTranslatorConnectorId(), app);
        KlaverConnector klaverConnector = klaverService.find(requestTopic.getKlaverConnectorId(), app);
        Topic topic = createTopicFromRequestTopic(requestTopic, app, twitterConnector, msTranslatorConnector, klaverConnector);
        Set<Keyword> keywords = createKeywordsFromRequestKeywords(requestTopic, app, topic);
        topic.keywords(keywords);
        topic.createdBy(getCurrentAppId());
        topicRepository.save(topic);
        eventBus.post(new TopicCreated(topic));
        return topic;
    }

    private Set<Keyword> createKeywordsFromRequestKeywords(RequestTopic requestTopic, Application app, Topic topic) {
        Set<Keyword> keywords = requestTopic.getKeywords()
                .stream()
                .map(rk -> new Keyword()
                        .remoteId(rk.getId())
                        .lang(rk.getLang())
                        .query(rk.getQuery())
                        .topic(topic)
                        .type(rk.getType())
                        .application(app)
                        .buildSearchQuery())
                .collect(Collectors.toSet());
        return keywords;
    }

    private Topic createTopicFromRequestTopic(RequestTopic requestTopic, Application app,
                                              TwitterConnector twitterConnector, MsTranslatorConnector msTranslatorConnector, KlaverConnector klaverConnector) {
        RunStatus rs = Topic.getDerivedRunStatus(requestTopic.since(), requestTopic.until(), RunStatus.valueOf(requestTopic.getRunStatus()));

        Topic topic = new Topic()
                .remoteId(requestTopic.getId())
                .application(app)
                .twitterConnector(twitterConnector)
                .msTranslatorConnector(msTranslatorConnector)
                .klaverConnector(klaverConnector)
                .description(requestTopic.getDescription())
                .findNegativeEmoticons(requestTopic.getFindNegativeEmoticons())
                .findOriginalPosts(requestTopic.getFindOriginalPosts())
                .findPositiveEmoticons(requestTopic.getFindPositiveEmoticons())
                .findQuestions(requestTopic.getFindQuestions())
                .lat(requestTopic.getLat()).lon(requestTopic.getLon())
                .radius(requestTopic.getRadius())
                .radiusUnit(requestTopic.getRadiusUnit())
                .runStatus(rs.name())
                .since(requestTopic.since())
                .until(requestTopic.until())
                .timeZone(requestTopic.getTimeZone());
        return topic;
    }

    private void ensureConnectorExisted(TwitterConnector twitterConnector) {
        if (twitterConnector == null) {
            throw new ConnectorException(ConnectorException.NOT_FOUND, "TwitterConnector not found.");
        }
    }

    private void ensureTopicNotExisting(RequestTopic requestTopic, Application app) {
        Topic topic = topicRepository.findByRemoteIdAndApplication(requestTopic.getId(), app);
        if (topic != null) {
            throw new TopicException(TopicException.ALREADY_EXISTS, "Topic exists.");
        }
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void delete(String remoteId, String appId) {
        Application app = appService.findByAppId(appId);
        Topic existingTopic = topicRepository.findByRemoteIdAndApplication(remoteId, app);
        ensureTopicExists(existingTopic);
        delete(existingTopic);
    }

    private void delete(Topic topic) {
        deleteReferences(topic);
        topicRepository.delete(topic);
    }

    private void deleteReferences(Topic topic) {
        topicStatusService.deleteByTopic(topic);
        schedulingInformationService.deleteKeywordSchedulingInformation(topic);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Topic update(RequestTopic requestTopic) {
        Application app = appService.findByAppId(requestTopic.getAppId());
        Topic existingTopic = topicRepository.findByRemoteIdAndApplication(requestTopic.getId(), app);
        ensureTopicExists(existingTopic);

        TwitterConnector twitterConnector = twitterConnectorService.find(requestTopic.getTwitterConnectorId(), app);
        ensureConnectorExisted(twitterConnector);
        MsTranslatorConnector msTranslatorConnector = msTranslatorConnectorService.find(requestTopic.getMsTranslatorConnectorId(), app);
        KlaverConnector klaverConnector = klaverService.find(requestTopic.getKlaverConnectorId(), app);
        Topic updatingTopic = createTopicFromRequestTopic(requestTopic, app, twitterConnector, msTranslatorConnector, klaverConnector);
        final boolean searchOperatorsModified = existingTopic.searchOperatorsModified(updatingTopic);
        existingTopic.update(updatingTopic, getCurrentAppId());

        Set<Keyword> updatingKeywords = new HashSet<>(createKeywordsFromRequestKeywords(requestTopic, app, existingTopic));

        Map<String, Keyword> updatingKeywordsByRemoteId = updatingKeywords.stream().collect(Collectors.toMap(k -> k.getRemoteId(), k -> k));

        Set<Keyword> existingKeywords = new HashSet<>(existingTopic.getKeywords());

        Set<Keyword> keywordsToUpdate = Sets.intersection(existingKeywords, updatingKeywords);

        Set<Keyword> keywordsToCreate = Sets.difference(updatingKeywords, existingKeywords);

        Set<Keyword> keywordsToDelete = Sets.difference(existingKeywords, updatingKeywords);

        // Update keywords
        keywordsToUpdate.forEach(k -> k.update(searchOperatorsModified, updatingKeywordsByRemoteId.get(k.getRemoteId()), existingTopic, getCurrentAppId()));

        // Add new keywords to be cascaded
        keywordsToCreate.forEach(k -> k.createdBy(getCurrentAppId()));
        existingTopic.addKeywords(keywordsToCreate);

        // Delete keywords
        // Delete scheduling information
        topicStatusService.deleteByKeywords(keywordsToDelete);
        existingTopic.removeKeywords(keywordsToDelete);
        existingTopic.updateKeywordsCount();
        topicRepository.save(existingTopic);
        eventBus.post(new TopicUpdated(existingTopic));
        return existingTopic;
    }

    private void ensureTopicExists(Topic topic) {
        if (topic == null) {
            throw new TopicException(TopicException.NOT_FOUND, "Topic not found.");
        }
    }

    public boolean isDeletedCompletely(long id) {
        List<Keyword> keywords = keywordRepository.findByTopic(new Topic().id(id));
        if (!keywords.isEmpty()) {
            return false;
        }
        return !topicRepository.exists(id);
    }

    public void save(List<Topic> topics) {
        topicRepository.save(topics);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void deleteByTwitterConnector(TwitterConnector connector) {
        List<Topic> topics = topicRepository.findByTwitterConnector(connector);
        topics.forEach(topic -> delete(topic));
    }

    public Topic updateServiceFields(Topic topic) {
        Topic existingTopic = topicRepository.findOne(topic.getId());
        if (existingTopic == null) {
            // if topic is deleted, keep the collected data but not update it
            return topic;
        }
        existingTopic.updateServiceFields(topic);
        topicRepository.save(existingTopic);
        return existingTopic;
    }

    public Topic save(Topic topic) {
        return topicRepository.save(topic);
    }

    public List<Topic> findByRemoteIds(Collection<String> topicIds) {
        return topicRepository.findByRemoteIdIn(topicIds);
    }

    public Topic findById(long id) {
        return topicRepository.findOne(id);
    }

    public Topic findRemoteById(String remoteId) {
        return topicRepository.findByRemoteId(remoteId);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void create(Collection<RequestTopic> requestTopics) {
        requestTopics.forEach(this::create);
    }


    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void update(Collection<RequestTopic> requestTopics) {
        requestTopics.forEach(this::update);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void delete(Collection<String> removeIds, String appId) {
        removeIds.forEach(id -> delete(id, appId));
    }


}
