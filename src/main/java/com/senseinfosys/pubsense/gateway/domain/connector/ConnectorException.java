package com.senseinfosys.pubsense.gateway.domain.connector;

import com.senseinfosys.pubsense.gateway.domain.core.DomainException;

public class ConnectorException extends DomainException {

    private static final long serialVersionUID = -6953083144667961062L;

    public static final int INVALID_CONNECTOR = 1;

    public static final int INVALID_CREDENTIALS = 2;

    public static final int UNKNOWN_ERROR = 3;

    public ConnectorException(int errorCode, String message) {
        super(DomainType.CONNECTOR, errorCode, message);
    }

    public ConnectorException(int errorCode, Throwable throwable) {
        super(DomainType.CONNECTOR, errorCode, throwable);
    }

    public ConnectorException(int errorCode, String message, Throwable throwable) {
        super(DomainType.CONNECTOR, errorCode, message, throwable);
    }

}
