package com.senseinfosys.pubsense.gateway.domain.translator.ms;

import com.senseinfosys.pubsense.gateway.domain.connector.ConnectorRepository;

public interface MsTranslatorConnectorRepository extends ConnectorRepository<MsTranslatorConnector> {

}
