package com.senseinfosys.pubsense.gateway.domain.twitter;

import com.senseinfosys.pubsense.gateway.domain.connector.ConnectorRepository;

public interface TwitterConnectorRepository extends ConnectorRepository<TwitterConnector> {

}
