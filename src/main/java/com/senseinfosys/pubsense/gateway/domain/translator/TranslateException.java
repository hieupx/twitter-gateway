package com.senseinfosys.pubsense.gateway.domain.translator;

import com.senseinfosys.pubsense.gateway.domain.core.DomainException;

/**
 * Created by toan on 9/21/16.
 */
public class TranslateException extends DomainException {

    private static final long serialVersionUID = -7877683322284456080L;

    public static final int ZERO_BALANCE = 1;

    public static final int INTERNALLY_RATE_LIMITED = 2;

    public static final int INVALID_SUBSCRIPTION_KEY = 3;

    public TranslateException(int errorCode) {
        this(errorCode, "");
    }

    public TranslateException(String message) {
        this(UNKNOWN_ERROR, message);
    }

    public TranslateException(int errorCode, Throwable e) {
        this(errorCode, null, e);
    }

    public TranslateException(int errorCode, String message) {
        this(errorCode, message, null);
    }

    public TranslateException(int errorCode, String message, Throwable e) {
        super(DomainType.TRANSLATE, errorCode, message, e);
    }
}
