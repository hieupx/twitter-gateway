package com.senseinfosys.pubsense.gateway.domain.klaver;

import com.senseinfosys.pubsense.gateway.domain.connector.ConnectorRepository;

/**
 * Created by toan on 10/5/16.
 */
public interface KlaverConnectorRepository extends ConnectorRepository<KlaverConnector> {
}
