package com.senseinfosys.pubsense.gateway.domain.app;

import com.senseinfosys.pubsense.gateway.domain.core.DomainRepository;

public interface AppRepository extends DomainRepository<Application> {

    Application findByAppIdAndAppSecret(String appId, String appSecret);

    Application findByAppId(String appId);

    Application findByClientId(String clientId);
}
