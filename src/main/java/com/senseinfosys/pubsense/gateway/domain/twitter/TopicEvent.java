package com.senseinfosys.pubsense.gateway.domain.twitter;

import com.senseinfosys.pubsense.gateway.domain.twitter.topic.Topic;
import com.senseinfosys.pubsense.gateway.infrastructure.event.Event;

/**
 * Created by toan on 11/11/16.
 */
public abstract class TopicEvent implements Event {

    final protected Topic topic;

    public TopicEvent(Topic topic) {
        this.topic = topic;
    }

    public Topic getTopic() {
        return topic;
    }
}
