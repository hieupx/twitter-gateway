package com.senseinfosys.pubsense.gateway.domain.twitter.topic;

import javaslang.Tuple;
import javaslang.Tuple2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class KeywordService {

    private final KeywordRepository keywordRepository;

    private final TopicService topicService;

    @Autowired
    public KeywordService(KeywordRepository keywordRepository, TopicService topicService) {
        this.keywordRepository = keywordRepository;
        this.topicService = topicService;
    }

    @Transactional
    public Tuple2<Topic, Keyword> updateServiceFields(Topic topic, Keyword keyword) {
        Topic t = topicService.updateServiceFields(topic);
        Keyword k = updateServiceFields(keyword);
        return Tuple.of(t, k);
    }

    private Keyword updateServiceFields(Keyword keyword) {
        Keyword existingKeyword = keywordRepository.findOne(keyword.getId());
        if (existingKeyword == null) {
            // if keyword is deleted, not update it, but keep the collected data,
            // because that data belongs to the keyword in the past, not the current keyword, which is deleted
            return keyword;
        }
        existingKeyword.updateServiceFields(keyword);
        keywordRepository.save(existingKeyword);
        return existingKeyword;
    }

    public Keyword findById(long id) {
        return keywordRepository.findOne(id);
    }

    public Keyword findByRemoteId(String remoteId) {
        return keywordRepository.findByRemoteId(remoteId);
    }

    public List<Keyword> findByTopics(List<Topic> topics) {
        return keywordRepository.findByTopicIn(topics);
    }
}
