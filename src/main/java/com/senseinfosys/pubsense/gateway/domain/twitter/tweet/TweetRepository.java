package com.senseinfosys.pubsense.gateway.domain.twitter.tweet;

import com.senseinfosys.pubsense.gateway.domain.twitter.TwitterClientCache;
import com.senseinfosys.pubsense.gateway.domain.twitter.TwitterConnector;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.Keyword;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.Topic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import twitter4j.*;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Repository
public class TweetRepository {

    private TwitterClientCache twitterClientCache;

    @Autowired
    public TweetRepository(TwitterClientCache twitterClientCache) {
        this.twitterClientCache = twitterClientCache;
    }

    public List<Tweet> find(Keyword keyword, int pageSize, int collectPages, TwitterConnector connector,
                            Boolean findOriginalTweets, Tweet.SearchType searchType) {

        Twitter twitter = twitterClientCache.get(connector);

        Query searchQuery = new Query(keyword.getSearchQuery());
        Topic topic = keyword.getTopic();
        if (topic.hasGeoCode()) {
            searchQuery.setGeoCode(topic.geoLoc(), topic.getRadius(), topic.radiusUnit());
        }
        searchQuery.setCount(pageSize);

        if (keyword.searched()) {
            if (searchType == Tweet.SearchType.RECENT) {
                searchQuery.setSinceId(keyword.getSinceTweetId());
            }
            if (searchType == Tweet.SearchType.HISTORICAL) {
                searchQuery.setMaxId(keyword.getMaxTweetId());
            }
        }

        Predicate<Status> predicate = s -> true;
        if (Boolean.TRUE.equals(findOriginalTweets)) {
            predicate = s -> s.getRetweetCount() == 0;
        }

        List<Tweet> result = new LinkedList<>();
        int collectedPages = 0;
        QueryResult queryResult;

        do {
            try {
                queryResult = twitter.search(searchQuery);
                List<Status> statuses = queryResult.getTweets();
                if (statuses.isEmpty()) {
                    return result;
                }
                List<Tweet> tweets = statusesToTweets(statuses, predicate);
                result.addAll(tweets);
                if (!keyword.searched()) {
                    // since id holds greater value
                    keyword.setMaxTweetId(statuses.get(statuses.size() - 1).getId());
                    // max id holds smaller value
                    keyword.setSinceTweetId(statuses.get(0).getId());
                } else {
                    if (searchType == Tweet.SearchType.RECENT) {
                        keyword.setSinceTweetId(statuses.get(0).getId());
                    }
                    if (searchType == Tweet.SearchType.HISTORICAL) {
                        keyword.setMaxTweetId(statuses.get(statuses.size() - 1).getId());
                    }
                }
            } catch (TwitterException e) {
                throw new SearchTweetsException(e.getMessage(), e);
            }

        } while (++collectedPages < collectPages && ((searchQuery = queryResult.nextQuery()) != null));

        return result;
    }

    private List<Tweet> statusesToTweets(List<Status> statuses, Predicate<Status> predicate) {
        return statuses
                .stream()
                .filter(predicate)
                .map(status -> new Tweet(status))
                .collect(Collectors.toList());
    }
}
