package com.senseinfosys.pubsense.gateway.domain.twitter;

import com.senseinfosys.pubsense.gateway.domain.connector.Connector;
import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.DateTimeConstants;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Min;

@Entity
@Table(
   uniqueConstraints = {@UniqueConstraint(columnNames = {"consumerKey", "consumerSecret"}),
                        @UniqueConstraint(columnNames = {"application_id", "remoteId"})}
)
public class TwitterConnector extends Connector {

    public static final double TWITTER_WINDOW_SIZE = 15.0;

    {
        this.type = ConnectorType.TWITTER;
    }

    @NotBlank
    private String consumerKey;

    @NotBlank
    private String consumerSecret;

    private String accessToken;

    private String accessTokenSecret;

    @Min(1)
    private int intervalLength;

    @Min(1)
    private int requestsPerInterval;

    public TwitterConnector() {
    }

    public TwitterConnector(String consumerKey, String consumerSecret, String accessToken, String accessTokenSecret,
                            int intervalLength, int requestsPerInterval) {
        this.consumerKey = consumerKey;
        this.consumerSecret = consumerSecret;
        this.accessToken = accessToken;
        this.accessTokenSecret = accessTokenSecret;
        this.intervalLength = intervalLength;
        this.requestsPerInterval = requestsPerInterval;
    }

    public String getConsumerKey() {
        return consumerKey;
    }

    public String getConsumerSecret() {
        return consumerSecret;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getAccessTokenSecret() {
        return accessTokenSecret;
    }

    public int getIntervalLength() {
        return intervalLength;
    }

    public long intervalInMillis() {
        return 1L * intervalLength * DateTimeConstants.MILLIS_PER_MINUTE;
    }

    public int getRequestsPerInterval() {
        return requestsPerInterval;
    }

    public boolean update(TwitterConnector connector, String currentAppId) {
        if (connector.accessToken != null) {
            this.accessToken = connector.accessToken;
        }
        if (connector.accessTokenSecret != null) {
            this.accessTokenSecret = connector.accessTokenSecret;
        }
        if (connector.consumerKey != null) {
            this.consumerKey = connector.consumerKey;
        }
        if (connector.accessTokenSecret != null) {
            this.accessTokenSecret = connector.accessTokenSecret;
        }
        if (connector.intervalLength > 0) {
            this.intervalLength = connector.intervalLength;
        }
        if (connector.requestsPerInterval > 0) {
            this.requestsPerInterval = connector.requestsPerInterval;
        }
        if (connector.consumerSecret != null) {
            this.consumerSecret = connector.consumerSecret;
        }
        this.updatedBy(currentAppId);
        return true;
    }

    public String name() {
        return "twitter";
    }

    @Override
    public String toString() {
        return "TwitterConnector(" +
                "uuid=" + remoteId +
                "consumerKey=" + consumerKey +
                ") ";
    }

    public double requestRate() {
        return Math.min(intervalLength / TWITTER_WINDOW_SIZE, 1.0);
    }
}
