package com.senseinfosys.pubsense.gateway.domain.app;

import com.senseinfosys.pubsense.gateway.domain.core.DomainService;
import com.senseinfosys.pubsense.gateway.infrastructure.id.SecretFactory;
import com.senseinfosys.pubsense.gateway.web.app.RequestApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AppService extends DomainService {

    private AppRepository appRepository;

    @Autowired
    public AppService(AppRepository appRepository) {
        this.appRepository = appRepository;
    }

    public Application findByAppId(String appId) {
        Application app = appRepository.findByAppId(appId);
        if (app == null) {
            throw new AppException(AppException.NOT_FOUND, "App not found.");
        }
        return app;
    }

    @Transactional(readOnly = true)
    public List<Application> findAll() {
        return appRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Application find(String appId, String appSecret) {
        Application app = appRepository.findByAppIdAndAppSecret(appId, appSecret);
        if (app == null) {
            throw new AppException(AppException.NOT_FOUND, "App not found.");
        }
        return app;
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Application createApp(RequestApp requestApp) {
        Application existingApp = appRepository.findByClientId(requestApp.getClientId());
        if (existingApp != null) {
            return existingApp;
        }
        Application app = new Application(SecretFactory.getSecret(Application.SECRET_LENGTH),
                requestApp.getClientId()).createdBy(getCurrentUserId());
        return appRepository.save(app);
    }

    private void ensureAppNotRegistered(String clientId) {
        Application app = appRepository.findByClientId(clientId);
        if (app != null) {
            throw new AppException(AppException.APP_ALREADY_REGISTERED, "App already registered for clientId: " + clientId + ".");
        }
    }

}
