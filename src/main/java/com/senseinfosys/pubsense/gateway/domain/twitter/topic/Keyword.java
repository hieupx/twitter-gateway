package com.senseinfosys.pubsense.gateway.domain.twitter.topic;

import com.senseinfosys.pubsense.gateway.domain.app.AppRelatedDomain;
import com.senseinfosys.pubsense.gateway.domain.app.Application;
import com.senseinfosys.pubsense.gateway.infrastructure.date.DateUtils;

import javax.persistence.*;

import org.hibernate.validator.constraints.NotBlank;

import java.util.Date;
import java.util.Objects;

@Entity
@Table(
    name = "twitter_keyword",
    uniqueConstraints = {@UniqueConstraint(columnNames = {"application_id", "remoteId"})}
)
public class Keyword extends AppRelatedDomain {

    public static final int MAX_TWEETS_COLLECTED_IN_A_REQUEST = 100;

    public static final String NEGATIVE_EMOTICONS = " :(";

    public static final String POSITIVE_EMOTICONS = " :)";

    public static final String QUESTIONS = " ?";

    @NotBlank
    @Column(columnDefinition = "text")
    private String query;

    @NotBlank
    @Column(columnDefinition = "text")
    private String searchQuery;

    private String lang;

    private String type;

    // Tweet's fields
    private long maxTweetId;

    private long sinceTweetId;

    private long totalTweetsCount;

    private long historicalSearchesCount;

    private long recentSearchesCount;

    // https://www.cs.umd.edu/~pugh/java/memoryModel/jsr-133-faq.html#volatile
    private volatile int lastHistoricalTweetsCount = -1;

    private int lastRecentTweetsCount = -1;

    private boolean doingRecentTweetsSearch = true;

    private Date lastServiced;

    // read https://www.cs.umd.edu/~pugh/java/memoryModel/jsr-133-faq.html#volatile
    private volatile long servicedCount;

    // If topic is cached in the session for lazy init
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "topic_id", nullable = false)
    private Topic topic;

    public Keyword() {
    }


    public String getQuery() {
        return query;
    }

    public String getLang() {
        return lang;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public Topic getTopic() {
        return topic;
    }

    public boolean isDoingRecentTweetsSearch() {
        return doingRecentTweetsSearch;
    }

    public String getType() {
        return type;
    }

    public long getMaxTweetId() {
        return maxTweetId;
    }

    public void setMaxTweetId(long maxTweetId) {
        this.maxTweetId = maxTweetId;
    }

    public long getSinceTweetId() {
        return sinceTweetId;
    }

    public void setSinceTweetId(long sinceTweetId) {
        this.sinceTweetId = sinceTweetId;
    }

    public long getTotalTweetsCount() {
        return totalTweetsCount;
    }

    public void setTotalTweetsCount(long totalTweetsCount) {
        this.totalTweetsCount = totalTweetsCount;
    }

    public long getHistoricalSearchesCount() {
        return historicalSearchesCount;
    }

    public long getServicedCount() {
        return servicedCount;
    }

    public Date getLastServiced() {
        return lastServiced;
    }

    public void setHistoricalSearchesCount(long historicalSearchesCount) {
        this.historicalSearchesCount = historicalSearchesCount;
    }

    public long getRecentSearchesCount() {
        return recentSearchesCount;
    }

    public void setRecentSearchesCount(long recentSearchesCount) {
        this.recentSearchesCount = recentSearchesCount;
    }

    public int getLastHistoricalTweetsCount() {
        return lastHistoricalTweetsCount;
    }

    public void setLastHistoricalTweetsCount(int lastHistoricalTweetsCount) {
        this.lastHistoricalTweetsCount = lastHistoricalTweetsCount;
    }

    public int getLastRecentTweetsCount() {
        return lastRecentTweetsCount;
    }

    public void setLastRecentTweetsCount(int lastRecentTweetsCount) {
        this.lastRecentTweetsCount = lastRecentTweetsCount;
    }

    public Keyword query(String query) {
        this.query = query;
        return this;
    }

    public Keyword lang(String lang) {
        this.lang = lang;
        return this;
    }


    public Keyword topic(Topic topic) {
        this.topic = topic;
        return this;
    }

    public Keyword type(String type) {
        this.type = type;
        return this;
    }

    @Override
    public Keyword application(Application application) {
        return (Keyword) super.application(application);
    }

    public Keyword buildSearchQuery() {
        this.searchQuery = query;
        if (Boolean.TRUE.equals(topic.getFindNegativeEmoticons())) {
            searchQuery += NEGATIVE_EMOTICONS;
        }
        if (Boolean.TRUE.equals(topic.getFindPositiveEmoticons())) {
            searchQuery += POSITIVE_EMOTICONS;
        }
        if (Boolean.TRUE.equals(topic.getFindQuestions())) {
            searchQuery += QUESTIONS;
        }
        if (topic.getSince() != null && !query.contains(" since:")) {
            searchQuery += " since:" + DateUtils.isoDateFormat(topic.getSince());
        }
        if (topic.getUntil() != null && !query.contains(" until:")) {
            searchQuery += " until:" + DateUtils.isoDateFormat(topic.getUntil());
        }
        return this;
    }


    @Override
    public Keyword remoteId(String remoteId) {
        return (Keyword) super.remoteId(remoteId);
    }


    public void setCounts(int currentCollectedTweetsCount) {
        if (doingRecentTweetsSearch) {
            this.doingRecentTweetsSearch = false;
            this.lastRecentTweetsCount = currentCollectedTweetsCount;
            this.recentSearchesCount++;
        } else {
            this.doingRecentTweetsSearch = true;
            this.lastHistoricalTweetsCount = currentCollectedTweetsCount;
            this.historicalSearchesCount++;
        }
        totalTweetsCount += currentCollectedTweetsCount;
    }

    public boolean shouldPerformRecentTweetsSearch() {
        boolean allHistoricalTweetsCollected = lastHistoricalTweetsCount >= 0 && lastHistoricalTweetsCount < MAX_TWEETS_COLLECTED_IN_A_REQUEST;
        return allHistoricalTweetsCollected || doingRecentTweetsSearch;
    }

    public void serviced() {
        this.servicedCount++;
        this.lastServiced = new Date();
    }


    public void update(boolean searchOperatorsModified, Keyword keyword, Topic topic, String updatedBy) {

        if (keyword.lang != null) {
            this.lang = keyword.lang;
        }

        if (keyword.query != null) {
            this.query = keyword.query;
        }

        if (keyword.type != null) {
            this.type = keyword.type;
        }

        this.topic = topic;

        // if searchQuery changes, reset since_id and max_id
        String oldSearchQuery = this.searchQuery;
        this.buildSearchQuery();
        if (searchOperatorsModified || !Objects.equals(oldSearchQuery, this.searchQuery)) {
            this.sinceTweetId = 0;
            this.maxTweetId = 0;
            this.lastHistoricalTweetsCount = -1;
            this.lastRecentTweetsCount = -1;
        }
        updatedBy(updatedBy);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((remoteId == null) ? 0 : remoteId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Keyword other = (Keyword) obj;
        if (remoteId == null) {
            if (other.remoteId != null)
                return false;
        } else if (!remoteId.equals(other.remoteId))
            return false;
        return true;
    }

    public boolean searched() {
        return maxTweetId != 0 || sinceTweetId != 0;
    }

    public void updateServiceFields(Keyword keyword) {
        this.maxTweetId = keyword.maxTweetId;
        this.sinceTweetId = keyword.sinceTweetId;
        this.totalTweetsCount = keyword.totalTweetsCount;
        this.historicalSearchesCount = keyword.historicalSearchesCount;
        this.recentSearchesCount = keyword.recentSearchesCount;
        this.lastHistoricalTweetsCount = keyword.lastHistoricalTweetsCount;
        this.lastRecentTweetsCount = keyword.lastRecentTweetsCount;
        this.doingRecentTweetsSearch = keyword.doingRecentTweetsSearch;
        this.lastServiced = keyword.lastServiced;
        this.servicedCount = keyword.servicedCount;
        this.updatedBy = keyword.updatedBy;
        this.updatedAt = keyword.updatedAt;
    }

    @Override
    public String toString() {
        return "Keyword{" +
                "uuid=" + remoteId +
                ", searchQuery='" + searchQuery + '\'' +
                ", lang='" + lang + '\'' +
                '}';
    }
}
