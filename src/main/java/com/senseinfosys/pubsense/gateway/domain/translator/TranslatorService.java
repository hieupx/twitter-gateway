package com.senseinfosys.pubsense.gateway.domain.translator;

import com.senseinfosys.pubsense.gateway.domain.translator.ms.MsTranslatorConnector;
import com.senseinfosys.pubsense.gateway.domain.translator.ms.MsTranslatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by toan on 10/3/16.
 */

@Service
public class TranslatorService {

    @Autowired
    private MsTranslatorService translatorService;

    public List<String> translateToEnglish(List<String> texts, TranslatorConnector translatorConnector) {
        if (translatorConnector == null) {
            return texts;
        } else if (translatorConnector instanceof MsTranslatorConnector) {
            return translatorService.translateToEnglish(texts, (MsTranslatorConnector) translatorConnector);
        } else {
            throw new UnsupportedOperationException("Translator type not yet implemented.");
        }
    }
}
