package com.senseinfosys.pubsense.gateway.domain.twitter.topic;

import com.github.krukow.clj_lang.PersistentHashMap;
import com.senseinfosys.pubsense.gateway.domain.core.Domain;
import com.senseinfosys.pubsense.gateway.domain.core.DomainException;
import com.senseinfosys.pubsense.gateway.domain.klaver.KlaverException;
import com.senseinfosys.pubsense.gateway.domain.translator.TranslateException;
import com.senseinfosys.pubsense.gateway.domain.twitter.tweet.SearchTweetsException;
import com.senseinfosys.pubsense.gateway.domain.twitter.tweet.SearchTweetsWindowResource;
import com.senseinfosys.pubsense.gateway.infrastructure.date.DateUtils;
import com.senseinfosys.pubsense.gateway.infrastructure.json.JsonUtils;

import javax.persistence.*;

import static com.senseinfosys.pubsense.gateway.domain.twitter.topic.TopicStatus.ServiceStatus.NO_ISSUE;
import static com.senseinfosys.pubsense.gateway.domain.twitter.topic.TopicStatus.ServiceStatus.WARNING;

/**
 * Created by toan on 9/27/16.
 */
@Entity
@Table(name = "twitter_topic_status")
public class TopicStatus extends Domain {

    private long topicId;

    private long keywordId;

    @Enumerated(value = EnumType.STRING)
    private ServiceStatus serviceStatus;

    @Column(length = 512)
    private String statusContent;

    @Column(length = 1024)
    private String details;

    private Integer code;


    public TopicStatus() {
    }

    public TopicStatus(Topic topic) {
        this(topic, null);
    }

    public TopicStatus(Topic topic, Keyword keyword) {
        this.topicId = topic.getId();
        if (keyword != null) {
            this.keywordId = keyword.getId();
        }
    }

    private void composeContent(DomainException e, Topic topic) {
        if (e instanceof SearchTweetsException) {
            SearchTweetsException ste = (SearchTweetsException) e;
            if (ste.getTwitterException().exceededRateLimitation()) {
                this.statusContent = topic.getTwitterConnector() + "has reached the rate limit.";
            }
            this.statusContent = ste.getTwitterException().getMessage();
        } else if (e instanceof TranslateException) {
            this.statusContent = e.getMessage();
        } else if (e instanceof KlaverException) {
            this.statusContent = e.getCustomMessage();
        } else {
            this.statusContent = e.getMessage();
        }
        this.code = e.getCode();
    }


    public ServiceStatus getServiceStatus() {
        return serviceStatus;
    }

    public String getStatusContent() {
        return statusContent;
    }

    public TopicStatus asError(DomainException e, Topic topic) {
        this.serviceStatus = ServiceStatus.ERROR;
        composeContent(e, topic);
        return this;
    }

    public TopicStatus asWarning(DomainException e, Topic topic) {
        this.serviceStatus = ServiceStatus.WARNING;
        composeContent(e, topic);
        return this;
    }

    public TopicStatus asSuccess() {
        this.serviceStatus = ServiceStatus.SUCCESS;
        composeNormalContent();
        return this;
    }

    private void composeNormalContent() {
        this.statusContent = "normal";
    }

    public TopicStatus asNotStarted() {
        this.serviceStatus = NO_ISSUE;
        this.statusContent = "Topic has not been serviced.";
        return this;
    }

    public TopicStatus asWarning(SearchTweetsWindowResource resource) {
        this.serviceStatus = WARNING;
        String details = JsonUtils.serialize(PersistentHashMap.create(
                "max-requests", String.valueOf(resource.getConnector().getRequestsPerInterval()),
                "remaining-requests", String.valueOf(resource.getRemainingRequests()),
                "created-requests", String.valueOf(resource.getMaxRequestsPerInterval()),
                "window-start", DateUtils.isoDateTimeFormat(resource.getStartTimeInMillis()),
                "window-end", DateUtils.isoDateTimeFormat(resource.getResetTimeInMillis()),
                "connector-id", resource.getConnector().getRemoteId()
        ));
        this.details = details;
        this.statusContent = "Too many requests in an interval, diminish the number of requests for better service " +
                "if you see this warning often.";
        return this;
    }

    public Integer getCode() {
        return code;
    }

    public Object details() {
        return PersistentHashMap.create(
                "message", getStatusContent(),
                "code", getCode(),
                "occurredAt", DateUtils.isoDateTimeFormat(getCreatedAt())
        );
    }

    public String getDetails() {
        return details;
    }

    public enum ServiceStatus {

        ERROR("error"),

        WARNING("warning"),

        SUCCESS("info"),

        NO_ISSUE("info");

        private String key;

        ServiceStatus(String key) {
            this.key = key;
        }

        public String getKey() {
            return key;
        }
    }

}
