package com.senseinfosys.pubsense.gateway.domain.twitter.topic;

import com.senseinfosys.pubsense.gateway.domain.core.DomainException;

/**
 * Created by toan on 11/9/16.
 */
public class KeywordException extends DomainException {

    public static final int NOT_FOUND = 1;

    public KeywordException(int errorCode, String message, Throwable e) {
        super(DomainType.KEYWORD, errorCode, message, e);
    }

    public KeywordException(int errorCode, String message) {
        super(DomainType.KEYWORD, errorCode, message, null);
    }
}
