package com.senseinfosys.pubsense.gateway.domain.twitter.topic;

import com.senseinfosys.pubsense.gateway.domain.core.DomainRepository;

import java.util.Collection;
import java.util.List;

/**
 * Created by toan on 9/27/16.
 */
public interface TopicStatusRepository extends DomainRepository<TopicStatus> {

    TopicStatus findFirstByTopicIdOrderByCreatedAtDesc(long topicId);

    List<TopicStatus> findByTopicId(long topicId);

    List<TopicStatus> findByKeywordIdIn(Collection<Long> keywordIds);
}
