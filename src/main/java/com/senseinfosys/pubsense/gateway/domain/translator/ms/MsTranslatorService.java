package com.senseinfosys.pubsense.gateway.domain.translator.ms;

import com.memetix.mst.language.Language;
import com.memetix.mst.translate.Translate;
import com.memetix.mst.web.HttpOptions;
import com.memetix.mst.web.ShareableClientContext;
import com.senseinfosys.pubsense.gateway.domain.translator.TranslateException;
import com.senseinfosys.pubsense.gateway.infrastructure.collection.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by toan on 9/13/16.
 */

@Service
public class MsTranslatorService {

    @Autowired
    MsTranslatorBackoffManager msTranslatorBackoffManager;

    @Autowired
    MsTranslatorClientCache msTranslatorClientCache;

    private static final long FIVE_MINUTES_IN_MS = 5 * 60 * 1000; // arbitrarily chosen

    private static final HttpOptions options = new HttpOptions(null, "text/html");

    /**
     * Microsoft Translator (Translate) can handle up to 10,240 bytes per request
     * A twitter can contain 140-characters, but the number would go beyond that according to https://blog.twitter.com/express-even-more-in-140-characters
     *
     * @return
     */
    public List<String> translateToEnglish(List<String> texts, MsTranslatorConnector msConnector) {
        try {
            ShareableClientContext context = msTranslatorClientCache.get(msConnector);

            if (!msTranslatorBackoffManager.isBackoffNeeded(msConnector.getSubscriptionKey())) {
                List<List<String>> subTextsList = CollectionUtils.split(texts, 20);
                List<String> result = new ArrayList<>(texts.size());
                for (List<String> subTexts : subTextsList) {
                    String[] translatedTexts = Translate.execute(subTexts.toArray(new String[subTexts.size()]), Language.ENGLISH, context, options);
                    result.addAll(Arrays.asList(translatedTexts));
                }
                return result;
            } else {
                throw new TranslateException(TranslateException.INTERNALLY_RATE_LIMITED, msConnector + "is being internally rate limited due to issues with the translation service.");
            }
        } catch (Exception e) {
            if (e.getMessage() != null) {
	            if (e.getMessage().contains("403")) {
	                // Underlying translator library just throws generic exceptions, not typed exceptions
	                // TODO: improve translator library
	
	                // Don't frequently attempt to ask for translations if the service is telling us we can't
	                Date now = new Date();
	                msTranslatorBackoffManager.backoffUntil(msConnector.getSubscriptionKey(), new Date(now.getTime() + FIVE_MINUTES_IN_MS));
	                throw new TranslateException(TranslateException.ZERO_BALANCE, msConnector + "has zero balance");
	            } else if (e.getMessage().contains("server authentication")) {
	                Date now = new Date();
	                msTranslatorBackoffManager.backoffUntil(msConnector.getSubscriptionKey(), new Date(now.getTime() + FIVE_MINUTES_IN_MS));
	                throw new TranslateException(TranslateException.INVALID_SUBSCRIPTION_KEY, msConnector + "may not be a valid subscription - check subscription key");
	            }
            }

            throw new TranslateException(TranslateException.UNKNOWN_ERROR, msConnector + e.getMessage(), e);
        }
    }
}
