package com.senseinfosys.pubsense.gateway.domain.twitter.scheduling;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.senseinfosys.pubsense.gateway.domain.klaver.KlaverService;
import com.senseinfosys.pubsense.gateway.domain.translator.TranslatorService;
import com.senseinfosys.pubsense.gateway.domain.twitter.*;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.*;
import com.senseinfosys.pubsense.gateway.domain.twitter.tweet.SearchTweetsWindowResource;
import com.senseinfosys.pubsense.gateway.domain.twitter.tweet.SearchTweetsWindowResourceException;
import com.senseinfosys.pubsense.gateway.domain.twitter.tweet.TweetCollectionTask;
import com.senseinfosys.pubsense.gateway.domain.twitter.tweet.TweetService;
import com.senseinfosys.pubsense.gateway.infrastructure.annotation.SingleThreadConfined;
import com.senseinfosys.pubsense.gateway.infrastructure.date.DateUtils;
import com.senseinfosys.pubsense.gateway.infrastructure.monitor.MonitorService;
import com.senseinfosys.pubsense.gateway.infrastructure.number.NumberParser;
import javaslang.Tuple;
import javaslang.Tuple2;
import org.joda.time.DateTimeConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import twitter4j.RateLimitStatus;
import twitter4j.Twitter;
import twitter4j.TwitterException;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.senseinfosys.pubsense.gateway.domain.twitter.topic.Keyword.MAX_TWEETS_COLLECTED_IN_A_REQUEST;

@Component
public class TwitterScheduler implements InitializingBean {

    private static final Logger LOG = LoggerFactory.getLogger(TwitterScheduler.class);

    public static final String TWITTER_SCHEDULER = "twitterScheduler";

    public static final String TWEET_COLLECTION_EXECUTOR = "tweetCollectionExec";

    public static final String TWEET_PROCESSING_EXECUTOR = "tweetProcessingExec";

    public static final String TWITTER_SCHEDULER_EXECUTOR = "twitterSchedulerExec";

    private static final long TWITTER_SEARCH_TWEETS_WINDOW = DateTimeConstants.MILLIS_PER_MINUTE * 15;

    private static final long DEFAULT_INIT_DELAY_IN_MILLIS = DateTimeConstants.MILLIS_PER_SECOND * 5;

    // if we make requests every <interval length> then we run the risk of scheduling our next set of
    // requests too close to the end of the current window
    private static final long DEFAULT_INIT_PADDING_TIME_IN_MILLIS = DateTimeConstants.MILLIS_PER_SECOND * 5;

    private final int maxRequestsPerKeyword = 5;

    private final int requestsPerTopic = 1;

    private final int maxTweetsPerRequests;

    private final ExecutorService tweetProcessingExecutor;

    private final ExecutorService tweetCollectionExecutor;

    private final ScheduledExecutorService tweetSchedulerExecutor;

    @Autowired
    private TweetService tweetService;

    private final TopicSchedulingInformationRepository topicSchedulingInformationRepository;

    private final KeywordSchedulingInformationRepository keywordSchedulingInformationRepository;

    @Autowired
    private KeywordService keywordService;

    private final TranslatorService translatorService;

    private final ConnectorTaskManager connectorTaskManager = new ConnectorTaskManager();

    private final TwitterClientCache twitterClientCache;

    private final TopicStatusService topicStatusService;

    @Autowired
    private TopicService topicService;

    @Autowired
    private KlaverService klaverService;

    @Autowired
    MonitorService monitorService;

    @Autowired
    private EventBus eventBus;

    @Autowired
    public TwitterScheduler(TopicSchedulingInformationRepository topicSchedulingInformationRepository,
                            KeywordSchedulingInformationRepository keywordSchedulingInformationRepository,
                            TranslatorService translatorService,
                            TwitterClientCache twitterClientCache,
                            TopicStatusService topicStatusService,
                            @Qualifier(TWEET_COLLECTION_EXECUTOR) ExecutorService tweetCollectionExecutor,
                            @Qualifier(TWEET_PROCESSING_EXECUTOR) ExecutorService tweetProcessingExecutor,
                            @Qualifier(TWITTER_SCHEDULER_EXECUTOR) ScheduledExecutorService tweetSchedulerExecutor,
                            Environment env) {
        this.topicSchedulingInformationRepository = topicSchedulingInformationRepository;
        this.keywordSchedulingInformationRepository = keywordSchedulingInformationRepository;
        this.translatorService = translatorService;
        this.topicStatusService = topicStatusService;
        this.twitterClientCache = twitterClientCache;
        this.tweetCollectionExecutor = tweetCollectionExecutor;
        this.tweetProcessingExecutor = tweetProcessingExecutor;
        this.tweetSchedulerExecutor = tweetSchedulerExecutor;
        this.maxTweetsPerRequests = NumberParser.getInt(env.getProperty("twitter.max-tweets-per-requests"), MAX_TWEETS_COLLECTED_IN_A_REQUEST);
    }

    private class ConnectorTaskManager {

        private final ExecutorService serialExec = Executors.newSingleThreadExecutor();

        @SingleThreadConfined("serialExec")
        private final Map<String, Tuple2<ConnectorSchedulingTask, ScheduledFuture<?>>> managedTasks = new HashMap<>();

        public void newTask(
                TwitterConnector connector,
                long initDelayInMillis,
                BlockingQueue<TweetCollectionTask> overdueTaskQueue) {
            serialExec.execute(() ->
                    newTaskIfAbsent(connector, initDelayInMillis, overdueTaskQueue)
            );
        }

        public void cancelTask(TwitterConnector connector) {
            serialExec.execute(() ->
                    cancelTaskIfPresent(connector)
            );
        }

        public void updateTask(TwitterConnector connector) {
            serialExec.execute(() ->
                    cancelIfPresentAndCreateNew(connector)
            );
        }

        public void quickSchedule(TwitterConnector connector, long defaultInitDelayInMillis) {
            serialExec.execute(() ->
                    quickScheduleIfPresent(connector, defaultInitDelayInMillis)
            );
        }

        private void quickScheduleIfPresent(TwitterConnector connector, long initDelayInMillis) {
            Tuple2<ConnectorSchedulingTask, ScheduledFuture<?>> tuple = managedTasks.get(connector.getConsumerKey());
            if (tuple == null) {
                return;
            }

            TopicSchedulingInformation topicSchedulingInformation = topicSchedulingInformationRepository.findByTwitterConnector(connector);

            if (topicSchedulingInformation == null || !SearchTweetsWindowResource.ensureValidResource
                    (topicSchedulingInformation.getLastWindowResetTime(), topicSchedulingInformation.getLastAvailableRequests())) {
                return;
            }

            ConnectorSchedulingTask task = new ConnectorSchedulingTask(connector, tuple._1.windowOverdueTaskQueue, true);
            tweetSchedulerExecutor.schedule(
                    task,
                    initDelayInMillis,
                    TimeUnit.MILLISECONDS
            );
        }

        private Tuple2<ConnectorSchedulingTask, ScheduledFuture<?>> newTaskIfAbsent(
                TwitterConnector connector,
                long initDelayInMillis,
                BlockingQueue<TweetCollectionTask> overdueTaskQueue) {

            Tuple2<ConnectorSchedulingTask, ScheduledFuture<?>> tuple = managedTasks.get(connector.getConsumerKey());
            if (tuple != null) {
                return tuple;
            }
            ConnectorSchedulingTask task = new ConnectorSchedulingTask(connector, overdueTaskQueue, false);
            ScheduledFuture<?> future = tweetSchedulerExecutor.scheduleWithFixedDelay(
                    task,
                    initDelayInMillis,
                    connector.intervalInMillis() + DEFAULT_INIT_PADDING_TIME_IN_MILLIS,
                    TimeUnit.MILLISECONDS
            );
            return managedTasks.put(connector.getConsumerKey(), Tuple.of(task, future));
        }

        private Tuple2<ConnectorSchedulingTask, ScheduledFuture<?>> cancelTaskIfPresent(TwitterConnector connector) {
            Tuple2<ConnectorSchedulingTask, ScheduledFuture<?>> tuple = managedTasks.remove(connector.getConsumerKey());
            if (tuple != null) {
                tuple._2.cancel(false);
            }
            return tuple;
        }

        private Tuple2<ConnectorSchedulingTask, ScheduledFuture<?>> cancelIfPresentAndCreateNew(TwitterConnector connector) {
            Tuple2<ConnectorSchedulingTask, ScheduledFuture<?>> tuple = cancelTaskIfPresent(connector);
            long initDelayInMillis = DEFAULT_INIT_DELAY_IN_MILLIS;
            BlockingQueue<TweetCollectionTask> overdueTaskQueue;
            if (tuple != null) {
                overdueTaskQueue = tuple._1.windowOverdueTaskQueue;
                long remainingDelayInMillis = tuple._2.getDelay(TimeUnit.MILLISECONDS);
                if (remainingDelayInMillis > 0L) {
                    initDelayInMillis = remainingDelayInMillis;
                } else {
                    initDelayInMillis = tuple._1.connector.intervalInMillis() + remainingDelayInMillis;
                }
            } else {
                overdueTaskQueue = new LinkedBlockingQueue<>();
            }
            return newTaskIfAbsent(connector, initDelayInMillis, overdueTaskQueue);
        }

    }

    public void unSchedule(TwitterConnector twitterConnector) {
        connectorTaskManager.cancelTask(twitterConnector);
    }

    public void reSchedule(TwitterConnector twitterConnector) {
        connectorTaskManager.updateTask(twitterConnector);
    }

    public void schedule(TwitterConnector twitterConnector) {
        connectorTaskManager.newTask(twitterConnector, DEFAULT_INIT_DELAY_IN_MILLIS, new LinkedBlockingQueue<>());
    }

    public void quickSchedule(TwitterConnector twitterConnector) {
        connectorTaskManager.quickSchedule(twitterConnector, DEFAULT_INIT_DELAY_IN_MILLIS);
    }

    private class ConnectorSchedulingTask implements Runnable {

        private final TwitterConnector connector;

        private final BlockingQueue<TweetCollectionTask> windowOverdueTaskQueue;

        private final AtomicLong counter = new AtomicLong(1);

        private final boolean quickScheduling;

        ConnectorSchedulingTask(TwitterConnector connector, BlockingQueue<TweetCollectionTask> windowOverdueTaskQueue, boolean quickScheduling) {
            this.connector = connector;
            this.windowOverdueTaskQueue = windowOverdueTaskQueue;
            this.quickScheduling = quickScheduling;
        }

        @Override
        public void run() {
            long time = counter.getAndIncrement();
            try {
                LOG.info("\n\n\n**********START SCHEDULING CONNECTOR: ID-{}, TIME-{}**********\n\n\n", connector.getId(), time);
                long start = System.currentTimeMillis();
                schedule(connector, windowOverdueTaskQueue, quickScheduling);
                LOG.info("\n\n\n**********END SCHEDULING CONNECTOR ID-{}, TIME-{}, ELAPSED-TIME: {}ms**********\n\n\n", connector.getId(), time,
                        System.currentTimeMillis() - start);
            } catch (Exception e) {
                LOG.error("CONNECTOR: ID-" + connector.getRemoteId() + ", TIME-" + time, ";" + e.getMessage(), e);
            }
        }
    }

    private final Map<String, ReentrantLock> lockByConnector = new HashMap<>();

    private final Object lockMonitor = new Object();

    @Transactional
    private void schedule(TwitterConnector connector, final BlockingQueue<TweetCollectionTask> windowOverdueTasks, boolean quickScheduling) {

        ReentrantLock schedulingLock = getSchedulingLock(connector);
        schedulingLock.lock();
        try {

            TopicSchedulingInformation topicSchedulingInformation = topicSchedulingInformationRepository.findByTwitterConnector(connector);

            if (topicSchedulingInformation == null) {
                topicSchedulingInformation = new TopicSchedulingInformation(0L, connector);
                topicSchedulingInformation.createdBy(TWITTER_SCHEDULER);
            }

            // get search tweets rate limit
            Twitter twitter = twitterClientCache.get(connector);
            Map<String, RateLimitStatus> rateLimitStatusByApi;
            try {
                rateLimitStatusByApi = twitter.getRateLimitStatus("search");
            } catch (TwitterException e) {
                throw new TweetException(e.getMessage(), e.getErrorCode(), e);
            }
            RateLimitStatus searchTweetsRateLimitStatus = rateLimitStatusByApi.get("/search/tweets");
            final int maxRequestsPerInterval = Math.min(connector.getRequestsPerInterval(), (int) (searchTweetsRateLimitStatus.getRemaining() * connector.requestRate()));
            if (maxRequestsPerInterval == 0) {
                throw new SearchTweetsWindowResourceException("No remaining requests left in the current window for " + connector +
                        ". \nHave to wait for " + searchTweetsRateLimitStatus.getSecondsUntilReset() + "s");
            }

            final SearchTweetsWindowResource searchTweetsWindowResource = new SearchTweetsWindowResource(connector, searchTweetsRateLimitStatus, maxRequestsPerInterval);
            if (!quickScheduling) {
                if (!searchTweetsWindowResource.ensureValidResource()) {
                    // this window is too short, skip this window
                    LOG.info("Twitter window is too short and will be reset at '{}', current time:'{}'",
                            DateUtils.isoDateTimeFormat(searchTweetsWindowResource.getResetTimeInMillis()),
                            DateUtils.isoDateTimeFormat(System.currentTimeMillis()));
                    return;
                }
            }

            // the list containing all tasks in this window
            List<TweetCollectionTask> submittingTasks = new LinkedList<>();

            List<TweetCollectionTask> overdueTasks = new LinkedList<>();
            windowOverdueTasks.drainTo(overdueTasks);

            // refresh over-due tasks of the previous window
            overdueTasks
                    .stream()
                    .map(overdueTask -> new TweetCollectionTask(
                            searchTweetsWindowResource,
                            overdueTask.getTopic(),
                            overdueTask.getKeyword(),
                            requestsPerTopic,
                            maxTweetsPerRequests,
                            tweetService,
                            keywordService,
                            translatorService,
                            topicStatusService,
                            klaverService,
                            monitorService,
                            tweetProcessingExecutor,
                            windowOverdueTasks))
                    .forEach(submittingTasks::add);

            // to maximize number of topics being serviced, a topic is given only one request, so only one searchQuery is selected to service
            // (if the number of topics are greater than number of remaining requests)
            int maxTopicsCanService = maxRequestsPerInterval - overdueTasks.size();
            if (quickScheduling) {
                maxTopicsCanService = topicSchedulingInformation.getLastAvailableRequests();
            }
            final Date currentDate = new Date();
            final long topicsNeedServicing = topicService.countTopicsNeedServicing(currentDate, connector.getId());

            List<Topic> servicingTopics = topicService.findToService(
                    currentDate,
                    connector.getId(),
                /* continue servicing topics from the last serviced id, to implement round-robin scheduling */
                    topicSchedulingInformation.getLastServicedTopicId(),
                    maxTopicsCanService);

            if (topicsNeedServicing > 0) {
                if (shouldTakeMoreToService(maxTopicsCanService, servicingTopics, topicsNeedServicing)) {
                    // we're at the end of the list, take some more from the beginning
                    long takeMore;
                    if (topicsNeedServicing > maxTopicsCanService) {
                        // demand > supply, service what we can
                        takeMore = maxTopicsCanService - servicingTopics.size();
                    } else {
                        // supply > demand, service all
                        takeMore = topicsNeedServicing - servicingTopics.size();
                    }

                    // take more topics to service, implementing round-robin scheduling
                    List<Topic> moreServicingTopics = topicService.findToService(
                            currentDate,
                            connector.getId(),
                    /* search since id = 0 */
                            0,
                            takeMore);

                    servicingTopics.addAll(moreServicingTopics);
                }
            }

            List<KeywordSchedulingInformation> keywordSchedulingInformations = keywordSchedulingInformationRepository.findByTopicIn(servicingTopics);
            Map<Topic, KeywordSchedulingInformation> keywordSchedulingInformationsByTopic = keywordSchedulingInformations
                    .stream()
                    .collect(Collectors
                            .toMap(
                                    KeywordSchedulingInformation::getTopic,
                                    Function.identity()));

            // service topics in the round-robin style
            int maxRequests = maxTopicsCanService;
            Queue<Topic> servicingTopicsQueue = new ArrayDeque<>(servicingTopics);
            Multiset<Keyword> servicedKeywords = HashMultiset.create();
            Topic lastServicedTopic = null;
            while (maxRequests > 0 && !servicingTopicsQueue.isEmpty()) {
                // retrieve and remove the topic at the head of the queue to service
                Topic servicingTopic = servicingTopicsQueue.poll();

                KeywordSchedulingInformation keywordSchedulingInformation = keywordSchedulingInformationsByTopic.get(servicingTopic);
                if (keywordSchedulingInformation == null) {
                    // first created, last serviced searchQuery id is set to 0L
                    keywordSchedulingInformation = new KeywordSchedulingInformation(servicingTopic, 0L);
                    keywordSchedulingInformation.createdBy(TWITTER_SCHEDULER);
                    keywordSchedulingInformationsByTopic.put(keywordSchedulingInformation.getTopic(), keywordSchedulingInformation);
                }

                // look for a searchQuery to service in a topic, it's done in round-robin style
                Keyword servicingKeyword = servicingTopic.findKeywordToService(keywordSchedulingInformation.getLastServicedKeywordId());
                if (servicingKeyword == null) {
                    // skip servicing this topic, since all its keywords have been serviced already
                    // and offer the turn/request for the next topics in the queue
                    servicingTopicsQueue.offer(servicingTopic);
                    continue;
                }

                maxRequests--;
                submittingTasks.add(new TweetCollectionTask(
                        searchTweetsWindowResource,
                        servicingTopic,
                        servicingKeyword,
                        requestsPerTopic,
                        maxTweetsPerRequests,
                        tweetService,
                        keywordService,
                        translatorService,
                        topicStatusService,
                        klaverService,
                        monitorService,
                        tweetProcessingExecutor,
                        windowOverdueTasks
                ));

                keywordSchedulingInformation.setLastServicedKeywordId(servicingKeyword.getId());
                keywordSchedulingInformation.updatedBy(TWITTER_SCHEDULER);

                // a topic is seen as serviced when one of its keywords is actually serviced
                // servicingTopic.serviced();

                // insert the serviced topic at the end of the queue, to be serviced next round if there're requests left
                servicingTopicsQueue.offer(servicingTopic);

                servicedKeywords.add(servicingKeyword);
                lastServicedTopic = servicingTopic;

                if (servicedKeywords.count(servicingKeyword) > maxRequestsPerKeyword) {
                    break;
                }
            }

            if (lastServicedTopic != null) {
                topicSchedulingInformation.setLastServicedTopicId(lastServicedTopic.getId());
            }

            topicSchedulingInformation.setLastAvailableRequests(maxRequests);
            topicSchedulingInformation.setLastWindowResetTime(searchTweetsWindowResource.getResetTimeInMillis());

            topicService.save(servicingTopics);
            keywordSchedulingInformationRepository.save(keywordSchedulingInformationsByTopic.values());
            topicSchedulingInformation.updatedBy(TWITTER_SCHEDULER);
            topicSchedulingInformationRepository.save(topicSchedulingInformation);

            // everything succeeded, execute the tasks
            submitTweetCollectionTasks(submittingTasks);
        } finally {
            schedulingLock.unlock();
        }
    }

    private ReentrantLock getSchedulingLock(TwitterConnector connector) {
        ReentrantLock schedulingLock;
        synchronized (lockMonitor) {
            schedulingLock = lockByConnector.get(connector.getConsumerKey());
            if (schedulingLock == null) {
                schedulingLock = new ReentrantLock();
                lockByConnector.put(connector.getConsumerKey(), schedulingLock);
            }
        }
        return schedulingLock;
    }

    private void submitTweetCollectionTasks(List<TweetCollectionTask> submittingTasks) {
        Map<Long, List<TweetCollectionTask>> tasksByKId = submittingTasks
                .stream()
                .collect(Collectors.groupingBy(t -> t.getKeyword().getId()));

        tasksByKId.forEach((kid, tasks) -> {
            if (tasks.size() > 1) {

                TweetCollectionTask current = tasks.get(0);
                // the first, waiting for no latch
                current.setWaitLatch(null);
                current.setHoldLatch(new CountDownLatch(1));
                current.order(0);
                for (int i = 1; i < tasks.size(); i++) {
                    TweetCollectionTask next = tasks.get(i);
                    next.setWaitLatch(current.getHoldLatch());

                    // the last task holds no latch
                    if (i < tasks.size() - 1) {
                        next.setHoldLatch(new CountDownLatch(1));
                    }
                    next.order(i);
                    current = next;
                }
            }
        });

        submittingTasks.forEach(tweetCollectionExecutor::execute);
    }

    private boolean shouldTakeMoreToService(int maxTopicCanService, List<Topic> servicingTopics, long topicsNeedServicing) {
        // reaching to the end, and there're some requests left, take some more topics from the beginning to service.
        return maxTopicCanService > servicingTopics.size() && topicsNeedServicing > servicingTopics.size();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        eventBus.register(new TopicEventListener());
    }

    class TopicEventListener {
        @Subscribe
        public void topicCreated(TopicCreated event) {
            quickSchedule(event.getTopic().getTwitterConnector());
        }

        @Subscribe
        public void topicUpdated(TopicUpdated event) {
            quickSchedule(event.getTopic().getTwitterConnector());
        }

    }

}
