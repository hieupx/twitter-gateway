package com.senseinfosys.pubsense.gateway.domain.twitter.topic;

import com.senseinfosys.pubsense.gateway.domain.core.DomainException;

public class TopicException extends DomainException {

    private static final long serialVersionUID = -2841283220613798268L;
    public static final int RUN_STATUS_NOT_SUPPORTED = 3;
    public static final int DISTANCE_UNIT_NOT_SUPPORTED = 4;

    public TopicException(int errorCode, String message) {
        super(DomainType.TOPIC, errorCode, message);
    }

    public TopicException(int errorCode, Throwable e) {
        super(DomainType.TOPIC, errorCode, e);
    }

}
