package com.senseinfosys.pubsense.gateway.domain.twitter;

import com.senseinfosys.pubsense.gateway.domain.app.Application;
import com.senseinfosys.pubsense.gateway.domain.connector.Connector;
import com.senseinfosys.pubsense.gateway.domain.connector.ConnectorSupport;
import com.senseinfosys.pubsense.gateway.domain.core.DomainService;
import com.senseinfosys.pubsense.gateway.domain.twitter.scheduling.SchedulingInformationService;
import com.senseinfosys.pubsense.gateway.domain.twitter.scheduling.TwitterScheduler;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by toan on 10/5/16.
 */
@Service
public class TwitterConnectorService extends DomainService implements ConnectorSupport {

    @Autowired
    private TwitterConnectorRepository twitterConnectorRepository;

    @Autowired
    TwitterScheduler twitterScheduler;

    @Autowired
    private TwitterClientCache twitterClientCache;

    @Autowired
    private TopicService topicService;

    @Autowired
    private SchedulingInformationService schedulingInformationService;

    private void testCredentials(TwitterConnector connector) {
        twitterClientCache.get(connector);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void delete(String remoteId, Application app) {
        TwitterConnector twitterConnector = twitterConnectorRepository.findByRemoteIdAndApplication(remoteId, app);
        ensureConnectorExists(twitterConnector);
        deleteReferences(twitterConnector);
        twitterConnectorRepository.delete(twitterConnector);
        twitterScheduler.unSchedule(twitterConnector);
        twitterClientCache.remove(twitterConnector);
    }

    private void deleteReferences(TwitterConnector twitterConnector) {
        topicService.deleteByTwitterConnector(twitterConnector);
        schedulingInformationService.deleteTopicSchedulingInformation(twitterConnector);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Connector create(TwitterConnector twitterConnector) {
        testCredentials(twitterConnector);
        TwitterConnector existingConnector = twitterConnectorRepository
                .findByRemoteIdAndApplication(twitterConnector.getRemoteId(), twitterConnector.getApplication());
        ensureConnectorNotExisting(existingConnector);
        twitterConnector.createdBy(getCurrentAppId());
        TwitterConnector savedTwitterConnector = twitterConnectorRepository.save(twitterConnector);
        twitterScheduler.schedule(savedTwitterConnector);
        return savedTwitterConnector;
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Connector update(TwitterConnector twitterConnector) {
        testCredentials(twitterConnector);
        TwitterConnector existingConnector = twitterConnectorRepository
                .findByRemoteIdAndApplication(twitterConnector.getRemoteId(), twitterConnector.getApplication());
        ensureConnectorExists(existingConnector);
        existingConnector.update(twitterConnector, getCurrentAppId());
        TwitterConnector updatedTwitterConnector = twitterConnectorRepository.save(existingConnector);
        twitterScheduler.reSchedule(updatedTwitterConnector);
        twitterClientCache.remove(updatedTwitterConnector);
        return updatedTwitterConnector;

    }

    public TwitterConnector find(String twitterConnectorId, Application app) {
        if (twitterConnectorId == null) {
            return null;
        }
        return twitterConnectorRepository.findByRemoteIdAndApplication(twitterConnectorId, app);
    }

    public void scheduleAll() {
        twitterConnectorRepository.findAll().forEach(twitterScheduler::schedule);
    }

    public TwitterScheduler getTwitterScheduler() {
        return twitterScheduler;
    }

}
