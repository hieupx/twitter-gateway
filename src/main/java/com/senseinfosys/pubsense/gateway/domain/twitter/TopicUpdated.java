package com.senseinfosys.pubsense.gateway.domain.twitter;

import com.senseinfosys.pubsense.gateway.domain.twitter.topic.Topic;

/**
 * Created by toan on 11/11/16.
 */
public class TopicUpdated extends TopicEvent {

    public TopicUpdated(Topic topic) {
        super(topic);
    }
}
