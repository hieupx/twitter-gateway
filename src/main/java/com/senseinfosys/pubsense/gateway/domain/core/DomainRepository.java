package com.senseinfosys.pubsense.gateway.domain.core;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface DomainRepository<E extends Domain> extends JpaRepository<E, Long> {
}