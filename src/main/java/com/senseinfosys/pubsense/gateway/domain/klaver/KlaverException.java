package com.senseinfosys.pubsense.gateway.domain.klaver;

import com.senseinfosys.pubsense.gateway.domain.core.DomainException;

/**
 * Created by toan on 10/13/16.
 */
public class KlaverException extends DomainException {

    public static final int NO_KLAVER_CONNECTOR = 1;

    public static final int POST_TWEETS_ERROR = 2;

    public KlaverException(int errorCode, String message) {
        this(errorCode, message, null);
    }

    public KlaverException(int errorCode, String message, Throwable e) {
        super(DomainType.KLAVER, errorCode, message, e);
    }
}
