package com.senseinfosys.pubsense.gateway.domain.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class UserService {

    private UserRepository userRepository;

    private String salt;

    @Autowired
    public UserService(UserRepository userRepository, Environment env) {
        this.userRepository = userRepository;
        salt = env.getProperty("bcrypt.salt");
    }

    @Transactional
    public User save(User user) {
        user.setPassword(hash(user.getPassword()));
        return userRepository.save(user);
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Transactional(readOnly = true)
    public User findByUsernameAndPassword(String username, String password) {
        return userRepository.findByUsernameAndPassword(username, hash(password));
    }

    private String hash(String input) {
        return BCrypt.hashpw(input, salt);
    }
}
