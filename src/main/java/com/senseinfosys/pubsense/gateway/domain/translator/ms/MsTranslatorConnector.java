package com.senseinfosys.pubsense.gateway.domain.translator.ms;

import com.senseinfosys.pubsense.gateway.domain.translator.TranslatorConnector;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Min;

@Entity
@Table(
    name = "ms_azure_translator_connector",
    uniqueConstraints = {@UniqueConstraint(columnNames = {"application_id", "remoteId"})}
)
public class MsTranslatorConnector extends TranslatorConnector {

    {
        this.type = ConnectorType.MS_TRANSLATOR;
    }

    @Min(1)
    private long charsPerMonth;

    @NotBlank
    @Column(unique=true)
    private String subscriptionKey;

    public MsTranslatorConnector() {
    }

    public MsTranslatorConnector(long charsPerMonth, String subscriptionKey) {
        this.charsPerMonth = charsPerMonth;
        this.subscriptionKey = subscriptionKey;
    }

    public long getCharsPerMonth() {
        return charsPerMonth;
    }

    public String getSubscriptionKey() {
        return subscriptionKey;
    }

	public boolean update(MsTranslatorConnector connector, String updatedBy) {
        if (connector.charsPerMonth > 0) {
            this.charsPerMonth = connector.charsPerMonth;
        }
        if (connector.subscriptionKey != null) {
            this.subscriptionKey = connector.subscriptionKey;
        }
        this.updatedBy(updatedBy);
        return true;
    }

    @Override
    public String toString() {
        return "MsTranslatorConnector(" +
                "uuid=" + remoteId + ") ";
    }
}
