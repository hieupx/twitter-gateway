package com.senseinfosys.pubsense.gateway.web.connector;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.senseinfosys.pubsense.gateway.domain.connector.Connector;
import com.senseinfosys.pubsense.gateway.domain.klaver.KlaverConnector;

/**
 * Created by toan on 10/5/16.
 */
@JsonDeserialize
public class RequestKlaverConnector extends RequestConnector {

    public String token;

    @Override
    public Connector connector() {
        return new KlaverConnector(token).remoteId(id);
    }

    public String getToken() {
        return token;
    }
}
