package com.senseinfosys.pubsense.gateway.web.topic;

import com.senseinfosys.pubsense.gateway.domain.twitter.topic.Topic;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.TopicService;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.TopicStatus;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.TopicStatusService;
import com.senseinfosys.pubsense.gateway.infrastructure.date.DateUtils;
import com.senseinfosys.pubsense.gateway.web.support.Response;
import com.senseinfosys.pubsense.gateway.web.support.ResponseSupport;
import com.senseinfosys.pubsense.gateway.web.support.RestApiV1;
import javaslang.Tuple2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@RestApiV1
@CrossOrigin
public class TopicController implements ResponseSupport {

    @Autowired
    private TopicService topicService;

    @Autowired
    private TopicStatusService topicStatusService;

    @PostMapping(path = "/apps/{appId}/topics")
    public Response createTopic(@RequestBody RequestTopic requestTopic, HttpServletResponse response) {
        Topic topic = topicService.create(requestTopic);
        topic.getKeywords().forEach(k -> k.topic(null));
        return createdOk(topic, response);
    }


    @PostMapping(path = "/apps/{appId}/topics/bulk")
    public Response createTopics(@RequestBody Collection<RequestTopic> requestTopics, HttpServletResponse response) {
        topicService.create(requestTopics);
        return createdOk("", response);
    }

    @PutMapping(path = "/apps/{appId}/topics/{id}")
    public Response updateTopic(@RequestBody RequestTopic requestTopic, HttpServletResponse response) {
        Topic topic = topicService.update(requestTopic);
        topic.getKeywords().forEach(k -> k.topic(null));
        return updatedOk(topic, response);
    }

    @PutMapping(path = "/apps/{appId}/topics/bulk")
    public Response updateTopics(@RequestBody Collection<RequestTopic> requestTopics, HttpServletResponse response) {
        topicService.update(requestTopics);
        return updatedOk("", response);
    }

    @DeleteMapping(path = "/apps/{appId}/topics/{id}")
    public void deleteTopic(@PathVariable String id, @PathVariable String appId, HttpServletResponse response) {
        topicService.delete(id, appId);
        deletedOk(response);
    }

    @DeleteMapping(path = "/apps/{appId}/topics/bulk")
    public void deleteTopics(@RequestBody Collection<String> ids, @PathVariable String appId, HttpServletResponse response) {
        topicService.delete(ids, appId);
        deletedOk(response);
    }

    @PostMapping(path = "/apps/{appId}/topics/statuses")
    public Response listStatuses(@RequestBody Map<String, Object> data, HttpServletResponse response) {
        Collection<String> topicIds = (Collection<String>) data.get("topicIds");

        Boolean latest = (Boolean) data.get("latest");

        // get latest by default
        List<Tuple2<Topic, TopicStatus>> latestStatuses = topicStatusService.findLatest(topicIds);
        List<ResponseLatestStatus> results = latestStatuses
                .stream()
                .map(tuple ->
                        new ResponseLatestStatus(
                                tuple._1.getRemoteId(),
                                tuple._1.getRunStatus().name(),
                                DateUtils.isoDateTimeFormat(tuple._1.getLastServiced()),
                                tuple._2.getServiceStatus().name()).details(tuple._2.getServiceStatus().getKey(), tuple._2.details()))
                .collect(Collectors.toList());

        return ok(results);
    }
}
