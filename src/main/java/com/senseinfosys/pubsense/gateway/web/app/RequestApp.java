package com.senseinfosys.pubsense.gateway.web.app;

public class RequestApp {

    private String clientId;

    public RequestApp() {
    }

    public RequestApp(String clientId) {
        this.clientId = clientId;
    }

    public String getClientId() {
        return clientId;
    }
}
