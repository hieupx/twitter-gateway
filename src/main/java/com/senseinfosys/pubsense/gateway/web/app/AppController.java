package com.senseinfosys.pubsense.gateway.web.app;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.senseinfosys.pubsense.gateway.domain.app.AppService;
import com.senseinfosys.pubsense.gateway.domain.app.Application;
import com.senseinfosys.pubsense.gateway.infrastructure.security.TokenService;
import com.senseinfosys.pubsense.gateway.web.support.Response;
import com.senseinfosys.pubsense.gateway.web.support.ResponseSupport;
import com.senseinfosys.pubsense.gateway.web.support.RestApiV1;

@RestApiV1
@CrossOrigin
public class AppController implements ResponseSupport {

    AppService appService;

    TokenService tokenService;

    @Autowired
    public AppController(AppService appService, TokenService tokenService) {
        this.appService = appService;
        this.tokenService = tokenService;
    }

    @PostMapping(path = "/apps/register")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Response registerApp(@RequestBody RequestApp requestApp, HttpServletResponse response) {
        Application app = appService.createApp(requestApp);
        return createdOk(new ResponseApp(app.getAppId(), app.getAppSecret()), response);
    }

    @GetMapping(path = "/apps")
    public Response listApps() {
        return page(appService.findAll());
    }

    @PostMapping(path = "/apps/token")
    public Response getAccessToken() {
        String appId = SecurityContextHolder.getContext().getAuthentication().getName();
        return ok(tokenService.getJwtResponse(appId));
    }
}
