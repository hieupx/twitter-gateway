package com.senseinfosys.pubsense.gateway.web.connector;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.senseinfosys.pubsense.gateway.domain.connector.Connector.ConnectorType;
import com.senseinfosys.pubsense.gateway.infrastructure.json.JsonUtils;

import java.io.IOException;
import java.util.Map;

public class ConnectorDeserializer extends JsonDeserializer<RequestConnector> {

    @Override
    public RequestConnector deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException, JsonProcessingException {
        ObjectCodec objectCodec = jp.getCodec();
        Map<?, ?> map = objectCodec.readValue(jp, Map.class);
        String json = JsonUtils.serialize(map);
        ConnectorType type = ConnectorType.fromString((String) map.get("type"));
        if (type == null) {
            return null;
        }
        switch (type) {
            case TWITTER:
                return JsonUtils.deserialize(json, RequestTwitterConnector.class);
            case MS_TRANSLATOR:
                return JsonUtils.deserialize(json, RequestMsTranslatorConnector.class);
            case KLAVER:
                return JsonUtils.deserialize(json, RequestKlaverConnector.class);
        }
        return null;
    }

}
