package com.senseinfosys.pubsense.gateway.web.connector;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.senseinfosys.pubsense.gateway.domain.connector.Connector;

@JsonDeserialize(using = ConnectorDeserializer.class)
public abstract class RequestConnector {

    public String id;

    public String appId;

    public String type;

    public String getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public String getAppId() {
        return appId;
    }

    public abstract Connector connector();

}
