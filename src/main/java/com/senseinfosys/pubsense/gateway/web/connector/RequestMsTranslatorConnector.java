package com.senseinfosys.pubsense.gateway.web.connector;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.senseinfosys.pubsense.gateway.domain.connector.Connector;
import com.senseinfosys.pubsense.gateway.domain.translator.ms.MsTranslatorConnector;

@JsonDeserialize
public class RequestMsTranslatorConnector extends RequestConnector {

    public long charsPerMonth;

    String subscriptionKey;

	public long getCharsPerMonth() {
        return charsPerMonth;
    }

    public String getSubscriptionKey() {
        return subscriptionKey;
    }

    public RequestMsTranslatorConnector() {
    }

    public RequestMsTranslatorConnector(long charsPerMonth, String subscriptionKey) {
        this.charsPerMonth = charsPerMonth;
        this.subscriptionKey = subscriptionKey;
    }

    @Override
    public Connector connector() {
        return new MsTranslatorConnector(charsPerMonth, subscriptionKey).remoteId(id);
    }

}
