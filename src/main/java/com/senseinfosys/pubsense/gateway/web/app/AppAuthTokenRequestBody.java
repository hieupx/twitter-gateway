package com.senseinfosys.pubsense.gateway.web.app;

public class AppAuthTokenRequestBody {

    private String appId;

    private String appSecret;

    public AppAuthTokenRequestBody(String appId, String appSecret) {
        this.appId = appId;
        this.appSecret = appSecret;
    }

    public AppAuthTokenRequestBody() {
    }

    public String getAppId() {
        return appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

}
