package com.senseinfosys.pubsense.gateway.web.topic;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;

public class RequestTopic {

    String id;

    String appId;

    String twitterConnectorId;

    String msTranslatorConnectorId;

    String klaverConnectorId;

    String description;

    String runStatus;

    Long since;

    Long until;

    Double lat;

    Double lon;

    String timeZone;

    Double radius;

    String radiusUnit;

    Boolean findNegativeEmoticons;

    Boolean findPositiveEmoticons;

    Boolean findQuestions;

    Boolean findOriginalPosts;

    Collection<RequestKeyword> keywords;

    public String getTimeZone() {
        return timeZone;
    }

    public String getId() {
        return id;
    }

    public String getAppId() {
        return appId;
    }

    public String getDescription() {
        return description;
    }

    public String getRunStatus() {
        return runStatus;
    }

    public Long getSince() {
        return since;
    }

    public String getKlaverConnectorId() {
        return klaverConnectorId;
    }

    public Date since() {
        if (since != null) {
            return new Date(since);
        }
        return null;
    }

    public Date until() {
        if (until != null) {
            return new Date(until);
        }
        return null;
    }

    public Long getUntil() {
        return until;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLon() {
        return lon;
    }

    public Double getRadius() {
        return radius;
    }

    public String getRadiusUnit() {
        return radiusUnit;
    }

    public Boolean getFindNegativeEmoticons() {
        return findNegativeEmoticons;
    }

    public Boolean getFindPositiveEmoticons() {
        return findPositiveEmoticons;
    }

    public Boolean getFindQuestions() {
        return findQuestions;
    }

    public Boolean getFindOriginalPosts() {
        return findOriginalPosts;
    }

    public String getTwitterConnectorId() {
        return twitterConnectorId;
    }

    public String getMsTranslatorConnectorId() {
        return msTranslatorConnectorId;
    }

    public Collection<RequestKeyword> getKeywords() {
        if (keywords == null) {
            keywords = Collections.emptyList();
        }
        return keywords;
    }

    public void setSince(Long since) {
        this.since = since;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    public void setRadiusUnit(String radiusUnit) {
        this.radiusUnit = radiusUnit;
    }

    public void setUntil(Long until) {
        this.until = until;
    }
}
