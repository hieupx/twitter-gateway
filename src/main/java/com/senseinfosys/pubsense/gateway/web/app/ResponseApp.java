package com.senseinfosys.pubsense.gateway.web.app;

public class ResponseApp {

    private String appId;

    private String appSecret;

    public ResponseApp(String appId, String appSecret) {
        this.appId = appId;
        this.appSecret = appSecret;
    }

    public ResponseApp() {
    }

    public String getAppId() {
        return appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

}
