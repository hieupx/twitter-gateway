package com.senseinfosys.pubsense.gateway.web.topic;

public class RequestKeyword {

    String id;

    String query;

    String lang;

    String type;

    public RequestKeyword() {
    }

    public RequestKeyword(String id, String query, String lang, String type) {
        this.id = id;
        this.query = query;
        this.lang = lang;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public String getQuery() {
        return query;
    }

    public String getLang() {
        return lang;
    }

    public String getType() {
        return type;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
