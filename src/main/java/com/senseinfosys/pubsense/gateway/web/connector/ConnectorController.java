package com.senseinfosys.pubsense.gateway.web.connector;

import com.senseinfosys.pubsense.gateway.domain.connector.ConnectorService;
import com.senseinfosys.pubsense.gateway.web.support.Response;
import com.senseinfosys.pubsense.gateway.web.support.ResponseSupport;
import com.senseinfosys.pubsense.gateway.web.support.RestApiV1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@RestApiV1
@CrossOrigin
public class ConnectorController implements ResponseSupport {

    @Autowired
    private ConnectorService connectorService;

    @PostMapping(path = "/apps/{appId}/connectors/{type}")
    public Response createConnector(@RequestBody RequestConnector requestConnector, HttpServletResponse response) {
        return createdOk(connectorService.create(requestConnector), response);
    }

    @PutMapping(path = "/apps/{appId}/connectors/{type}/{id}")
    public Response updateConnector(@RequestBody RequestConnector requestConnector, HttpServletResponse response) {
        return updatedOk(connectorService.update(requestConnector), response);
    }

    @DeleteMapping(path = "/apps/{appId}/connectors/{type}/{id}")
    public void deleteConnector(@PathVariable String id, @PathVariable String type, @PathVariable String appId,
                                HttpServletResponse response) {
        connectorService.delete(id, type, appId);
        deletedOk(response);
    }
}
