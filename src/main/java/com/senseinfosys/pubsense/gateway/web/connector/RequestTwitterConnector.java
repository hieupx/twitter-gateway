package com.senseinfosys.pubsense.gateway.web.connector;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.senseinfosys.pubsense.gateway.domain.connector.Connector;
import com.senseinfosys.pubsense.gateway.domain.twitter.TwitterConnector;

@JsonDeserialize
public class RequestTwitterConnector extends RequestConnector {

    String consumerKey;

    String consumerSecret;

    String accessToken;

    String accessTokenSecret;

    int intervalLength;

    int requestsPerInterval;

    public String getConsumerKey() {
        return consumerKey;
    }

    public String getConsumerSecret() {
        return consumerSecret;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getAccessTokenSecret() {
        return accessTokenSecret;
    }

    public int getIntervalLength() {
        return intervalLength;
    }

    public int getRequestsPerInterval() {
        return requestsPerInterval;
    }

    public RequestTwitterConnector() {
    }

    public RequestTwitterConnector(String consumerKey, String consumerSecret, String accessToken,
            String accessTokenSecret, int intervalLength, int requestsPerInterval) {
        this.consumerKey = consumerKey;
        this.consumerSecret = consumerSecret;
        this.accessToken = accessToken;
        this.accessTokenSecret = accessTokenSecret;
        this.intervalLength = intervalLength;
        this.requestsPerInterval = requestsPerInterval;
    }

    @Override
    public Connector connector() {
        return new TwitterConnector(consumerKey, consumerSecret, accessToken, accessTokenSecret, intervalLength,
                requestsPerInterval).remoteId(id);
    }
}
