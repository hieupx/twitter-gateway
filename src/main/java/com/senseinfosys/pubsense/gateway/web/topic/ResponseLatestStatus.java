package com.senseinfosys.pubsense.gateway.web.topic;


import java.util.HashMap;

/**
 * Created by toan on 10/3/16.
 */
public class ResponseLatestStatus extends HashMap<String, Object> {

    public ResponseLatestStatus() {
    }

    public ResponseLatestStatus(String topicId, String runStatus, String lastServiced, String serviceStatus) {
        put("topicId", topicId);
        put("runStatus", runStatus);
        put("lastServiced", lastServiced);
        put("serviceStatus", serviceStatus);
    }

    public ResponseLatestStatus details(String key, Object details) {
        put(key, details);
        return this;
    }

}
