package com.senseinfosys.pubsense.gateway.infrastructure.annotation;

import java.lang.annotation.*;

/**
 * Created by toan on 9/9/16.
 * Indicates that the field is safe to be shared between multiple threads even with modification
 */

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ThreadSafeShared {
    String value();
}
