package com.senseinfosys.pubsense.gateway.infrastructure.annotation;

import java.lang.annotation.*;

/**
 * Created by toan on 9/7/16.
 * Hints that the field should be treated as immutable, although it is not technically immutable
 */

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface EffectivelyImmutable {
    String value();
}
