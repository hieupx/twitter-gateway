package com.senseinfosys.pubsense.gateway.infrastructure;

import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.senseinfosys.pubsense.gateway.infrastructure.json.JsonUtils;

/**
 * Created by toan on 10/3/16.
 */
public class RestClient {

    static {
        Unirest.setObjectMapper(new ObjectMapper() {
            public <T> T readValue(String value, Class<T> valueType) {
                return JsonUtils.deserialize(value, valueType);
            }

            public String writeValue(Object value) {
                return JsonUtils.serialize(value);
            }
        });
    }

    public static void init() {

    }
}