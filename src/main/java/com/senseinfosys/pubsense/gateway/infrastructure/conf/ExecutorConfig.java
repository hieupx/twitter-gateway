package com.senseinfosys.pubsense.gateway.infrastructure.conf;

import com.senseinfosys.pubsense.gateway.infrastructure.number.NumberParser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static com.senseinfosys.pubsense.gateway.domain.twitter.scheduling.TwitterScheduler.*;

/**
 * Created by toan on 8/30/16.
 */

@Configuration
public class ExecutorConfig {

    @Bean(name = TWEET_COLLECTION_EXECUTOR)
    public ExecutorService tweetCollectionExecutor(Environment env) {
        int maxWorkerThreads = NumberParser.getInt(env.getProperty("tweet-collection.max-worker-threads"), 100);
        return Executors.newFixedThreadPool(maxWorkerThreads);
    }

    @Bean(name = TWEET_PROCESSING_EXECUTOR)
    public ExecutorService tweetsProcessingExecutor(Environment env) {
        int maxWorkerThreads = NumberParser.getInt(env.getProperty("tweet-processing.max-worker-threads"), 100);
        return Executors.newFixedThreadPool(maxWorkerThreads);
    }

    @Bean(name = TWITTER_SCHEDULER_EXECUTOR)
    public ScheduledExecutorService scheduledExecutorService(Environment env) {
        int maxWorkerThreads = NumberParser.getInt(env.getProperty("twitter.scheduler.max-worker-threads"),
                Runtime.getRuntime().availableProcessors());
        return Executors.newScheduledThreadPool(maxWorkerThreads);
    }
}
