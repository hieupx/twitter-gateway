package com.senseinfosys.pubsense.gateway.infrastructure.security;

import com.senseinfosys.pubsense.gateway.domain.core.DomainException;

public class TokenException extends DomainException {

    private static final long serialVersionUID = 1296717076321471486L;
    
    public static final int INVALID_TOKEN = 1;
    
    

    public TokenException(int errorCode, String message) {
        super(DomainType.TOKEN, errorCode, message);
    }

    public TokenException(int errorCode, Throwable throwable) {
        super(DomainType.TOKEN, errorCode, throwable);
    }

}
