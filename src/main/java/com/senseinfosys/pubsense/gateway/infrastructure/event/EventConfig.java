package com.senseinfosys.pubsense.gateway.infrastructure.event;

import com.google.common.eventbus.EventBus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by toan on 11/11/16.
 */

@Configuration
public class EventConfig {

    @Bean
    public EventBus eventBus() {
        return new EventBus();
    }
}
