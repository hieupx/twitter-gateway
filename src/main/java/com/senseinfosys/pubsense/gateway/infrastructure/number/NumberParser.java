package com.senseinfosys.pubsense.gateway.infrastructure.number;

/**
 * Created by toan on 9/23/16.
 */
public class NumberParser {

    public static int getInt(String text, int defaultValue) {
        try {
            return Integer.parseInt(text);
        } catch (Exception e) {
            return defaultValue;
        }
    }
}
