package com.senseinfosys.pubsense.gateway.infrastructure.monitor;

import com.github.krukow.clj_lang.PersistentHashMap;
import com.senseinfosys.pubsense.gateway.infrastructure.json.JsonUtils;
import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.metadata.HikariDataSourcePoolMetadata;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadPoolExecutor;

import static com.senseinfosys.pubsense.gateway.domain.twitter.scheduling.TwitterScheduler.TWEET_COLLECTION_EXECUTOR;
import static com.senseinfosys.pubsense.gateway.domain.twitter.scheduling.TwitterScheduler.TWEET_PROCESSING_EXECUTOR;

/**
 * Created by toan on 11/2/16.
 */
@Service
public class MonitorService {

    private static final Logger LOG = LoggerFactory.getLogger(MonitorService.class);

    ScheduledExecutorService exec;

    @Autowired
    HikariDataSource dataSource;

    @Autowired
    Environment env;

    @Qualifier(TWEET_COLLECTION_EXECUTOR)
    @Autowired
    ExecutorService tweetCollectionExecutor;

    @Qualifier(TWEET_PROCESSING_EXECUTOR)
    @Autowired
    ExecutorService tweetProcessingExecutor;

    public void monitor() {

        Map<String, Object> dbpoolInfo = getDBPoolInfo();

        Map<String, Object> tweetCollectionTPoolInfo = getThreadPoolInfo((ThreadPoolExecutor) tweetCollectionExecutor);

        Map<String, Object> tweetProcessingTPoolInfo = getThreadPoolInfo((ThreadPoolExecutor) tweetProcessingExecutor);

        LOG.info("DB_CONNECTION_POOL: {}; " +
                        "TWEET_COLLECTION_EXECUTOR: {}; " +
                        "TWEET_PROCESSING_EXECUTOR: {}",
                JsonUtils.serialize(dbpoolInfo),
                JsonUtils.serialize(tweetCollectionTPoolInfo),
                JsonUtils.serialize(tweetProcessingTPoolInfo));
    }

    private PersistentHashMap<String, Object> getDBPoolInfo() {
        HikariDataSourcePoolMetadata poolMetadata = new HikariDataSourcePoolMetadata(dataSource);
        return PersistentHashMap.create(
                "active", poolMetadata.getActive(),
                "pool_size", poolMetadata.getMax(),
                "usage", poolMetadata.getUsage());
    }

    private Map<String, Object> getThreadPoolInfo(ThreadPoolExecutor tweetCollectionThreadPoolExecutor) {
        return PersistentHashMap.create(
                "active", tweetCollectionThreadPoolExecutor.getActiveCount(),
                "pool_size", tweetCollectionThreadPoolExecutor.getCorePoolSize(),
                "completed_tasks", tweetCollectionThreadPoolExecutor.getCompletedTaskCount());
    }

}
