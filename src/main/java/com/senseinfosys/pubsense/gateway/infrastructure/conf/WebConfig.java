package com.senseinfosys.pubsense.gateway.infrastructure.conf;

import com.senseinfosys.pubsense.gateway.infrastructure.json.JsonUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
public class WebConfig extends WebMvcConfigurationSupport {

    /**
     * @return Override the ObjectMapper
     * http://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/htmlsingle/#howto-customize-the-jackson-objectmapper
     */
    @Bean
    public MappingJackson2HttpMessageConverter customJackson2HttpMessageConverter() {
        return new MappingJackson2HttpMessageConverter(JsonUtils.MAPPER);
    }

    /**
     * Extend ExceptionHandlerExceptionResolver if needed
     *   @Override
     *   protected ExceptionHandlerExceptionResolver createExceptionHandlerExceptionResolver() {
     *       return new ExceptionHandlerExceptionResolver();
     *   }
     */

}
