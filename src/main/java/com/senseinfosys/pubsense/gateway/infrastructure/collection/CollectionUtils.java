package com.senseinfosys.pubsense.gateway.infrastructure.collection;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by toan on 10/27/16.
 */
public class CollectionUtils {
    public static <T> List<List<T>> split(List<T> list, int splitSize) {

        List<List<T>> result = new ArrayList<>(list.size() / splitSize + 1);

        if (list.size() <= splitSize) {
            result.add(list);
            return result;
        }

        List<T> subList = new ArrayList<>(splitSize);
        for (int i = 0; i < list.size(); i++) {
            if (i != 0 && i % splitSize == 0) {
                result.add(subList);
                subList = new ArrayList<>(splitSize);
            }
            subList.add(list.get(i));
        }
        result.add(subList);
        return result;
    }
}
