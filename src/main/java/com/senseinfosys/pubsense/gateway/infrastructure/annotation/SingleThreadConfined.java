package com.senseinfosys.pubsense.gateway.infrastructure.annotation;

import java.lang.annotation.*;

/**
 * Created by toan on 9/13/16.
 * Indicates that the field is modified by a single thread, therefore no synchronization is required
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SingleThreadConfined {
    String value();
}
