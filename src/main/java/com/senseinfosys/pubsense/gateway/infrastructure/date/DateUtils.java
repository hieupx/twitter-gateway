package com.senseinfosys.pubsense.gateway.infrastructure.date;


import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

import static java.util.Calendar.*;

/**
 * Created by toan on 9/12/16.
 */
public class DateUtils {


    public static String isoDateFormat(Date date) {
        if (date == null) {
            return null;
        }
        return DateTimeFormatter.ISO_DATE.format(LocalDateTime.ofInstant(Instant.ofEpochMilli(date.getTime()), ZoneId.systemDefault()));
    }

    public static String isoDateTimeFormat(Date date) {
        if (date == null) {
            return null;
        }
        return isoDateTimeFormat(date.getTime());
    }

    public static String isoDateTimeFormat(long epochMillis) {
        return DateTimeFormatter.ISO_DATE_TIME.format(ZonedDateTime.ofInstant(Instant.ofEpochMilli(epochMillis), ZoneId.systemDefault()));
    }

    public static Date getBeginningOfDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(HOUR_OF_DAY, 0);
        cal.set(MINUTE, 0);
        cal.set(SECOND, 0);
        return cal.getTime();
    }

    public static Date getEndOfDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(HOUR_OF_DAY, 23);
        cal.set(MINUTE, 59);
        cal.set(SECOND, 59);
        return cal.getTime();
    }

}
