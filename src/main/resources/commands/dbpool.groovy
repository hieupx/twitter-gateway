package commands

import com.zaxxer.hikari.HikariDataSource
import org.crsh.cli.Command
import org.crsh.cli.Usage
import org.crsh.command.InvocationContext
import org.springframework.beans.factory.BeanFactory

class dbpool {

    def dataSource;

    @Usage("monitor db connection pool")
    @Command
    def main(InvocationContext context) {
        init(context)
        return "Active: " + dataSource.pool.activeConnections + ", PoolSize: " + dataSource.getMaximumPoolSize()
    }

    private HikariDataSource init(InvocationContext context) {
        if (dataSource == null) {
            Map<String, Object> attributes = context.getAttributes()
            BeanFactory factory = attributes.get("spring.beanfactory")
            dataSource = factory.getBean(HikariDataSource.class)
        }
    }

}