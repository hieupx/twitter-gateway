package com.senseinfosys.pubsense.gateway.web.twitter;

import com.google.common.eventbus.EventBus;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.KeywordService;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.TopicService;
import com.senseinfosys.pubsense.gateway.domain.twitter.tweet.Tweet;
import com.senseinfosys.pubsense.gateway.web.Failure;
import com.senseinfosys.pubsense.gateway.web.Success;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * Created by toan on 10/27/16.
 */

@RestController
public class FakeKlaver {

    @Autowired
    TopicService topicService;

    @Autowired
    KeywordService keywordService;

    @Autowired
    @Qualifier("test1")
    EventBus eventBus;

    @PostMapping(path = "/klaverrhcc/tweets")
    public void receiveTweets(@RequestBody Map<String, List<Tweet>> tweetData) {
        List<Tweet> tweets = tweetData.get("data");
        try {
            if (!tweets.isEmpty()) {
                new TweetAssertion(
                        topicService.findRemoteById(tweets.get(0).getTopicId()),
                        keywordService.findByRemoteId(tweets.get(0).getKeywordId()),
                        tweets).doAssert();
            }
            eventBus.post(new Success());
            eventBus.post(tweets);
        } catch (Error e) {
            eventBus.post(new Failure(e));
        }

    }
}
