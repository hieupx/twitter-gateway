package com.senseinfosys.pubsense.gateway.web.twitter;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.Sets;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.senseinfosys.pubsense.gateway.domain.translator.ms.MsTranslatorConnector;
import com.senseinfosys.pubsense.gateway.domain.twitter.TwitterConnector;
import com.senseinfosys.pubsense.gateway.domain.twitter.scheduling.TwitterScheduler;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.KeywordService;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.Topic;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.TopicService;
import com.senseinfosys.pubsense.gateway.domain.twitter.tweet.Tweet;
import com.senseinfosys.pubsense.gateway.infrastructure.id.IdFactory;
import com.senseinfosys.pubsense.gateway.web.Event;
import com.senseinfosys.pubsense.gateway.web.Failure;
import com.senseinfosys.pubsense.gateway.web.support.SuccessResponse;
import com.senseinfosys.pubsense.gateway.web.topic.RequestKeyword;
import com.senseinfosys.pubsense.gateway.web.topic.RequestTopic;
import com.senseinfosys.pubsense.gateway.web.topic.TopicControllerTest;
import org.joda.time.DateTimeConstants;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.internal.matchers.LessOrEqual;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

/**
 * Created by toan on 9/12/16.
 */
public class TwitterSchedulerTest extends TopicControllerTest {

    @Autowired
    TwitterScheduler twitterScheduler;

    @Autowired
    KeywordService keywordService;

    @Autowired
    TopicService topicService;

    @Autowired
    @Qualifier("test1")
    EventBus eventBus;

    static class AssertEventListener {

        final BlockingQueue<Event> eventQueue = new LinkedBlockingQueue<>();

        final Collection<Tweet> collectedTweets = new ConcurrentLinkedQueue<>();

        @Subscribe
        public void listen(Event event) {
            eventQueue.offer(event);
        }

        @Subscribe
        public void collect(Collection<Tweet> tweets) {
            collectedTweets.addAll(tweets);
        }

    }

    @Test
    public void testSchedulingAndUpdateTopic() throws InterruptedException {
        AssertEventListener assertEventListener = new AssertEventListener();
        eventBus.register(assertEventListener);
        TwitterConnector twitterConnector = createTwitterConnector(createRequestTwitterConnector(1, 2));
        MsTranslatorConnector msTranslatorConnector = createMsTranslatorConnector(createRequestMsTranslatorConnector());
        String klaverConnectorId = createKlaverConnector();

        RequestKeyword keyword = new RequestKeyword(IdFactory.getUUID(),
                "java", "en", "CUSTOM");
        List<RequestKeyword> requestKeywords = Arrays.asList(keyword);
        RequestTopic requestTopic = createRequestTopic(twitterConnector, msTranslatorConnector, "functional java",
                System.currentTimeMillis() - DateTimeConstants.MILLIS_PER_DAY * 1L,
                System.currentTimeMillis() + DateTimeConstants.MILLIS_PER_DAY * 1L,
                "STOPPED",
                requestKeywords, klaverConnectorId);
        createTopic(requestTopic);

        while (assertEventListener.eventQueue.size() < 2) ;

        List<?> failures = assertEventListener.eventQueue
                .stream()
                .filter(e -> e instanceof Failure)
                .collect(Collectors.toList());

        List<Failure> errors = (List<Failure>) failures;

        Set<Tweet> tweets = new HashSet<>(assertEventListener.collectedTweets);

        assertEventListener.collectedTweets.clear();
        assertEventListener.eventQueue.clear();
        requestTopic.setSince(System.currentTimeMillis() - DateTimeConstants.MILLIS_PER_DAY * 2L);
        updateTopic1(requestTopic);
        while (assertEventListener.eventQueue.size() < 2) ;
        List<?> failures1 = assertEventListener.eventQueue
                .stream()
                .filter(e -> e instanceof Failure)
                .collect(Collectors.toList());
        List<Failure> errors1 = (List<Failure>) failures1;

        Set<Tweet> tweets1 = new HashSet<>(assertEventListener.collectedTweets);

        Set<Tweet> duplicates = Sets.intersection(tweets1, tweets);

        Assert.assertThat(duplicates.size(), new LessOrEqual<>(2));
        requestTopic.setLat(1D);
        requestTopic.setLon(1D);
        requestTopic.setRadius(10D);
        requestTopic.setRadiusUnit("MILES");
        updateTopic1(requestTopic);
        Topic t1 = topicService.findRemoteById(requestTopic.getId());

        Assert.assertEquals(0, t1.getKeywords().iterator().next().getMaxTweetId());
        Assert.assertEquals(0, t1.getKeywords().iterator().next().getSinceTweetId());

        assertEventListener.collectedTweets.clear();
        assertEventListener.eventQueue.clear();
        while (assertEventListener.eventQueue.size() < 2) ;

        requestTopic.getKeywords().iterator().next().setQuery("clojure");
        updateTopic1(requestTopic);

        Assert.assertEquals(0, t1.getKeywords().iterator().next().getMaxTweetId());
        Assert.assertEquals(0, t1.getKeywords().iterator().next().getSinceTweetId());

        requestTopic.setUntil(System.currentTimeMillis());
        updateTopic1(requestTopic);
        assertEventListener.collectedTweets.clear();
        assertEventListener.eventQueue.clear();

        // Thread.sleep(90 * 1000);

        //  Assert.assertEquals(0, assertEventListener.collectedTweets.size());
        //  Assert.assertEquals(0, assertEventListener.eventQueue.size());

    }

    private void updateTopic1(RequestTopic requestTopic) {
        HttpEntity<RequestTopic> entity2 = new HttpEntity<>(requestTopic, getAuthHeader());
        ResponseEntity<SuccessResponse> response2 = restTemplate.exchange(
                "/api/v1/apps/" + app.getAppId() + "/topics/" + requestTopic.getId(), HttpMethod.PUT, entity2,
                SuccessResponse.class);
    }

    @Test
    public void testSchedulingTopics() throws Throwable {
        AssertEventListener assertEventListener = new AssertEventListener();
        eventBus.register(assertEventListener);
        TwitterConnector twitterConnector = createTwitterConnector(createRequestTwitterConnector(2, 15));
        MsTranslatorConnector msTranslatorConnector = createMsTranslatorConnector(createRequestMsTranslatorConnector());
        String klaverConnectorId = createKlaverConnector();
        String description = "description";

        for (int i = 0; i < 1; i++) {
            RequestKeyword keyword = new RequestKeyword(IdFactory.getUUID(),
                    "clojure", "en", "CUSTOM");
            RequestKeyword keyword1 = new RequestKeyword(IdFactory.getUUID(),
                    "java", "en", "CUSTOM");
            List<RequestKeyword> requestKeywords = Arrays.asList(keyword, keyword1);
            RequestTopic requestTopic = createRequestTopic(twitterConnector, msTranslatorConnector, description + " " + i,
                    System.currentTimeMillis() - DateTimeConstants.MILLIS_PER_DAY * 30L,
                    System.currentTimeMillis() + DateTimeConstants.MILLIS_PER_DAY * 10L,
                    "STOPPED",
                    requestKeywords, klaverConnectorId);
            createTopic(requestTopic);
        }

        // twitterScheduler.unSchedule(twitterConnector);

        // updateTwitterConnector(createRequestTwitterConnector(3, 10, twitterConnector.getRemoteId()));
        while (assertEventListener.eventQueue.size() < 11) ;

        List<?> failures = assertEventListener.eventQueue
                .stream()
                .filter(e -> e instanceof Failure)
                .collect(Collectors.toList());

        List<Failure> errors = (List<Failure>) failures;

        if (!errors.isEmpty()) {
            errors.forEach(f -> f.err.printStackTrace());
            throw errors.get(0).err;
        }
        long e1 = System.currentTimeMillis();
        Multiset<Tweet> tweets = HashMultiset.create(assertEventListener.collectedTweets);

        Set<Multiset.Entry<Tweet>> entries = tweets.entrySet();

        Set<Multiset.Entry<Tweet>> duplicateTweetEntries = entries.stream().filter(e -> e.getCount() > 1).collect(Collectors.toSet());

        Assert.assertThat(duplicateTweetEntries.size(), new LessOrEqual<>(10));

        entries.forEach(tweetEntry -> Assert.assertThat(tweetEntry.getCount(), new LessOrEqual<>(2)));

        RequestKeyword keyword = new RequestKeyword(IdFactory.getUUID(),
                "functional programming", "en", "CUSTOM");
        List<RequestKeyword> requestKeywords = Arrays.asList(keyword);
        RequestTopic requestTopic = createRequestTopic(twitterConnector, msTranslatorConnector, description,
                System.currentTimeMillis() - DateTimeConstants.MILLIS_PER_DAY * 30L,
                System.currentTimeMillis() + DateTimeConstants.MILLIS_PER_DAY * 10L,
                "STOPPED",
                requestKeywords, klaverConnectorId);
        createTopic(requestTopic);


        assertEventListener.collectedTweets.clear();
        assertEventListener.eventQueue.clear();

        while (assertEventListener.eventQueue.size() < 4) ;

        List<?> failures1 = assertEventListener.eventQueue
                .stream()
                .filter(e -> e instanceof Failure)
                .collect(Collectors.toList());

        List<Failure> errors1 = (List<Failure>) failures;


    }

}
