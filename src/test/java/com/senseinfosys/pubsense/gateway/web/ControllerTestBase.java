package com.senseinfosys.pubsense.gateway.web;

import com.senseinfosys.pubsense.gateway.domain.connector.Connector.ConnectorType;
import com.senseinfosys.pubsense.gateway.domain.translator.ms.MsTranslatorConnector;
import com.senseinfosys.pubsense.gateway.domain.twitter.TwitterConnector;
import com.senseinfosys.pubsense.gateway.infrastructure.RestClient;
import com.senseinfosys.pubsense.gateway.infrastructure.id.IdFactory;
import com.senseinfosys.pubsense.gateway.infrastructure.json.JsonUtils;
import com.senseinfosys.pubsense.gateway.infrastructure.security.JwtResponse;
import com.senseinfosys.pubsense.gateway.web.app.RequestApp;
import com.senseinfosys.pubsense.gateway.web.app.ResponseApp;
import com.senseinfosys.pubsense.gateway.web.connector.RequestKlaverConnector;
import com.senseinfosys.pubsense.gateway.web.connector.RequestMsTranslatorConnector;
import com.senseinfosys.pubsense.gateway.web.connector.RequestTwitterConnector;
import com.senseinfosys.pubsense.gateway.web.support.SuccessResponse;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Base64;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
// @DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public abstract class ControllerTestBase {

    @Autowired
    protected TestRestTemplate restTemplate;

    protected ResponseApp app;

    protected JwtResponse jwt;

    static {
        RestClient.init();
    }

    @Before
    public void setup() {
        // Headers with authorization
        HttpHeaders headers = new HttpHeaders();

        // Register an app
        String userCredentials = "gateway_admin:ch894ae3d9JVcTESjY6BiKKeSJeEiM1N";
        headers.add(HttpHeaders.AUTHORIZATION,
                "Basic " + Base64.getEncoder().encodeToString(userCredentials.getBytes()));
        HttpEntity<RequestApp> entity = new HttpEntity<>(new RequestApp(IdFactory.getUUID()), headers);
        ResponseEntity<SuccessResponse> response = restTemplate.postForEntity("/api/v1/apps/register", entity,
                SuccessResponse.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        app = JsonUtils.convertValue(response.getBody().getData(), ResponseApp.class);
        assertThat(app.getAppId()).isNotNull();
        assertThat(app.getAppSecret()).isNotNull();

        // Get jwt
        HttpHeaders headers1 = new HttpHeaders();
        String appCredentials = app.getAppId() + ":" + app.getAppSecret();
        headers1.add(HttpHeaders.AUTHORIZATION,
                "Basic " + Base64.getEncoder().encodeToString(appCredentials.getBytes()));
        HttpEntity<RequestApp> entity1 = new HttpEntity<>(headers1);
        ResponseEntity<SuccessResponse> response1 = restTemplate.postForEntity("/api/v1/apps/token", entity1,
                SuccessResponse.class);
        assertThat(response1.getStatusCode()).isEqualTo(HttpStatus.OK);
        jwt = JsonUtils.convertValue(response1.getBody().getData(), JwtResponse.class);
        assertThat(jwt.getToken()).isNotNull();
        assertThat(jwt.getExpiredAt()).isNotNull();
    }

    public String getBearerToken() {
        return "Bearer " + jwt.getToken();
    }

    public HttpHeaders getAuthHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, getBearerToken());
        return headers;
    }

    protected void deleteMsTranslatorConnector(MsTranslatorConnector msTranslatorConnector) {
        HttpEntity<RequestMsTranslatorConnector> entity3 = new HttpEntity<>(null, getAuthHeader());
        ResponseEntity<SuccessResponse> response3 = restTemplate.exchange(
                "/api/v1/apps/" + app.getAppId() + "/connectors/ms_translator/" + msTranslatorConnector.getRemoteId(),
                HttpMethod.DELETE, entity3, SuccessResponse.class);
        assertThat(response3.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    protected MsTranslatorConnector updateMsTranslatorConnector(
            RequestMsTranslatorConnector requestMsTranslatorConnector) {
        requestMsTranslatorConnector.charsPerMonth = 0;

        HttpEntity<RequestMsTranslatorConnector> entity2 = new HttpEntity<>(requestMsTranslatorConnector,
                getAuthHeader());
        ResponseEntity<SuccessResponse> response2 = restTemplate.exchange(
                "/api/v1/apps/" + app.getAppId() + "/connectors/ms_translator/" + requestMsTranslatorConnector.getId(),
                HttpMethod.PUT, entity2, SuccessResponse.class);
        assertThat(response2.getStatusCode()).isEqualTo(HttpStatus.OK);

        MsTranslatorConnector connector2 = JsonUtils.convertValue(response2.getBody().getData(),
                MsTranslatorConnector.class);
        return connector2;
    }

    protected MsTranslatorConnector createConnector(RequestMsTranslatorConnector requestMsTranslatorConnector) {
        return createMsTranslatorConnector(requestMsTranslatorConnector);
    }

    protected MsTranslatorConnector createMsTranslatorConnector(
            RequestMsTranslatorConnector requestMsTranslatorConnector) {
        HttpEntity<RequestMsTranslatorConnector> entity1 = new HttpEntity<>(requestMsTranslatorConnector,
                getAuthHeader());
        ResponseEntity<SuccessResponse> response1 = restTemplate.postForEntity(
                "/api/v1/apps/" + app.getAppId() + "/connectors/ms_translator", entity1, SuccessResponse.class);
        assertThat(response1.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        MsTranslatorConnector connector1 = JsonUtils.convertValue(response1.getBody().getData(),
                MsTranslatorConnector.class);
        return connector1;
    }


    protected String createKlaverConnector() {
        RequestKlaverConnector requestKlaverConnector = createRequestKlaverConnector();
        HttpEntity<RequestKlaverConnector> entity1 = new HttpEntity<>(requestKlaverConnector,
                getAuthHeader());
        ResponseEntity<SuccessResponse> response1 = restTemplate.postForEntity(
                "/api/v1/apps/" + app.getAppId() + "/connectors/klaver", entity1, SuccessResponse.class);
        assertThat(response1.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        return requestKlaverConnector.getId();
    }

    protected RequestMsTranslatorConnector createRequestMsTranslatorConnector() {
        RequestMsTranslatorConnector requestMsTranslatorConnector = new RequestMsTranslatorConnector(5000, 
                "7eadd964a8714237a68851ecacb51697"); // TODO: remove credentials
        requestMsTranslatorConnector.id = IdFactory.getUUID();
        requestMsTranslatorConnector.appId = app.getAppId();
        requestMsTranslatorConnector.type = ConnectorType.MS_TRANSLATOR.name();
        return requestMsTranslatorConnector;
    }

    protected RequestTwitterConnector createRequestTwitterConnector(int interval, int maxRequests) {
        return createRequestTwitterConnector(interval, maxRequests, IdFactory.getUUID());
    }

    protected RequestTwitterConnector createRequestTwitterConnector(int interval, int maxRequests, String id) {
        RequestTwitterConnector requestTwitterConnector = new RequestTwitterConnector("Oq6RvY63nzQChTZCVktCmkiB3",
                "bQBsoA1YbmTkJJ2LN6fu0pgzFW75RddDWZykUsofML8GFK3nu5", "zAZUC7C3RYxSGtmxkiaTqdxTbysAyPPfkN7FdAWf",
                "Lq7Oxt7XFGR7hiJd81WWcSzzkwmWBN8uZpLptrTdiQJrX", interval, maxRequests);
        requestTwitterConnector.id = id;
        requestTwitterConnector.appId = app.getAppId();
        requestTwitterConnector.type = ConnectorType.TWITTER.name();
        return requestTwitterConnector;
    }

    protected RequestKlaverConnector createRequestKlaverConnector() {
        RequestKlaverConnector requestKlaverConnector = new RequestKlaverConnector();
        requestKlaverConnector.id = IdFactory.getUUID();
        requestKlaverConnector.appId = app.getAppId();
        requestKlaverConnector.type = ConnectorType.KLAVER.name();
        requestKlaverConnector.token = "fake_token";
        return requestKlaverConnector;
    }


    protected TwitterConnector createTwitterConnector(RequestTwitterConnector requestTwitterConnector) {
        HttpEntity<RequestTwitterConnector> entity = new HttpEntity<>(requestTwitterConnector, getAuthHeader());
        ResponseEntity<SuccessResponse> response = restTemplate
                .postForEntity("/api/v1/apps/" + app.getAppId() + "/connectors/twitter", entity, SuccessResponse.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        TwitterConnector connector = JsonUtils.convertValue(response.getBody().getData(), TwitterConnector.class);
        return connector;
    }

    protected TwitterConnector updateTwitterConnector(RequestTwitterConnector requestTwitterConnector) {
        HttpEntity<RequestTwitterConnector> entity = new HttpEntity<>(requestTwitterConnector, getAuthHeader());

        ResponseEntity<SuccessResponse> response = restTemplate.exchange(
                "/api/v1/apps/" + app.getAppId() + "/connectors/twitter/" + requestTwitterConnector.getId(),
                HttpMethod.PUT, entity, SuccessResponse.class);

        TwitterConnector connector = JsonUtils.convertValue(response.getBody().getData(), TwitterConnector.class);
        return connector;
    }
}
