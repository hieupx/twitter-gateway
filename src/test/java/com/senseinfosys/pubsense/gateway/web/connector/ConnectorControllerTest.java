package com.senseinfosys.pubsense.gateway.web.connector;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import com.senseinfosys.pubsense.gateway.domain.translator.ms.MsTranslatorConnector;
import com.senseinfosys.pubsense.gateway.domain.twitter.TwitterConnector;
import com.senseinfosys.pubsense.gateway.web.ControllerTestBase;

public class ConnectorControllerTest extends ControllerTestBase {

    @Test
    public void testConnectors() {

        // Create a twitter connector
        RequestTwitterConnector requestTwitterConnector = createRequestTwitterConnector(15, 10);
        TwitterConnector twitterConnector = createTwitterConnector(requestTwitterConnector);
        assertCreatingTwitterConnector(requestTwitterConnector, twitterConnector);

        // Create a translator translator connector
        RequestMsTranslatorConnector requestMsTranslatorConnector = createRequestMsTranslatorConnector();
        MsTranslatorConnector msTranslatorConnector = createMsTranslatorConnector(requestMsTranslatorConnector);
        assertCreatingMsTranslatorConnector(requestMsTranslatorConnector, msTranslatorConnector);

        // Update a connector
        MsTranslatorConnector updatedMsTranslatorConnector = updateMsTranslatorConnector(requestMsTranslatorConnector);
        assertUpdatingMsTranslatorConnector(requestMsTranslatorConnector, updatedMsTranslatorConnector);

        // Delete a connector
        deleteMsTranslatorConnector(msTranslatorConnector);

    }

    private void assertCreatingTwitterConnector(RequestTwitterConnector requestTwitterConnector,
                                                TwitterConnector twitterConnector) {
        assertThat(twitterConnector.getRemoteId()).isEqualTo(requestTwitterConnector.getId());
        assertThat(twitterConnector.getConsumerKey()).isEqualTo(requestTwitterConnector.getConsumerKey());
        assertThat(twitterConnector.getConsumerSecret()).isEqualTo(requestTwitterConnector.getConsumerSecret());
        assertThat(twitterConnector.getIntervalLength()).isEqualTo(requestTwitterConnector.getIntervalLength());
        assertThat(twitterConnector.getRequestsPerInterval())
                .isEqualTo(requestTwitterConnector.getRequestsPerInterval());
        assertThat(twitterConnector.getApplication()).isNull();
    }

    private void assertCreatingMsTranslatorConnector(RequestMsTranslatorConnector requestMsTranslatorConnector,
                                                     MsTranslatorConnector msTranslatorConnector) {
        assertThat(msTranslatorConnector.getApplication()).isNull();
        assertThat(msTranslatorConnector.getRemoteId()).isEqualTo(requestMsTranslatorConnector.getId());
        assertThat(msTranslatorConnector.getId()).isNotNull();
        assertThat(msTranslatorConnector.getSubscriptionKey()).isEqualTo(requestMsTranslatorConnector.getSubscriptionKey());
        assertThat(msTranslatorConnector.getCharsPerMonth()).isEqualTo(requestMsTranslatorConnector.getCharsPerMonth());
    }

    private void assertUpdatingMsTranslatorConnector(RequestMsTranslatorConnector requestMsTranslatorConnector,
                                                     MsTranslatorConnector updatedMsTranslatorConnector) {
        assertThat(updatedMsTranslatorConnector.getApplication()).isNull();
        assertThat(updatedMsTranslatorConnector.getRemoteId()).isEqualTo(requestMsTranslatorConnector.getId());
        assertThat(updatedMsTranslatorConnector.getId()).isNotNull();
        assertThat(updatedMsTranslatorConnector.getSubscriptionKey())
                .isEqualTo(requestMsTranslatorConnector.getSubscriptionKey());
        assertThat(updatedMsTranslatorConnector.getCharsPerMonth()).isEqualTo(5000);
    }
}
