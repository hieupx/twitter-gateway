package com.senseinfosys.pubsense.gateway.web;

import com.google.common.eventbus.EventBus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by toan on 10/27/16.
 */

@Configuration
public class Config {
    @Bean(name = "test1")
    public EventBus eventBus() {
        return new EventBus("test1");
    }

}
