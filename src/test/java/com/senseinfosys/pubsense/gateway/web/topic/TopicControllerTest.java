package com.senseinfosys.pubsense.gateway.web.topic;

import com.github.krukow.clj_lang.PersistentHashMap;
import com.senseinfosys.pubsense.gateway.domain.connector.ConnectorService;
import com.senseinfosys.pubsense.gateway.domain.translator.ms.MsTranslatorConnector;
import com.senseinfosys.pubsense.gateway.domain.twitter.TwitterConnector;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.Topic;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.Topic.RunStatus;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.TopicService;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.TopicStatus;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.TopicStatusService;
import com.senseinfosys.pubsense.gateway.infrastructure.RestClient;
import com.senseinfosys.pubsense.gateway.infrastructure.id.IdFactory;
import com.senseinfosys.pubsense.gateway.infrastructure.json.JsonUtils;
import com.senseinfosys.pubsense.gateway.web.ControllerTestBase;
import com.senseinfosys.pubsense.gateway.web.connector.RequestMsTranslatorConnector;
import com.senseinfosys.pubsense.gateway.web.support.SuccessResponse;
import javaslang.Tuple2;
import org.joda.time.DateTimeConstants;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TopicControllerTest extends ControllerTestBase {

    @Autowired
    TopicService topicService;

    @Autowired
    ConnectorService connectorService;

    @Autowired
    TopicStatusService topicStatusService;

    private TwitterConnector twitterConnector;
    private MsTranslatorConnector msTranslatorConnector;

    private String klaverConnectorId;

    public void initConnectors() {
        twitterConnector = createTwitterConnector(createRequestTwitterConnector(15, 10));
        msTranslatorConnector = createMsTranslatorConnector(createRequestMsTranslatorConnector());
        klaverConnectorId = createKlaverConnector();
    }

    @Test
    public void testTopics() throws InterruptedException {
        initConnectors();
        CreateTopic createTopic = new CreateTopic().invoke();
        Topic topic = createTopic.getTopic();
        RequestTopic requestTopic = createTopic.getRequestTopic();

        assertCreatingTopic(topic, requestTopic);

        Topic updatedTopic = updateTopic(requestTopic);
        assertUpdatingTopic(updatedTopic, requestTopic);

        //connectorService.delete(twitterConnector.getRemoteId(), "twitter", app.getAppId());

        deleteTopic(updatedTopic);
    }

    public void deleteTopic(Topic updatedTopic) {
        HttpEntity<RequestMsTranslatorConnector> entity3 = new HttpEntity<>(null, getAuthHeader());
        ResponseEntity<SuccessResponse> response3 = restTemplate.exchange(
                "/api/v1/apps/" + app.getAppId() + "/topics/" + updatedTopic.getRemoteId(), HttpMethod.DELETE, entity3,
                SuccessResponse.class);
        assertThat(response3.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);

        assertThat(topicService.isDeletedCompletely(updatedTopic.getId())).isTrue();
    }

    public void assertUpdatingTopic(Topic updatedTopic, RequestTopic requestTopic) {
        assertThat(updatedTopic.getRunStatus().name()).isEqualTo(RunStatus.STARTED.name());
        assertThat(updatedTopic.getDescription()).isNull();
        assertThat(updatedTopic.getUntil()).isNull();
    }

    public Topic updateTopic(RequestTopic requestTopic) {
        requestTopic.description = null;
        requestTopic.until = null;
        requestTopic.runStatus = RunStatus.STARTED.name();

        RequestKeyword keyword2 = new RequestKeyword(IdFactory.getUUID(), "compiler", "en", "CUSTOM");
        RequestKeyword next = requestTopic.keywords.iterator().next();
        requestTopic.keywords = Arrays.asList(keyword2, next);
        HttpEntity<RequestTopic> entity2 = new HttpEntity<>(requestTopic, getAuthHeader());
        ResponseEntity<SuccessResponse> response2 = restTemplate.exchange(
                "/api/v1/apps/" + app.getAppId() + "/topics/" + requestTopic.getId(), HttpMethod.PUT, entity2,
                SuccessResponse.class);
        assertThat(response2.getStatusCode()).isEqualTo(HttpStatus.OK);
        Topic topic = JsonUtils.convertValue(response2.getBody().getData(), Topic.class);
        return topic;
    }

    public void assertCreatingTopic(Topic topic, RequestTopic requestTopic) {
        assertThat(topic.getRemoteId()).isEqualTo(requestTopic.getId());
        assertThat(topic.getLat()).isEqualTo(requestTopic.getLat());
        assertThat(topic.getLon()).isEqualTo(requestTopic.getLon());
        assertThat(topic.getSince().getTime()).isEqualTo(requestTopic.getSince());
        assertThat(topic.getRunStatus().name()).isEqualTo(RunStatus.STARTED.name());
        assertThat(topic.getKeywords().size()).isEqualTo(requestTopic.getKeywords().size());
    }

    public Topic createTopic(RequestTopic requestTopic) {
        HttpEntity<RequestTopic> entity1 = new HttpEntity<>(requestTopic, getAuthHeader());
        ResponseEntity<SuccessResponse> response1 = restTemplate
                .postForEntity("/api/v1/apps/" + app.getAppId() + "/topics", entity1, SuccessResponse.class);
        assertThat(response1.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        Topic topic = JsonUtils.convertValue(response1.getBody().getData(), Topic.class);
        return topic;
    }

    public RequestTopic createRequestTopic(TwitterConnector twitterConnector, MsTranslatorConnector msTranslatorConnector, String description, Long since, Long until, String runStatus, List<RequestKeyword> keywords, String klaverConnectorId) {
        RequestTopic requestTopic = new RequestTopic();
        requestTopic.appId = app.getAppId();
        requestTopic.description = description;
        requestTopic.id = IdFactory.getUUID();
        // requestTopic.lat = 100.5;
        // requestTopic.lon = 50.1;
        requestTopic.twitterConnectorId = twitterConnector.getRemoteId();
        if (msTranslatorConnector != null) {
            requestTopic.msTranslatorConnectorId = msTranslatorConnector.getRemoteId();
        }
        // requestTopic.radius = 100.0;
        // requestTopic.radiusUnit = DistanceUnit.MILES.name();
        requestTopic.since = since;
        requestTopic.until = until;
        requestTopic.runStatus = runStatus;
        requestTopic.timeZone = "Asia/Singapore";
        requestTopic.keywords = keywords;
        requestTopic.klaverConnectorId = klaverConnectorId;
        //requestTopic.findNegativeEmoticons = true;
        //requestTopic.findOriginalPosts = true;
        //requestTopic.findQuestions = true;
        // requestTopic.findPositiveEmoticons = true;
        return requestTopic;
    }

    @Test
    public void getTopicStatus() {
        initConnectors();
        CreateTopic createTopic = new CreateTopic().invoke();
        Topic topic = createTopic.getTopic();
        RequestTopic requestTopic = createTopic.getRequestTopic();
        List<Tuple2<Topic, TopicStatus>> statuses = topicStatusService.findLatest(Arrays.asList(topic.getRemoteId()));

        HttpEntity entity1 = new HttpEntity(
                PersistentHashMap.create(
                        "topicIds", Arrays.asList(topic.getRemoteId()),
                        "latest", true),
                getAuthHeader());

        ResponseEntity<SuccessResponse> response1 = restTemplate
                .postForEntity("/api/v1/apps/" + app.getAppId() + "/topics/statuses", entity1, SuccessResponse.class);

        List<Object> latestStatuses = (List<Object>) response1.getBody().getData();

        ResponseLatestStatus status = JsonUtils.convertValue(latestStatuses.get(0), ResponseLatestStatus.class);

        assertEquals("NO_ISSUE", status.get("serviceStatus"));
        assertNotNull(status.get("info"));
        RestClient.init();
    }

    class CreateTopic {

        private RequestTopic requestTopic;

        private Topic topic;

        public RequestTopic getRequestTopic() {
            return requestTopic;
        }

        public Topic getTopic() {
            return topic;
        }

        public CreateTopic invoke() {
            RequestKeyword keyword = new RequestKeyword(IdFactory.getUUID(),
                    "functional programming OR object oriented programming", "en", "CUSTOM");
            RequestKeyword keyword1 = new RequestKeyword(IdFactory.getUUID(), "Domain driven design", "en", "CUSTOM");
            requestTopic = createRequestTopic(twitterConnector, msTranslatorConnector, "programming",
                    System.currentTimeMillis() - DateTimeConstants.MILLIS_PER_DAY * 2, null, "STOPPED",
                    Arrays.asList(keyword, keyword1), klaverConnectorId);
            topic = createTopic(requestTopic);
            return this;
        }
    }
}
