package com.senseinfosys.pubsense.gateway.web.twitter;

import com.senseinfosys.pubsense.gateway.domain.twitter.topic.Keyword;
import com.senseinfosys.pubsense.gateway.domain.twitter.topic.Topic;
import com.senseinfosys.pubsense.gateway.domain.twitter.tweet.Tweet;
import com.senseinfosys.pubsense.gateway.infrastructure.date.DateUtils;
import org.hamcrest.core.StringContains;
import org.junit.Assert;
import org.mockito.internal.matchers.GreaterOrEqual;
import org.mockito.internal.matchers.LessOrEqual;

import java.util.List;

/**
 * Created by toan on 10/28/16.
 */
public class TweetAssertion {

    Topic topic;

    Keyword keyword;

    List<Tweet> tweets;

    public TweetAssertion(Topic topic, Keyword keyword, List<Tweet> tweets) {
        this.topic = topic;
        this.keyword = keyword;
        this.tweets = tweets;
    }

    public void doAssert() {

        for (int i = 0; i < tweets.size() - 1; i++) {
            long id1 = tweets.get(i).getId();
            long id2 = tweets.get(i + 1).getId();
            Assert.assertTrue(id1 > id2);
        }

        for (Tweet tweet : tweets) {
            // Topic assertions
            Assert.assertEquals(topic.getRemoteId(), tweet.getTopicId());
            Assert.assertEquals(topic.getDescription(), tweet.getTopic());

            if (Boolean.TRUE.equals(topic.getFindNegativeEmoticons())) {
                Assert.assertThat(tweet.getTextRaw(), new StringContains(Keyword.NEGATIVE_EMOTICONS));
            }

            if (Boolean.TRUE.equals(topic.getFindPositiveEmoticons())) {
                Assert.assertThat(tweet.getTextRaw(), new StringContains(Keyword.POSITIVE_EMOTICONS));
            }

            if (Boolean.TRUE.equals(topic.getFindQuestions())) {
                Assert.assertThat(tweet.getTextRaw(), new StringContains(Keyword.QUESTIONS));
            }

            if (Boolean.TRUE.equals(topic.getFindOriginalPosts())) {
                Assert.assertEquals(0, tweet.getShareCount());
            }

            if (topic.getSince() != null) {
                Assert.assertThat(tweet.getDate().getTime(), new GreaterOrEqual<>(DateUtils.getBeginningOfDay(topic.getSince()).getTime()));
            }

            if (topic.getUntil() != null) {
                Assert.assertThat(tweet.getDate().getTime(), new LessOrEqual<>(DateUtils.getEndOfDay(topic.getUntil()).getTime()));
            }

            // Keyword assertions
            if (tweet.getSearchType() == Tweet.SearchType.RECENT) {
                Assert.assertThat(tweet.getId(), new GreaterOrEqual<>(keyword.getSinceTweetId()));
            }

            if (tweet.getSearchType() == Tweet.SearchType.HISTORICAL) {
                Assert.assertThat(tweet.getId(), new LessOrEqual<>(keyword.getMaxTweetId()));
            }
        }
    }


}
