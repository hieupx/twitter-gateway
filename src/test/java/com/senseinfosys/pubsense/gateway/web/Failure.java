package com.senseinfosys.pubsense.gateway.web;

/**
 * Created by toan on 10/27/16.
 */
public class Failure implements Event {

    public final Throwable err;

    public Failure(Throwable err) {
        this.err = err;
    }
}
