All configuration properties will be loaded from resources/application.properties, and all the properties are profile-dependent and vary from profile to profile. 
However build profiles are defined in pom.xml, so a rebuild is required when we need modify any property of a profile, which sometimes causes inconvenience.

We need a way to change some properties without rebuilding the project, heres the way:

1. Create a deploy-folder, e.g: /pubsense/gateway
2. Move pubsense-gateway-0.0.1-SNAPSHOT.jar the deploy-folder
3. Create a file <deploy-folder>/config/application.properties, e.g /pubsense/gateway/config/application.properties
4. Copy the content of resources/application.properties to <deploy-folder>/config/application.properties
5. Modify properties we want in <deploy-folder>/config/application.properties.  Note, every variable needs to exist, even if we're modifying just a subset.

Example: We want to change the server.port=8888 and security.jwt.secret=new_secret, spring.datasource.url=jdbc:mysql://localhost/gateway?useUnicode=yes&amp;characterEncoding=UTF-8
```
# resources/application.properties
spring.datasource.url=jdbc:mysql://localhost/gateway_test?useUnicode=yes&amp;characterEncoding=UTF-8
spring.datasource.username=gateway_user
spring.datasource.password=NtqH@n01
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.jpa.hibernate.dialect=org.hibernate.dialect.MySQL5Dialect
spring.jpa.generate-ddl=false
spring.jpa.hibernate.ddl-auto=validate
spring.jpa.show-sql=false
spring.datasource.type=com.zaxxer.hikari.HikariDataSource
spring.datasource.hikari.autoCommit=false
spring.datasource.hikari.maximumPoolSize=100
spring.datasource.hikari.transactionIsolation=TRANSACTION_READ_COMMITTED
security.jwt.secret=gateway-jwt-secret
# 24h
security.jwt.expiration=24
server.port=8081
server.tomcat.max-threads=50
server.tomcat.min-spare-threads=10
tweet-collection.max-worker-threads=50
tweet-processing.max-worker-threads=50
twitter.max-tweets-per-requests=100
bcrypt.salt=$2a$10$sOT8tgRhunAW4QPrZ.CHU.
klaver.tweeets.url=http://192.168.1.153/klaverrhcc/tweets

```

Create the file /pubsense/gateway/config/application.properties with content as follows:

```
# /pubsense/gateway/config/application.properties

spring.datasource.url=jdbc:mysql://localhost/gateway?useUnicode=yes&amp;characterEncoding=UTF-8
spring.datasource.username=gateway_user
spring.datasource.password=NtqH@n01
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.jpa.hibernate.dialect=org.hibernate.dialect.MySQL5Dialect
spring.jpa.generate-ddl=false
spring.jpa.hibernate.ddl-auto=validate
spring.jpa.show-sql=false
spring.datasource.type=com.zaxxer.hikari.HikariDataSource
spring.datasource.hikari.autoCommit=false
spring.datasource.hikari.maximumPoolSize=100
spring.datasource.hikari.transactionIsolation=TRANSACTION_READ_COMMITTED
security.jwt.secret=new_secret
# 24h
security.jwt.expiration=24
server.port=8888
server.tomcat.max-threads=50
server.tomcat.min-spare-threads=10
tweet-collection.max-worker-threads=50
tweet-processing.max-worker-threads=50
twitter.max-tweets-per-requests=100
bcrypt.salt=$2a$10$sOT8tgRhunAW4QPrZ.CHU.
klaver.tweeets.url=http://192.168.1.153/klaverrhcc/tweets

```