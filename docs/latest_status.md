## Latest status API

### Request

```javascript
POST /api/v1/apps/:appId/topics/statuses
Authorization: Bearer <app_token>
{
    "topicIds" : [
        "aaaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeee0",
        "aaaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeee1",
        "aaaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeee2",
        "aaaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeee3",
        "aaaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeee4",
        "aaaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeee5",
        "aaaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeee6",
        "aaaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeee7",
        "aaaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeee8",
        "aaaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeee9"
    ],
    
    "latest" : true/false
}    
```

### Response 

```javascript
{
    "data":
         [
            {
                // general data collection failures
                "topicId": "aaaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeee0",
                "runStatus": "STARTED",
                "lastServiced": "2016-08-30T08:18:20.234+07:00[Asia/Ho_Chi_Minh]",
                "serviceStatus": "ERROR",
                 "error": {
                    "message" : "some error ocurred",
                    "code" : -111,
                    "occurredAt": "2016-08-30T08:18:20.234+07:00[Asia/Ho_Chi_Minh]" // lastest error's time
                 }
            },
            {
               // quota reached
               "topicId": "aaaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeee1",
               "runStatus": "STOPPED",
               "lastServiced": "2016-08-30T08:18:20.234+07:00[Asia/Ho_Chi_Minh]",
               "serviceStatus": "ERROR",
               "error": {
                "message" : "Quota reached for twitter connector ",
                "code" : -222,
                "occurredAt": "2016-08-30T08:18:20.234+07:00[Asia/Ho_Chi_Minh]"
               }
            },
              ...
            {
               // no errors
               "topicId": "aaaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeee9",
               "lastServiced": "2016-08-30T08:18:20.234+07:00[Asia/Ho_Chi_Minh]",
               "runStatus": "STARTED",
               "serviceStatus": "SUCCESS",
               "info": {
                    "message": "tweet collection succeeeded",
                    "code": Int
                    "occuredAt": "2016-08-30T08:18:20.234+07:00[Asia/Ho_Chi_Minh]"
               }
            }, 
            {
               // warning
               "topicId": "aaaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeee9",
               "lastServiced": "2016-08-30T08:18:20.234+07:00[Asia/Ho_Chi_Minh]",
               "runStatus": "STARTED",
               "serviceStatus": "WARNING",
               "warning": {
                    "message": "tweet collection task scheduled, too many tasks created, reduce the rate limit",
                    "code": Int
                    "occuredAt": "2016-08-30T08:18:20.234+07:00[Asia/Ho_Chi_Minh]"
               }
               
            }, 
            ...
        ]
}
```